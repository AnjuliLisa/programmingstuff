const mongoose = require("mongoose");

// open a connection to the test database (don't use production database!)
before(function (done) {
  mongoose
    .connect(
      "****connectionstring*****",
      {
        useNewUrlParser: true,
        useUnifiedTopology: true
      }
    )
    .then(() => done())
    .catch(err => done(err));
});

// drop both collections before each test
beforeEach(done => {
  const { users, days, meals, drinks } = mongoose.connection.collections;

  users.drop(() => {
    days.drop(() => {
      meals.drop(() => {
        drinks.drop(() => {
          done();
        })
      })
    });
  });
});
