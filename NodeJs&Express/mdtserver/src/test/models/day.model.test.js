const chai = require('chai')
const expect = chai.expect;
const Day = require('../../models/day.model')
const User = require('../../models/user.model')

describe('day model tests', function () {

    let user;

    this.beforeEach(async function () {
        user = new User(
            {
                firstname: 'Lisa',
                lastname: 'van den Berg',
                password: 'wachtwoord',
                email: 'lisavandenberg@mdt.nl',
                dateOfBirth_year: 1998,
                dateOfBirth_month: 12,
                dateOfBirth_day: 10
            });
        await user.save();
    });

    it('should reject a missing date_year', async function () {
        const day = new Day({
            date_month: 11,
            date_day: 13,
            userid: user._id
        })
        expect(async () => {
            day.save()
        }).to.throw

    })
    it('should reject a missing date_month', async function () {
        const day = new Day({
            date_year: 2020,
            date_day: 22,
            userid: user._id
        })
        expect(async () => {
            day.save()
        }).to.throw
    })
    it('should reject a missing date_day', async function () {
        const day = new Day({
            date_year: 2020,
            date_month: 2,
            userid: user._id
        })
        expect(async () => {
            day.save()
        }).to.throw
    })
    it('should reject a missing userid', async function () {

        const day = new Day({
            date_year: 2020,
            date_month: 1,
            date_day: 22
        })
        expect(async () => {
            day.save()
        }).to.throw

    })

})