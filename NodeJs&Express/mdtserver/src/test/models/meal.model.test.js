const chai = require('chai')
const expect = chai.expect;
const Day = require('../../models/day.model')
const Meal = require('../../models/meal.model')
const User = require('../../models/user.model')

describe('meal model tests', function () {

    let user;
    let day;

    this.beforeEach(async function () {
        user = new User(
            {
                firstname: 'Lisa',
                lastname: 'van den Berg',
                password: 'wachtwoord',
                email: 'lisavandenberg@mdt.nl',
                dateOfBirth_year: 1998,
                dateOfBirth_month: 12,
                dateOfBirth_day: 10
            });
        await user.save();

        day = new Day({
            userid: user._id,
            date_year: 1998,
            date_month: 12,
            date_day: 1
        })
        await day.save();
    });

    it('should reject a missing dayid', async function () {
        const meal = new Meal({
            name: 'Pannenkoeken',
            type: 'Breakfast',
            rating: 50,
            time: new Date()
        })
        expect(async () => {
            meal.save()
        }).to.throw

    })
    it('should reject a missing rating', async function () {
        const meal = new Meal({
            dayid: day._id,
            name: 'Pannenkoeken',
            type: 'Breakfast',
            time: new Date()
        })
        expect(async () => {
            meal.save()
        }).to.throw

    })

    it('should reject a rating out of bounds', async function () {
        const meal = new Meal({
            dayid: day._id,
            name: 'Pannenkoeken',
            type: 'Breakfast',
            rating: 150,
            time: new Date()
        })

        expect(async () => {
            meal.save()
        }).to.throw
    })
    it('should reject a rating out of bounds', async function () {
        const meal = new Meal({
            dayid: day._id,
            name: 'Pannenkoeken',
            type: 'Breakfast',
            rating: 150,
            time: new Date()
        })

        expect(async () => {
            meal.save()
        }).to.throw
    })

    it('should reject a missing type', async function () {
        const meal = new Meal({
            dayid: day._id,
            name: 'Pannenkoeken',
            rating: 100,
            time: new Date()
        })

        expect(async () => {
            meal.save()
        }).to.throw
    })

    it('should reject a invalid type', async function () {
        const meal = new Meal({
            dayid: day._id,
            name: 'Pannenkoeken',
            type: 'Ontbijt',
            rating: 100,
            time: new Date()
        })

        expect(async () => {
            meal.save()
        }).to.throw
    })

})