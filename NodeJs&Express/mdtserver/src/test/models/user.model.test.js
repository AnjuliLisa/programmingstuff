const chai = require('chai')
const expect = chai.expect;
const User = require('../../models/user.model')

describe('user model tests', function () {

    it('should reject a missing first name', async function () {
        const user = new User({
            lastname: 'van den Berg',
            password: 'wachtwoord',
            email: 'lisavandenberg@mdt.com',
            dateOfBirth_year: 1998,
            dateOfBirth_month: 12,
            dateOfBirth_day: 10,
            gender: 'female',
            diet: ['lactosevrij']
        })
        expect(async () => {
            user.save()
        }).to.throw

    })
    it('should reject a missing last name', async function () {

        const user = new User({
            firstname: 'lisa',
            password: 'wachtwoord',
            email: 'lisavandenberg@mdt.com',
            dateOfBirth_year: 1998,
            dateOfBirth_month: 12,
            dateOfBirth_day: 10,
            gender: 'female',
            diet: ['lactosevrij']
        })

        expect(async () => {
            user.save()
        }).to.throw


    })
    it('should reject a missing emailaddress', async function () {
        const user = new User({
            firstname: 'lisa',
            lastname: 'van den Berg',
            password: 'wachtwoord',
            dateOfBirth_year: 1998,
            dateOfBirth_month: 12,
            dateOfBirth_day: 10,
            gender: 'female',
            diet: ['lactosevrij']
        })

        expect(async () => {
            user.save()
        }).to.throw

    })
    it('should reject a missing password', async function () {
        const user = new User({
            firstname: 'lisa',
            lastname: 'van den Berg',
            email: 'lisavandenberg@mdt.com',
            dateOfBirth_year: 1998,
            dateOfBirth_month: 12,
            dateOfBirth_day: 10,
            gender: 'female',
            diet: ['lactosevrij']
        })

        expect(async () => {
            user.save()
        }).to.throw

    })
    it('should reject a missing date of birth year', async function () {
        const user = new User({
            firstname: 'lisa',
            lastname: 'van den Berg',
            password: 'wachtwoord',
            email: 'lisavandenberg@mdt.com',
            gender: 'female',
            dateOfBirth_month: 12,
            dateOfBirth_day: 10,
            diet: ['lactosevrij']
        })

        expect(async () => {
            user.save()
        }).to.throw

    })

    it('should create an empty diet list by default', async function () {
        const user = new User({
            firstname: 'lisa',
            lastname: 'van den Berg',
            password: 'wachtwoord',
            email: 'lisavandenberg@mdt.com',
            dateOfBirth_year: 1998,
            dateOfBirth_month: 12,
            dateOfBirth_day: 10,
            gender: 'female'
        })

        await user.save();
        expect(user).to.have.property('diet').and.to.be.empty
    })

})