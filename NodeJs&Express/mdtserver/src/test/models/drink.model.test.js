const chai = require('chai')
const expect = chai.expect;
const Day = require('../../models/day.model')
const Drink = require('../../models/drink.model')
const User = require('../../models/user.model')

describe('drink  model test', async function () {

    let user;
    let day;

    this.beforeEach(async function () {
        user = new User(
            {
                firstname: 'Lisa',
                lastname: 'van den Berg',
                password: 'wachtwoord',
                email: 'lisavandenberg@mdt.nl',
                dateOfBirth_year: 1998,
                dateOfBirth_month: 12,
                dateOfBirth_day: 10
            });
        await user.save();

        day = new Day({
            userid: user._id,
            date_year: 2020,
            date_month: 2,
            date_day: 22
        })
        await day.save();
    });

    it('should reject a missing dayid', async function () {
        const drink = new Drink({
            name: 'Water',
            quantity: 25,
            time: new Date()
        })
        expect(async () => {
            drink.save()
        }).to.throw
    })
    it('should reject a missing quantity', async function () {
        const drink = new Drink({
            dayid: day._id,
            name: 'Water',
            time: new Date()
        })
        expect(async () => {
            drink.save()
        }).to.throw
    })


    it('should reject a missing name', async function () {
        const drink = new Drink({
            dayid: day._id,
            quantity: 25,
            time: new Date()
        })
        expect(async () => {
            drink.save()
        }).to.throw
    })
})