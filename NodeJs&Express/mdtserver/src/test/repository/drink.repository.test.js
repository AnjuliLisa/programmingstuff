const repository = require('../../data/drink.repository');
const chai = require('chai')
const expect = chai.expect;
const User = require('../../models/user.model')
const Day = require('../../models/day.model')
const Drink = require('../../models/drink.model')

describe('Testing repository methods', function () {

    let day;
    let user;
    let drink;

    beforeEach(async function () {

        user = new User(
            {
                firstname: 'Lisa',
                lastname: 'van den Berg',
                password: 'wachtwoord',
                email: 'lisavandenberg@mdt.nl',
                dateOfBirth_year: 1998,
                dateOfBirth_month: 2,
                dateOfBirth_day: 2
            });
        await user.save();

        day = new Day({
            date_year: 2020,
            date_month: 1,
            date_day: 22,
            userid: user._id
        })
        await day.save();

        drink = new Drink({
            dayid: day._id,
            name: 'Water',
            quantity: 50,
            time: new Date(),
        })

        await drink.save();
    })

    it('should get a drink by id', async function () {
        const result = await repository.getDrinkById(drink._id);
        expect(result).not.to.be.null;
        expect(result).to.have.property('time')
        expect(result).to.have.property('name', 'Water')
    })

    it('should get all drinks with given dayid', async function () {
        const result = await repository.getAllDrinks(day._id);
        expect(result).not.to.be.null;
        expect(result).to.have.length(1)
    })
    it('should save a drink', async function () {
        const body = {
            dayid: day._id,
            name: 'Koffie',
            quantity: 150,
            time: new Date()
        }
        const result = await repository.saveDrink(body);
        expect(result).to.have.property('name', 'Koffie')
        expect(result).to.have.property('quantity', 150)
    })
    it('should update a drink', async function () {
        const body = {
            _id: drink._id,
            dayid: day._id,
            name: 'Koffie',
            quantity: 150,
            time: new Date()
        }
        const result = await repository.updateDrink(body);
        expect(result).to.have.property('name', 'Koffie')
        expect(result).to.have.property('quantity', 150)
    })
    it('should delete a drink', async function () {
        const result = await repository.deleteDrink(drink._id);
        expect(result).to.be.true;
    })



})