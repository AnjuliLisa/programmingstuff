const repository = require('../../data/meal.repository');
const chai = require('chai')
const expect = chai.expect;
const User = require('../../models/user.model')
const Day = require('../../models/day.model')
const Meal = require('../../models/meal.model')

describe('Testing repository methods', function () {

    let day;
    let user;
    let meal;

    beforeEach(async function () {

        user = new User(
            {
                firstname: 'Lisa',
                lastname: 'van den Berg',
                password: 'wachtwoord',
                email: 'lisavandenberg@mdt.nl',
                dateOfBirth_year: 1998,
                dateOfBirth_month: 12,
                dateOfBirth_day: 1
            });
        await user.save();

        day = new Day({
            date_year: 2020,
            date_month: 1,
            date_day: 22,
            userid: user._id
        })
        await day.save();

        meal = new Meal({
            dayid: day._id,
            name: 'Pannenkoeken',
            type: 'Breakfast',
            time: new Date(),
            rating: 50
        })
        await meal.save();
    })

    it('should get a meal by id', async function () {
        const result = await repository.getMealById(meal._id);
        expect(result).not.to.be.null;
        expect(result).to.have.property('type', 'Breakfast')
        expect(result).to.have.property('name', 'Pannenkoeken')
    })

    it('should get all meals with given dayid', async function () {
        const result = await repository.getAllMeals(day._id);
        expect(result).not.to.be.null;
        expect(result).to.have.length(1)
    })
    it('should save a meal', async function () {
        const body = {
            dayid: day._id,
            name: 'Tosti',
            type: 'Breakfast',
            time: new Date(),
            rating: 50
        }
        const result = await repository.saveMeal(body);
        expect(result).to.have.property('name', 'Tosti')
        expect(result).to.have.property('rating', 50)
    })
    it('should update a meal', async function () {
        const body = {
            _id: meal._id,
            dayid: day._id,
            name: 'Tosti',
            type: 'Breakfast',
            time: new Date(),
            rating: 50
        }
        const result = await repository.updateMeal(body);
        expect(result).to.have.property('name', 'Tosti')
        expect(result).to.have.property('rating', 50)
    })
    it('should delete a meal', async function () {
        const result = await repository.deleteMeal(meal._id);
        expect(result).to.be.true;
    })



})