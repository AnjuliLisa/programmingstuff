const repository = require('../../data/day.repository');
const chai = require('chai')
const expect = chai.expect;
const User = require('../../models/user.model')
const Day = require('../../models/day.model')

describe('Testing repository methods', function () {

    let day;
    let user;

    beforeEach(async function () {

        user = new User(
            {
                firstname: 'Lisa',
                lastname: 'van den Berg',
                password: 'wachtwoord',
                email: 'lisavandenberg@mdt.nl',
                dateOfBirth_year: 1998,
                dateOfBirth_month: 12,
                dateOfBirth_day: 10
            });
        await user.save();

        day = new Day({
            date_year: 2020,
            date_month: 1,
            date_day: 22,
            userid: user._id
        })
        await day.save();
    })

    it('Should get a day by id', async function () {
        const result = await repository.getDayById(day._id)
        expect(result).to.have.property('_id');
        expect(result).to.have.property('userid');
        expect(result).to.have.property('date_day');
    })

    it('Should get a day by userid and date', async function () {
        const data = {
            userid: user._id,
            date_year: 2020,
            date_month: 1,
            date_day: 22
        }
        const result = await repository.getDayByUserIdAndDate(data);
        expect(result).to.have.property('_id');
        expect(result).to.have.property('userid');
        expect(result).to.have.property('date_day');
    })

    it('Should save a day', async function () {
        const dayone = {
            date_year: 2020,
            date_month: 8,
            date_day: 22,
            userid: user._id
        }
        const result = await repository.saveDay(dayone)
        expect(result).not.to.be.null;
        expect(result).to.have.property('date_month', 8);
    })

    it('Should get all days', async function () {
        const result = await repository.getAllDays(user._id);
        expect(result).to.have.length(1);
        expect(result[0]).to.have.property('_id');
    })

    it('Should delete a day', async function () {
        const result = await repository.deleteDay(day._id)
        expect(result).to.be.true;
    })

    it('Should update a day', async function () {
        const body = {
            _id: day._id,
            date_year: 2020,
            date_month: 2,
            date_day: 1,
            userid: user._id
        }
        const result = await repository.updateDay(body);
        expect(result).not.to.be.null;
        expect(result).to.have.property('date_day', 1);
    })
})