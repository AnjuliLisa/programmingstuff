const repository = require('../../data/user.repository');
const chai = require('chai')
const expect = chai.expect;
const User = require('../../models/user.model')

describe('Testing repository methods', function () {

    let user;

    this.beforeEach(async function () {
        user = new User(
            {
                firstname: 'Lisa',
                lastname: 'van den Berg',
                password: 'wachtwoord',
                email: 'lisavandenberg@mdt.nl',
                gender: 'female',
                dateOfBirth_year: 1998,
                dateOfBirth_month: 12,
                dateOfBirth_day: 1
            });
        await user.save();
    })

    it('should get a user by email', async function () {
        const result = await repository.getUserByEmail(user.email)
        expect(result).to.have.property('_id')
        expect(result).to.have.property('firstname', user.firstname)
        expect(result).to.have.property('lastname', user.lastname)

    })

    it('should register a new user', async function () {
        const body = {
            firstname: 'Tim',
            lastname: 'Jansen',
            password: 'password123',
            email: 'timson@mdt.nl',
            gender: 'male',
            dateOfBirth_year: 1998,
            dateOfBirth_month: 12,
            dateOfBirth_day: 1
        }
        const result = await repository.register(body);
        expect(result).to.have.property('_id')
        expect(result).to.have.property('firstname', body.firstname)
    })

    it('should change the password', async function () {
        const data = {
            userid: user._id,
            password: 'pinda'
        }
        const result = await repository.changePassword(data)
        expect(result).to.be.true
    })

    it('should get all users', async function () {
        const result = await repository.getAllUsers();
        expect(result).to.have.length(1)
    })
})