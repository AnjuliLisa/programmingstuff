const requester = require("../../../requester.test");
const chai = require("chai");
const expect = chai.expect;
const Day = require('../../models/day.model')
const Drink = require('../../models/drink.model')
const User = require('../../models/user.model')

describe("drink endpoints tests", function () {

  let user
  let token
  let day
  let drink

  this.beforeEach(async function () {
    const body = {
      firstname: "Jos",
      lastname: "Jansen",
      password: "wachtwoord",
      email: "josjansen@mdt.com",
      dateOfBirth_year: 1998,
      dateOfBirth_month: 12,
      dateOfBirth_day: 1,
      gender: "male"
    }

    const res = await requester
      .post('/user/register')
      .send(body)

    token = res.body.token;
    user = await User.findOne()

    day = new Day({
      date_year: 2020,
      date_month: 2,
      date_day: 23,
      userid: user._id
    })

    await day.save();

    drink = new Drink({
      dayid: day._id,
      name: 'Water',
      quantity: 250,
      time: new Date()
    })
    await drink.save();
  })

  it("POST /day/:id/drinks should add a drink to a day", async function () {
    const body = {
      dayid: day._id,
      name: "Koffie",
      quantity: 150,
      time: new Date()
    };

    const result = await requester
      .post(`/drink/`)
      .set("Authorization", "Bearer " + token)
      .send(body);
    expect(result).to.have.status(200);
    expect(result.body).to.have.property('_id');
    expect(result.body).to.have.property('name', 'Koffie');
    expect(result.body).to.have.property('quantity', 150);
  });

  it("DELETE /drinks/:id should delete a drink", async function () {
    const res = await requester
      .delete(`/drink/${drink._id}`)
      .set("Authorization", "Bearer " + token);
    expect(res).to.have.status(200);
  });

  it("GET /drink/day/:id should get all drinks with a given day id", async function () {

    const res = await requester
      .get(`/drink/day/${day._id}`)
      .set("Authorization", "Bearer " + token);
    expect(res).to.have.status(200);
    expect(res.body).to.have.length(1);
  });

  it('GET /drink/:id should get a drink by id', async function () {
    const res = await requester
      .get(`/drink/${drink._id}`)
      .set("Authorization", "Bearer " + token);
    expect(res).to.have.status(200);
  })

  it('PUT /drink/:id should update a drink', async function () {
    const body = {
      _id: drink._id,
      dayid: day._id,
      name: "Koffie",
      quantity: 150,
      time: new Date()
    }

    const res = await requester.put(`/drink/${drink._id}`).set("Authorization", "Bearer " + token).send(body)
    expect(res).to.have.status(200)
    expect(res.body).to.have.property('_id')
    expect(res.body).to.have.property('name', 'Koffie')
    expect(res.body).to.have.property('quantity', 150)
  })
})


