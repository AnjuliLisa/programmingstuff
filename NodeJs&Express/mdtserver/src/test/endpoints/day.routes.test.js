const requester = require("../../../requester.test");
const chai = require("chai");
const expect = chai.expect;
const Day = require('../../models/day.model')
const User = require('../../models/user.model')

describe("day endpoints tests", function () {

  let user;
  let token;
  let day;

  this.beforeEach(async function () {
    const body = {
      firstname: "Jos",
      lastname: "Jansen",
      password: "wachtwoord",
      email: "josjansen@mdt.com",
      dateOfBirth_year: 1998,
      dateOfBirth_month: 12,
      dateOfBirth_day: 1,
      gender: "male"
    }

    const res = await requester
      .post('/user/register')
      .send(body)

    token = res.body.token;

    user = await User.findOne()

    day = new Day({
      date_year: 2020,
      date_month: 1,
      date_day: 22,
      userid: user._id
    })
    await day.save();
  });

  it("POST /day should add a new day", async function () {
    const body = {
      date_year: 2020,
      date_month: 1,
      date_day: 22
    };
    const res = await requester
      .post("/day")
      .set("Authorization", "Bearer " + token)
      .send(body);

    expect(res).to.have.status(200);
    expect(res.body).to.have.property("_id");
    expect(res.body).to.have.property("date_year");
    expect(res.body).to.have.property("userid");
  });

  it("POST /day should not create day without date", async function () {

    const res = await requester
      .post("/day")
      .set("Authorization", "Bearer " + token)
      .send({});

    expect(res).to.have.status(400);
    expect(res.body).to.have.property("message");
  });

  it("GET /day should return all days", async function () {
    const res = await requester
      .get("/day")
      .set("Authorization", "Bearer " + token);
    expect(res).to.have.status(200);
    expect(res.body).to.have.length(1)
  });

  it("GET /day should return code 401 if token is invalid", async function () {
    token = "234ksdkjhdkjdv";
    const res = await requester
      .get("/day")
      .set("Authorization", "Bearer " + token);
    expect(res).to.have.status(401);
  });

  it("GET /day/:id should return a day", async function () {
    const res = await requester
      .get(`/day/${day._id}`)
      .set("Authorization", "Bearer " + token);

    expect(res).to.have.status(200);
    expect(res.body).to.have.property("_id");
    expect(res.body).to.have.property("date_year");
    expect(res.body).to.have.property("userid");
  });

  it("GET /day/today should return a day", async function () {
    day = new Day({
      date_year: new Date().getFullYear(),
      date_month: new Date().getMonth() + 1,
      date_day: new Date().getDate() + 1,
      userid: user._id
    })
    await day.save();
    const res = await requester
      .get(`/day/today`)
      .set("Authorization", "Bearer " + token);

    expect(res).to.have.status(200);
    expect(res.body).to.have.property("_id");
    expect(res.body).to.have.property("date_year");
    expect(res.body).to.have.property("userid");
  });


  it("GET /day/today should return a day when it did not already exist", async function () {
    const res = await requester
      .get(`/day/today`)
      .set("Authorization", "Bearer " + token);

    expect(res).to.have.status(200);
    expect(res.body).to.have.property("_id");
    expect(res.body).to.have.property("date_year");
    expect(res.body).to.have.property("userid");
  });

  it("GET /day/:id should return status code 204 when day is not found", async function () {
    const id = 'jsadkjahskdjha'
    const res = await requester
      .get(`/day/${id}`)
      .set("Authorization", "Bearer " + token);
    expect(res).to.have.status(204);
  });

  it("DELETE /day should delete a day", async function () {
    const result = await requester
      .delete(`/day/${day._id}`)
      .set("Authorization", "Bearer " + token);
    expect(result).to.have.status(200);
  });

  it('PUT /day/:id should update a day', async function () {
    const data = {
      _id: day._id,
      date_year: 2020,
      date_month: 4,
      date_day: 23
    }
    const result = await requester
      .put(`/day/${day._id}`)
      .set("Authorization", "Bearer " + token)
      .send(data)
    expect(result).to.have.status(200)
    expect(result.body).to.have.property('date_month', 4)
  })
});
