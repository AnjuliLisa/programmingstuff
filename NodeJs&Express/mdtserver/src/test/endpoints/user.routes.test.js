const requester = require("../../../requester.test");
const chai = require("chai");
const expect = chai.expect;
const User = require('../../models/user.model')

describe("user endpoints tests", function () {

  let user;
  let token;

  this.beforeEach(async function () {
    const body = {
      firstname: "Jos",
      lastname: "Jansen",
      password: "wachtwoord",
      email: "josjansen@mdt.com",
      dateOfBirth_year: 1998,
      dateOfBirth_month: 12,
      dateOfBirth_day: 1,
      gender: "male"
    }

    const res = await requester
      .post('/user/register')
      .send(body)

    token = res.body.token;
    user = await User.findOne()


  })

  it("POST /user/register should create a user", async function () {
    const user = {
      firstname: "Lisa",
      lastname: "van den Berg",
      password: "wachtwoord",
      email: "lisavandenberg@mdt.com",
      dateOfBirth_year: 1998,
      dateOfBirth_month: 12,
      dateOfBirth_day: 1,
      gender: "female",
      diet: ["lactosevrij"]
    };
    const res = await requester.post("/user/register").send(user);
    expect(res).to.have.status(200);
    expect(res.body.user).to.have.property("firstname");
    expect(res.body).to.have.property("token");
  });
  it("POST /user/register should not create a user without firstname", async function () {
    const user = {
      lastname: "van den Berg",
      password: "wachtwoord",
      email: "lisavandenberg@mdt.com",
      dateOfBirth_year: 1998,
      dateOfBirth_month: 12,
      dateOfBirth_day: 1,
      gender: "female",
      diet: ["lactosevrij"]
    };

    const res = await requester.post("/user/register").send(user);

    expect(res).to.have.status(400);
    expect(res.body).to.have.property("message");
  });
  it("POST /user/register should not create a user without lastname", async function () {
    const user = {
      firstname: "Lisa",
      password: "wachtwoord",
      email: "lisavandenberg@mdt.com",
      dateOfBirth_year: 1998,
      dateOfBirth_month: 12,
      dateOfBirth_day: 1,
      gender: "female",
      diet: ["lactosevrij"]
    };

    const res = await requester.post("/user/register").send(user);

    expect(res).to.have.status(400);
    expect(res.body).to.have.property("message");
  });
  it("POST /user/register should not create a user without emailaddress", async function () {
    const user = {
      firstname: "Lisa",
      lastname: "van den Berg",
      password: "wachtwoord",
      dateOfBirth_year: 1998,
      dateOfBirth_month: 12,
      dateOfBirth_day: 1,
      gender: "female",
      diet: ["lactosevrij"]
    };

    const res = await requester.post("/user/register").send(user);

    expect(res).to.have.status(400);
    expect(res.body).to.have.property("message");
  });
  it("POST /user/register should not create a user without password", async function () {
    const user = {
      firstname: "Lisa",
      lastname: "van den Berg",
      email: "lisavandenberg@mdt.com",
      dateOfBirth_year: 1998,
      dateOfBirth_month: 12,
      dateOfBirth_day: 1,
      gender: "female",
      diet: ["lactosevrij"]
    };
    const res = await requester.post("/user/register").send(user);

    expect(res).to.have.status(400);
    expect(res.body).to.have.property("message");
  });
  it("POST /user/register should not create a user without date of birth", async function () {
    const user = {
      firstname: "Lisa",
      lastname: "van den Berg",
      password: "wachtwoord",
      email: "lisavandenberg@mdt.com",
      gender: "female",
      diet: ["lactosevrij"]
    };
    const res = await requester.post("/user/register").send(user);

    expect(res).to.have.status(400);
    expect(res.body).to.have.property("message");
  });
  it("POST /user/register should not create a user if emailaddress is already registered", async function () {
    const user = {
      firstname: "Lisa",
      lastname: "van den Berg",
      password: "wachtwoord",
      email: "lisavandenberg@mdt.com",
      dateOfBirth_year: 1998,
      dateOfBirth_month: 12,
      dateOfBirth_day: 1,
      gender: "female",
      diet: ["lactosevrij"]
    };
    const resone = await requester.post("/user/register").send(user);

    const res = await requester.post("/user/register").send(user);

    expect(res).to.have.status(400);
    expect(res.body).to.have.property(
      "message",
      "User with this emailaddress already exists"
    );
  });

  it("POST /user/login should return an token when email and password is correct", async function () {
    const login = {
      email: user.email,
      password: "wachtwoord"
    }

    const res = await requester.post("/user/login").send(login);
    expect(res).to.have.status(200);
    expect(res.body).to.have.property("token");
    expect(res.body).to.have.property("user");
    expect(res.body.user).to.have.property("firstname");
    expect(res.body.user).to.have.property("email");
  });
  it("POST /user/login should return a 401 error message when a wrong combination of emailAddress and password is given", async function () {
    const login = {
      email: user.email,
      password: "ander"
    }

    const res = await requester.post("/user/login").send(login);

    expect(res).to.have.status(401);
    expect(res.body).to.have.property(
      "message",
      'Invalid password'
    );
  });
  it("POST /user/login should return a 204 error message when the emailaddress does not exist in the database", async function () {
    const user = {
      email: "iets@email.com",
      password: "password"
    };

    const res = await requester.post("/user/login").send(user);

    expect(res).to.have.status(204);
  });

});
