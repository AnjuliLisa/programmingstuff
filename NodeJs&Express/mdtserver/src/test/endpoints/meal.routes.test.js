const requester = require('../../../requester.test');
const chai = require("chai");
const expect = chai.expect;
const Day = require('../../models/day.model')
const Meal = require('../../models/meal.model')
const User = require('../../models/user.model')

describe("Meal endpoints tests", function () {

  let user;
  let token;
  let day;
  let meal;

  this.beforeEach(async function () {
    const body = {
      firstname: "Jos",
      lastname: "Jansen",
      password: "wachtwoord",
      email: "josjansen@mdt.com",
      dateOfBirth_year: 1998,
      dateOfBirth_month: 12,
      dateOfBirth_day: 1,
      gender: "male"
    }

    const res = await requester
      .post('/user/register')
      .send(body)

    token = res.body.token;
    user = await User.findOne()

    day = new Day({
      date_year: 2020,
      date_month: 2,
      date_day: 23,
      userid: user._id
    })

    await day.save();

    meal = new Meal({
      dayid: day._id,
      name: 'Pannenkoeken',
      rating: 50,
      type: 'Breakfast',
      time: new Date()
    })

    await meal.save()
  });

  it("POST /meal/ should add a meal", async function () {
    const body = {
      dayid: day._id,
      name: 'Tosti',
      rating: 100,
      type: 'Lunch',
      time: new Date()
    }
    const res = await requester
      .post(`/meal`)
      .set("Authorization", "Bearer " + token)
      .send(body);

    expect(res).to.have.status(200);
    expect(res.body).to.have.property("_id");
    expect(res.body).to.have.property('name', 'Tosti');
    expect(res.body).to.have.property("rating", 100);
  });


  it("DELETE /meal/:id should delete a meal", async function () {
    const res = await requester
      .delete(`/meal/${meal._id}`)
      .set("Authorization", "Bearer " + token);

    expect(res).to.have.status(200);
  });

  it("GET /meal/day/:id should get all meals with a given day id", async function () {
    const res = await requester
      .get(`/meal/day/${day._id}`)
      .set("Authorization", "Bearer " + token);
    expect(res).to.have.status(200);
    expect(res.body).to.have.length(1);
  });

  it('GET /meal/:id should get a meal by id', async function () {
    const res = await requester
      .get(`/meal/${meal._id}`)
      .set("Authorization", "Bearer " + token)
    expect(res).to.have.status(200);
    expect(res.body).to.have.property("_id");
    expect(res.body).to.have.property('name', 'Pannenkoeken');
    expect(res.body).to.have.property("rating", 50);
  })

  it('PUT /meal/:id should update a meal', async function () {
    const body = {
      _id: meal._id,
      dayid: day._id,
      name: 'Tosti',
      rating: 100,
      type: 'Lunch',
      time: new Date()
    }
    const res = await requester.put(`/meal/${meal._id}`).set("Authorization", "Bearer " + token).send(body)
    expect(res).to.have.status(200)
    expect(res.body).to.have.property('_id')
    expect(res.body).to.have.property('name', 'Tosti')
    expect(res.body).to.have.property('type', 'Lunch')
  })

});
