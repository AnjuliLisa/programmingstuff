const express = require("express");
const app = express();
const cors = require("cors");

const UserRoutes = require("../routes/user.routes");
const DayRoutes = require("../routes/day.routes");
const DrinkRoutes = require("../routes/drink.routes");
const MealRoutes = require("../routes/meal.routes");

const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json());
app.use(cors({ credentials: true, origin: true }));

app.use("/user", UserRoutes);
app.use("/day", DayRoutes);
app.use("/drink", DrinkRoutes);
app.use("/meal", MealRoutes);

module.exports = app;
