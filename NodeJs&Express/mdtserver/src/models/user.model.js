const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    firstname: {
        type: String,
        required: [true, 'A user needs to have a firstname']
    },
    lastname: {
        type: String,
        required: [true, 'A user needs to have a lastname']
    },
    password: {
        type: String,
        required: [true, 'A user needs to have a password']
    },
    email: {
        type: String,
        required: [true, 'A user needs to have a emailaddress']
    },
    dateOfBirth_year: {
        type: Number,
        required: [true, 'A user requires a date of birth year'],
        range: [{
            type: Number,
            min: 1900
        }],
    },
    dateOfBirth_month: {
        type: Number,
        required: [true, 'A user requires a date of birth month'],
        enum: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    },
    dateOfBirth_day: {
        type: Number,
        required: [true, 'A day requires a date of birth day'],
        range: [{
            type: Number,
            min: 1,
            max: 31
        }],
    },
    gender: {
        type: String,
        required: [true, 'A user need to declare a gender'],
        default: 'not specified',
        enum: ['female', 'male', 'not specified']
    },
    diet: {
        type: [String],
        default: []
    }
});

const User = mongoose.model('user', UserSchema);

module.exports = User;