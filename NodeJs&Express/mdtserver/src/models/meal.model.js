const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const MealSchema = new Schema({
  dayid: {
    type: Schema.Types.ObjectId,
    required: true
  },
  name: String,
  rating: {
    type: Number,
    required: [true, "A meal requires a rating"],
    min: [0, "rating cannot be below 0"],
    max: [100, "rating cannot be above 100"]
  },
  type: {
    type: String,
    enum: ["Breakfast", "Lunch", "Dinner", "Snack"],
    required: [true, "A meal requires a type"]
  },
  time: Date
});

const Meal = mongoose.model('meal', MealSchema)
module.exports = Meal;
