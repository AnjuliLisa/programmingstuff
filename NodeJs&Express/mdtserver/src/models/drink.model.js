const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const DrinkSchema = new Schema({
  dayid: {
    type: Schema.Types.ObjectId,
    required: true
  },
  name: {
    type: String,
    required: [true, "A drink requires a name"]
  },
  quantity: {
    type: Number,
    required: [true, "A drink requires a quantity"]
  },
  time: Date
});

const Drink = mongoose.model('drink', DrinkSchema)

module.exports = Drink;
