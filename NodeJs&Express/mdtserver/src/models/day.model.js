const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const DaySchema = new Schema({
    date_year: {
        type: Number,
        required: [true, 'A day requires a year'],
        range: [{
            type: Number,
            min: 1900
        }],
    },
    date_month: {
        type: Number,
        required: [true, 'A day requires a month'],
        enum: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    },
    date_day: {
        type: Number,
        required: [true, 'A day requires a date_day'],
        range: [{
            type: Number,
            min: 1,
            max: 31
        }],
    },
    userid: {
        type: Schema.Types.ObjectId,
        required: [true, 'a day needs a user id'],
        ref: 'user',
    }
});


const Day = mongoose.model('day', DaySchema)
module.exports = Day