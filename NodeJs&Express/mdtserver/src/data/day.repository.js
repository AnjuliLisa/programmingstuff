const Day = require('../models/day.model');


module.exports = {

    getDayById: async function (dayid) {
        const day = await Day.findOne({ _id: dayid });
        return day;
    },
    getDayByUserIdAndDate: async function (data) {
        try {
            const day = await Day.findOne({
                userid: data.userid,
                date_year: data.date_year,
                date_month: data.date_month,
                date_day: data.date_day
            });
            if (day === null) {
                const newDay = new Day(data)
                await newDay.save()
                return newDay
            }
            return day;
        } catch {
            return null;
        }
    },
    saveDay: async function (day) {
        try {
            const nday = new Day(day);
            await nday.save();
            return nday;
        } catch (err) {
            return null;
        }
    },
    getAllDays: async function (userid) {
        const list = await Day.find({ userid: userid });
        return list;
    },
    deleteDay: async function (dayid) {
        try {
            await Day.deleteOne({ _id: dayid });
            return true;
        } catch {
            return false;
        }
    },
    updateDay: async function (day) {
        try {
            await Day.findByIdAndUpdate(day._id, day);
            const nday = await Day.findById(day._id);
            return nday;
        } catch (err) {
            return null;
        }
    }
}