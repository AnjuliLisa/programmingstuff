const Meal = require('../models/meal.model')

module.exports = {

    getMealById: async function (mealId) {
        const meal = await Meal.findOne({ _id: mealId })
        return meal
    },

    getAllMeals: async function (dayId) {
        const list = await Meal.find({ dayid: dayId })
        return list;
    },

    saveMeal: async function (meal) {
        try {
            const newMeal = new Meal(meal)
            newMeal.save()
            return newMeal
        } catch{
            return null;
        }
    },

    updateMeal: async function (meal) {
        try {
            await Meal.findByIdAndUpdate(meal._id, meal)
            const updatedMeal = await Meal.findOne({ _id: meal._id })
            return updatedMeal
        } catch (err) {
            return null;
        }
    },
    deleteMeal: async function (mealid) {
        try {
            await Meal.deleteOne({ _id: mealid })
            return true;
        } catch{
            return false;
        }
    }
}