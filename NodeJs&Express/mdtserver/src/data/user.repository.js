const User = require('../models/user.model')


module.exports = {
    getUserByEmail: async function (email) {
        const user = await User.findOne({ email: email })
        return user;
    },
    register: async function (user) {
        try {
            const newUser = new User(user);
            await newUser.save();
            return newUser;
        } catch (err) {
            return null;
        }
    },
    getAllUsers: async function () {
        const list = await User.find({}).select({
            _id: 0,
            firstname: 1,
            lastname: 1,
            email: 1
        });
        return list
    },

    changePassword: async function (data) {
        try {
            User.updateOne(
                { _id: data.userid },
                { $set: { password: data.password } }
            );
            return true;
        } catch (err) {
            return false;
        }

    }
}