const Drink = require('../models/drink.model')

module.exports = {

    getDrinkById: async function (drinkId) {
        const drink = await Drink.findOne({ _id: drinkId })
        return drink;
    },

    getAllDrinks: async function (dayId) {
        const list = await Drink.find({ dayid: dayId })
        return list;
    },

    saveDrink: async function (drink) {
        try {
            const newDrink = new Drink(drink)
            newDrink.save();
            return newDrink
        } catch{
            return null;
        }
    },

    updateDrink: async function (drink) {
        try {
            await Drink.findByIdAndUpdate(drink._id, drink)
            const updatedDrink = await Drink.findOne({ _id: drink._id })
            return updatedDrink
        } catch {
            return null;
        }
    },
    deleteDrink: async function (drinkid) {
        try {
            await Drink.deleteOne({ _id: drinkid })
            return true;
        } catch{
            return false;
        }
    }
}