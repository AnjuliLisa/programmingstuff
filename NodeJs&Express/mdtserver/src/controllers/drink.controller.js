const repository = require('../data/drink.repository')

module.exports = {

    addDrink: async function (req, res) {
        const drink = req.body;
        if (!drink.quantity || !drink.name || !drink.dayid) {
            res.status(400).json({ message: 'request body not valid' })
            return
        }
        const savedDrink = await repository.saveDrink(req.body);
        if (savedDrink === null) {
            res.status(500).json({ message: 'Something went wrong...' })
            return
        }
        res.status(200).json(savedDrink)

    },
    getDrinkById: async function (req, res) {
        const drinkid = req.params.id;
        const drink = await repository.getDrinkById(drinkid);
        if (drink === null) {
            res.status(500).json({ message: 'Something went wrong...' })
            return
        }
        res.status(200).json(drink)
        return
    },
    getAllDrinks: async function (req, res) {
        const list = await repository.getAllDrinks(req.params.id);
        if (list === null) {
            res.status(204)
            return
        }
        res.status(200).json(list)

    },
    updateDrink: async function (req, res) {
        const drink = req.body;
        if (!drink.quantity || !drink.name || !drink.dayid) {
            res.status(400).json({ message: 'request body not valid' })
            return
        }
        const updatedDrink = await repository.updateDrink(drink);
        if (updatedDrink === null) {
            res.status(204)
            return
        }
        res.status(200).json(drink)
        return

    },
    deleteDrink: async function (req, res) {
        const drinkid = req.params.id
        const isDeleted = await repository.deleteDrink(drinkid)
        if (isDeleted) {
            res.status(200).json({ message: 'drink is deleted' })
            return
        }
        res.status(500).json('Something went wrong...')
        return
    }

}