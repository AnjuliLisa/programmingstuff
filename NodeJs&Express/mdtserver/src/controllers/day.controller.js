const repository = require('../data/day.repository')



module.exports = {
    getAllDays: async function (req, res) {
        const list = await repository.getAllDays(req.userid);
        if (list.length === 0) {
            res.status(204).end();
            return;
        }

        res.status(200).json(list);
        return;
    },

    getDayById: async function (req, res) {
        try {
            const day = await repository.getDayById(req.params.id);
            if (day === null) {
                res.status(204).end()
                return
            }
            res.status(200).json(day);
            return;
        } catch {
            res.status(204).end()
            return
        }
    },

    getDayByUserIdAndDate: async function (req, res) {
        const data = {
            userid: req.userid,
            date_year: new Date().getFullYear(),
            date_month: new Date().getMonth() + 1,
            date_day: new Date().getDate()
        }

        try {
            const day = await repository.getDayByUserIdAndDate(data);
            if (day === null) {
                res.status(204).end()
                return
            }
            res.status(200).json(day);
            return;
        } catch {
            res.status(204).end()
            return
        }


    },

    addDay: async function (req, res) {
        const data = {
            userid: req.userid,
            date_year: req.body.date_year,
            date_month: req.body.date_month,
            date_day: req.body.date_day
        }
        const day = await repository.saveDay(data)
        if (day === null) {
            res.status(400).json({ message: 'request body is invalid' })
            return
        }
        res.status(200).json(day)
        return
    },

    updateDay: async function (req, res) {
        const data = {
            _id: req.body._id,
            date_year: req.body.date_year,
            date_month: req.body.date_month,
            date_day: req.body.date_day,
            userid: req.userid,
        }
        const result = await repository.updateDay(data);
        if (result !== null) {
            res.status(200).json(result)
            return
        }

        res.status(500).json({ message: 'Something went wrong...' })
        return
    },

    deleteDay: async function (req, res) {

        const result = await repository.deleteDay(req.params.id);
        if (result) {
            res.status(200).json({ message: 'day is deleted' })
            return
        }

        res.status(500).json({ message: 'Something went wrong...' })
        return

    },



}