const repository = require('../data/meal.repository')


module.exports = {
    addMeal: async function (req, res) {
        const meal = req.body;
        if (!meal.rating || !meal.type || !meal.dayid) {
            res.status(400).json({ message: 'request body not valid' })
            return
        }
        const savedMeal = await repository.saveMeal(req.body);
        if (savedMeal === null) {
            res.status(500).json({ message: 'Something went wrong...' })
            return
        }
        res.status(200).json(savedMeal)
    },
    getMealById: async function (req, res) {
        const mealid = req.params.id;
        const meal = await repository.getMealById(mealid);
        if (meal === null) {
            res.status(500).json({ message: 'Something went wrong...' })
            return
        }
        res.status(200).json(meal)
        return
    },
    getAllMeals: async function (req, res) {
        const list = await repository.getAllMeals(req.params.id);
        if (list === null) {
            res.status(204)
            return
        }
        res.status(200).json(list)
    },
    updateMeal: async function (req, res) {
        const meal = req.body;
        if (!meal.rating || !meal.name || !meal.dayid || !meal.type) {
            res.status(400).json({ message: 'request body not valid' })
            return
        }
        const updatedMeal = await repository.updateMeal(meal);
        if (updatedMeal === null) {
            res.status(204)
            return
        }
        res.status(200).json(meal)
        return
    },
    deleteMeal: async function (req, res) {
        const mealid = req.params.id
        const isDeleted = await repository.deleteMeal(mealid)
        if (isDeleted) {
            res.status(200).json({ message: 'meal is deleted' })
            return
        }
        res.status(500).json('Something went wrong...')
        return
    }
}