const jwt = require('jsonwebtoken');
const secretkey = require('../assets/variables').secretkey
const bcrypt = require('bcrypt-nodejs')
const UserRepository = require('../data/user.repository')

module.exports = {

    login: async function (req, res) {
        const user = await UserRepository.getUserByEmail(req.body.email);
        if (user === null) {
            res.status(204).end();
            return;
        }

        const istrue = await bcrypt.compareSync(req.body.password, user.password)
        if (istrue) {
            const payload = { userid: user._id }
            jwt.sign(payload, secretkey, { expiresIn: "10d" }, (err, token) => {
                if (err) {
                    res.status(500).json({ message: 'Something went wrong...' }); return;
                }
                if (token) {
                    res.status(200).json({
                        token: token,
                        user: {
                            firstname: user.firstname,
                            lastname: user.lastname,
                            email: user.email,
                            dateOfBirth_year: user.dateOfBirth_year,
                            dateOfBirth_month: user.dateOfBirth_month,
                            dateOfBirth_day: user.dateOfBirth_day,
                            gender: user.gender,
                            diet: user.diet
                        }
                    });
                    return;
                }
            })
        } else {
            res.status(401).json({ message: 'Invalid password' });
            return;
        }
    },

    register: async function (req, res, next) {
        const user = req.body;
        if (!user.firstname || !user.lastname || !user.password || !user.email || !user.dateOfBirth_year || !user.dateOfBirth_month || !user.dateOfBirth_day || !user.gender) {
            res.status(400).json({ message: 'Invalid request' }); return;
        }
        const existingUser = await UserRepository.getUserByEmail(req.body.email);
        if (existingUser !== null) {
            res.status(400).json({ message: 'User with this emailaddress already exists' }); return;
        }
        const oldpassword = req.body.password
        user.password = await bcrypt.hashSync(user.password);

        const savedUser = await UserRepository.register(req.body);

        if (!savedUser) {
            res.status(500).json({ message: 'Unable to save user' });
            return;
        }

        req.body = {
            email: req.body.email,
            password: oldpassword
        };
        next();
    },

    changePassword: async function (req, res) {
        if (!req.body.password) { res.status(400).json({ message: 'Missing password' }); return; }

        const password = bcrypt.hashSync(req.body.password);
        const data = {
            userid: req.userid,
            password: password
        }
        const isChanged = await UserRepository.changePassword(data);
        if (isChanged) {
            res.status(200).json({ message: 'password is changed' });
            return;
        } else {
            res.status(500).json({ message: 'Something went wrong...' });
            return;
        }
    },

    getAllUsers: async function (req, res) {
        const users = await UserRepository.getAllUsers();
        res.status(200).json(users);
        return;
    },

    validateToken: (req, res, next) => {
        const authHeader = req.headers.authorization;
        if (!authHeader) {
            res.status(401).json({ message: "Authorization header missing!" });
            return;
        }
        let token = authHeader.substring(7, authHeader.length);
        validateToken(token)
            .then(data => {
                req.userid = data.userid;
                req.token = token;
                next();

            })
            .catch((err) => {
                res.status(401).json({ message: 'You are not authorized!' }).end();
                return;
            });
    }


}

const validateToken = (token) => {
    return new Promise((resolve, reject) => {
        jwt.verify(token, secretkey, (err, payload) => {
            if (err) reject(err);
            resolve(payload);
        });
    });
}
