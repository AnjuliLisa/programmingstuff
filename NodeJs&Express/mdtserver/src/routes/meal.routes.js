const express = require("express");
const router = express.Router();
const authController = require('../controllers/user.controller')
const mealController = require('../controllers/meal.controller')

router.post('/', authController.validateToken, mealController.addMeal)
router.get('/day/:id', authController.validateToken, mealController.getAllMeals)
router.get('/:id', authController.validateToken, mealController.getMealById)
router.put('/:id', authController.validateToken, mealController.updateMeal)
router.delete('/:id', authController.validateToken, mealController.deleteMeal)

module.exports = router;
