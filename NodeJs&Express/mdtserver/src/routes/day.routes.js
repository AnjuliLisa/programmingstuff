const express = require("express");
const router = express.Router();
const dayController = require('../controllers/day.controller')
const authController = require('../controllers/user.controller')

router.post('/', authController.validateToken, dayController.addDay)
router.get('/', authController.validateToken, dayController.getAllDays)
router.get('/today', authController.validateToken, dayController.getDayByUserIdAndDate)
router.get('/:id', authController.validateToken, dayController.getDayById)
router.put('/:id', authController.validateToken, dayController.updateDay)
router.delete('/:id', authController.validateToken, dayController.deleteDay)

module.exports = router;
