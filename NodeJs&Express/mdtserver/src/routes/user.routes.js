const express = require("express");
const router = express.Router();
const userController = require('../controllers/user.controller')

router.post("/register", userController.register, userController.login);

router.post("/login", userController.login)

router.put("/password", userController.validateToken, userController.changePassword)

router.get("/", userController.validateToken, userController.getAllUsers);

module.exports = router;
