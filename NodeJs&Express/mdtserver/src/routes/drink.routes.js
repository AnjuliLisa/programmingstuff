const express = require("express");
const router = express.Router();
const authController = require('../controllers/user.controller')

const drinkController = require('../controllers/drink.controller')

router.post('/', authController.validateToken, drinkController.addDrink)
router.get('/day/:id', authController.validateToken, drinkController.getAllDrinks)
router.get('/:id', authController.validateToken, drinkController.getDrinkById)
router.put('/:id', authController.validateToken, drinkController.updateDrink)
router.delete('/:id', authController.validateToken, drinkController.deleteDrink)


module.exports = router;
