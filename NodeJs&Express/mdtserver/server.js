const mongoose = require("mongoose");
const environment = require("./src/enviroments/environment.prod");
const app = require('./src/config/app');

app.on("databaseConnected", function () {
  const port = process.env.PORT || 3000;
  app.listen(port, () => {
    console.log(`server is listening on port ${port}`);
  });
});

mongoose
  .connect(environment.dbUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  })
  .then(() => {
    console.log("MongoDB connection established");

    app.emit("databaseConnected");
  })
  .catch(err => {
    console.log("MongoDB connection failed");
    console.log(err);
  });
