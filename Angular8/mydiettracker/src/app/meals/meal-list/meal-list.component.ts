import { Component, OnInit, OnDestroy } from '@angular/core'
import { Meal } from 'src/models/meal'
import { DashboardService } from 'src/app/services/dashboard.service'
import { Subscription } from 'rxjs'

@Component({
  selector: 'app-meal-list',
  templateUrl: './meal-list.component.html',
  styleUrls: ['./meal-list.component.scss']
})
export class MealListComponent implements OnInit, OnDestroy {
  breakfast: Meal
  lunch: Meal
  dinner: Meal
  snack: Meal
  meals
  subs = new Subscription()

  constructor(private dashboardService: DashboardService) {}

  ngOnInit() {
    this.dashboardService.getMeals()
    this.subs = this.dashboardService.meals.subscribe(meals => {
      if (meals !== null) {
        this.meals = meals
        this.breakfast = meals.find(meal => meal.type === 'Breakfast')
        if (!this.breakfast) {
          this.breakfast = new Meal({ name: '', dayid: 'placeholder', type: 'Breakfast' })
        }
        this.lunch = meals.find(meal => meal.type === 'Lunch')
        if (!this.lunch) {
          this.lunch = new Meal({ name: '', dayid: 'placeholder', type: 'Lunch' })
        }
        this.dinner = meals.find(meal => meal.type === 'Dinner')
        if (!this.dinner) {
          this.dinner = new Meal({ name: '', dayid: 'placeholder', type: 'Dinner' })
        }
        this.snack = meals.find(meal => meal.type === 'Snack')
        if (!this.snack) {
          this.snack = new Meal({ name: '', dayid: 'placeholder', type: 'Snack' })
        }
      }
    })
  }

  ngOnDestroy() {
    this.subs.unsubscribe()
  }
}
