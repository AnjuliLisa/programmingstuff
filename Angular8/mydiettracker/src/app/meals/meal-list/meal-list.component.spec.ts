import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { MealListComponent } from './meal-list.component'
import { Component, Input } from '@angular/core'
import { BehaviorSubject } from 'rxjs'
import { Meal } from 'src/models/meal'
import { DashboardService } from 'src/app/services/dashboard.service'

@Component({ selector: 'app-meal', template: '' })
class MealStubComponent {
  @Input() Meal
}

describe('MealListComponent', () => {
  let component
  let fixture: ComponentFixture<MealListComponent>
  let dashboardServiceStub

  beforeEach(async(() => {
    dashboardServiceStub = {
      getMeals(): void {},
      meals: new BehaviorSubject<Meal[]>(null)
    }

    TestBed.configureTestingModule({
      declarations: [MealListComponent, MealStubComponent],
      providers: [{ provide: DashboardService, useValue: dashboardServiceStub }]
    }).compileComponents()
    fixture = TestBed.createComponent(MealListComponent)
    component = fixture.componentInstance
  }))

  afterEach(() => {
    fixture.destroy()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
