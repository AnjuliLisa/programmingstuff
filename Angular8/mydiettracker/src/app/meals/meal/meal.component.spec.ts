import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { MealComponent } from './meal.component'
import { ReactiveFormsModule, FormsModule } from '@angular/forms'
import { Component, ViewChild } from '@angular/core'
import { Meal } from 'src/models/meal'
import { DashboardService } from 'src/app/services/dashboard.service'
import { AlertService } from 'src/app/alert/alert.service'

@Component({
  selector: `host-component`,
  template: `
    <app-meal Meal=""></app-meal>
  `
})
class TestHostComponent {
  Meal = new Meal({ type: 'Breakfast', name: 'Appel', rating: 50 })
  onSubmit() {}
}

describe('MealComponent', () => {
  let component: TestHostComponent
  let fixture: ComponentFixture<TestHostComponent>
  let dashboardServiceStub
  let alertServiceStub

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MealComponent, TestHostComponent],
      imports: [ReactiveFormsModule, FormsModule],
      providers: [
        { provide: DashboardService, useValue: dashboardServiceStub },
        { provide: AlertService, useValue: alertServiceStub }
      ]
    }).compileComponents()
    fixture = TestBed.createComponent(TestHostComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  }))

  afterEach(() => {
    fixture.destroy()
  })

  it('should create', () => {
    fixture.detectChanges()
    expect(component).toBeTruthy()
  })
  it('should have function onSubmit()', () => {
    fixture.detectChanges()
    expect(component.onSubmit).toBeTruthy()
  })
})
