import { Component, OnInit, Input, OnDestroy } from '@angular/core'
import { Meal } from 'src/models/meal'
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { DashboardService } from 'src/app/services/dashboard.service'
import { AlertService } from 'src/app/alert/alert.service'
import { Subscription } from 'rxjs'

@Component({
  selector: 'app-meal',
  templateUrl: './meal.component.html',
  styleUrls: ['./meal.component.scss']
})
export class MealComponent implements OnInit, OnDestroy {
  @Input() Meal: Meal
  mealForm: FormGroup
  subs = new Subscription()

  constructor(private dashboardService: DashboardService, private alertService: AlertService) {}

  ngOnInit() {
    this.mealForm = new FormGroup({
      name: new FormControl(null)
    })
  }

  saveRating(rtg: number) {
    this.Meal.rating = rtg
  }

  onSubmit() {
    if (this.Meal.name === '' && this.mealForm.value.name === null) {
      this.alertService.error('vul een naam in')
    } else if (this.Meal.rating === undefined) {
      this.alertService.error('vul een waardering in')
    } else {
      if (this.Meal.name === '') {
        this.Meal.name = this.mealForm.value.name
      }
      if (this.Meal._id) {
        this.subs = this.dashboardService.updateMeal(this.Meal).subscribe(response => {
          if (response) {
            this.dashboardService.getMeals()
          }
        })
      } else {
        this.Meal.time = new Date()
        this.subs = this.dashboardService.addMeal(this.Meal).subscribe(res => {
          if (res) {
            this.dashboardService.getMeals()
          }
        })
      }
    }
  }
  ngOnDestroy() {
    this.subs.unsubscribe()
  }
}
