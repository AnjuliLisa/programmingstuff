import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './core/app/app.component'
import { RouterModule } from '@angular/router'
import { NavbarComponent } from './core/navbar/navbar.component'
import { AboutComponent } from './about/about.component'
import { UsecaseComponent } from './about/usecase/usecase.component'
import { UsecaseListComponent } from './about/usecase-list/usecase-list.component'
import { HomeComponent } from './home/home.component'
import { DashboardComponent } from './dashboard/dashboard.component'
import { RegisterComponent } from './auth/register/register.component'
import { LoginComponent } from './auth/login/login.component'
import { HttpClientModule } from '@angular/common/http'
import { DrinkGlassComponent } from './drinks/drink-glass/drink-glass.component'
import { MealComponent } from './meals/meal/meal.component'
import { MealListComponent } from './meals/meal-list/meal-list.component'
import { DrinkAddComponent } from './drinks/drink-add/drink-add.component'
import { DrinkListComponent } from './drinks/drink-list/drink-list.component'
import { DrinksComponent } from './drinks/drinks.component'
import { UserComponent } from './user/user.component'
import { HistoryModule } from './history/history.module'
import { AlertModule } from './alert/alert.module'
import { PageNotFoundComponent } from './page-not-found/page-not-found.component'

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    AboutComponent,
    UsecaseComponent,
    UsecaseListComponent,
    HomeComponent,
    DashboardComponent,
    RegisterComponent,
    DrinkGlassComponent,
    DrinkAddComponent,
    DrinksComponent,
    DrinkListComponent,
    LoginComponent,
    MealComponent,
    MealListComponent,
    UserComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule,
    AlertModule,
    NgbModule,
    HistoryModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
