import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { AboutComponent } from './about/about.component'
import { HomeComponent } from './home/home.component'
import { LoginComponent } from './auth/login/login.component'
import { RegisterComponent } from './auth/register/register.component'
import { DashboardComponent } from './dashboard/dashboard.component'
import { UserComponent } from './user/user.component'
import { HistoryComponent } from './history/history.component'
import { DrinkOverviewComponent } from './history/drink-overview/drink-overview.component'
import { MealOverviewComponent } from './history/meal-overview/meal-overview.component'
import { PageNotFoundComponent } from './page-not-found/page-not-found.component'
import { DayoverviewComponent } from './history/dayoverview/dayoverview.component'

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'dashboard', component: DashboardComponent },
  {
    path: 'history',
    children: [
      { path: '', component: HistoryComponent },
      { path: ':id', component: DayoverviewComponent }
    ]
  },
  { path: 'user', component: UserComponent },
  { path: '**', component: PageNotFoundComponent }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
