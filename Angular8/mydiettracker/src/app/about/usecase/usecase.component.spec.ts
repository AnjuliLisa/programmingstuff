import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { Component } from '@angular/core'
import { UsecaseComponent } from './usecase.component'
import { UseCase } from 'src/models/usecase'

@Component({
  selector: `host-component`,
  template: `
    <app-usecase useCase=".."></app-usecase>
  `
})
class TestHostComponent {
  useCase = new UseCase('id', 'name', 'actor', 'precondition', 'postcondition', ['string', 'array'])
}

describe('UseCaseComponent', () => {
  let component
  let fixture: ComponentFixture<TestHostComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UsecaseComponent, TestHostComponent]
    }).compileComponents()
    fixture = TestBed.createComponent(TestHostComponent)
    component = fixture.componentInstance
  }))

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
