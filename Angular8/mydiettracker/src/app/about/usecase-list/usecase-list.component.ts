import { Component, OnInit } from '@angular/core'
import { UseCase } from '../../../models/usecase'
import { USECASES } from '../../repos/repo-usercases'

@Component({
  selector: 'app-usecase-list',
  templateUrl: './usecase-list.component.html',
  styleUrls: ['./usecase-list.component.scss']
})
export class UsecaseListComponent implements OnInit {
  useCases = USECASES
  constructor() {}

  ngOnInit() {}
}
