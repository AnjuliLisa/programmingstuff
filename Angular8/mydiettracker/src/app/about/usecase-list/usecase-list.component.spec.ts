import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { UsecaseListComponent } from './usecase-list.component'
import { UsecaseComponent } from '../usecase/usecase.component'
import { Component, Input } from '@angular/core'

@Component({ selector: 'app-usecase', template: '' })
class UseCaseStubComponent {
  @Input() useCase
}

describe('UsecaseListComponent', () => {
  let component
  let fixture: ComponentFixture<UsecaseListComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UsecaseListComponent, UseCaseStubComponent]
    }).compileComponents()

    fixture = TestBed.createComponent(UsecaseListComponent)
    component = fixture.componentInstance
  }))

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
