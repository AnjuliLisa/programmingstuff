import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { AboutComponent } from './about.component'
import { UsecaseComponent } from './usecase/usecase.component'
import { UsecaseListComponent } from './usecase-list/usecase-list.component'
import { Component } from '@angular/core'

@Component({ selector: 'app-usecase-list', template: '' })
class UseCaseListStubComponent {}

describe('AboutComponent', () => {
  let component
  let fixture: ComponentFixture<AboutComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AboutComponent, UseCaseListStubComponent]
    }).compileComponents()
    fixture = TestBed.createComponent(AboutComponent)
    component = fixture.componentInstance
  }))

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
