import { UseCase } from '../../models/usecase'

export const USECASES: UseCase[] = [
  {
    id: 'UC-01',
    name: 'Inloggen',
    description: 'Hiermee logt een bestaande gebruiker in.',
    scenario: [
      'Gebruiker vult email en password in en klikt op Login knop.',
      'De applicatie valideert de ingevoerde gegevens.',
      'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.'
    ],
    actor: 'Reguliere gebruiker',
    precondition: 'Geen',
    postcondition: 'De actor is ingelogd'
  },
  {
    id: 'UC-02',
    name: 'Invullen ontbijt/lunch/avondeten/snack',
    description:
      'De gebruiker geeft d.m.v. smileys aan hoe goed de gebruiker zich aan zijn dieet heeft gehouden en vul de naam in van maaltijd',
    scenario: [
      'De gebruiker klikt op één van de smileys per maaltijd',
      'De gebruiker voert de naam van de maaltijd in',
      'De applicatie maakt een nieuwe meal aan met de bijbehorende naam en waardering en slaat deze op',
      'Indien dit goed gaat ziet de gebruiker dat de gekozen waardering uitgelicht is en het invoerveld voor de naam verdwijnt'
    ],
    actor: 'Reguliere gebruiker',
    precondition: 'De actor is ingelogd',
    postcondition: 'De waarderingen voor de maaltijden zijn ingevuld'
  },
  {
    id: 'UC-03',
    name: 'Toevoegen van drankje',
    description: 'De gebruiker kan een nieuw drankje toevoegen',
    scenario: [
      'De gebruiker klikt op de knop voor het toevoegen van een drankje',
      'De gebruiker vult de naam en hoeveelheid in',
      'De gebruiker klikt op toevoegen ',
      'De applicatie voegt het drankje toe aan de verzameling van drankjes voor de huidige dag',
      'De gebruiker ziet dat de totale aantal liters van de dag verhoogd is'
    ],
    actor: 'Reguliere gebruiker',
    precondition: 'De actor is ingelogd',
    postcondition: 'Het aantal liters van de dag is toegenomen met het aantal liters dat is ingevuld.'
  },
  {
    id: 'UC-04',
    name: 'Bekijken van historie (drankjes)',
    description: 'De gebruiker kan alle ingevoerde drankjes per dag terugzien',
    scenario: [
      'De gebruiker klikt op historie in de navigatiebalk',
      'De gebruiker kiest een dag waarvoor hij de maaltijden/drankjes wil zien',
      'De applicatie haalt alle drankjes op bij de gekozen dag',
      'De gebruiker ziet de eerder ingevulde drankjes in een dropdown lijst'
    ],
    actor: 'Reguliere gebruiker',
    precondition: 'De actor is ingelogd',
    postcondition: 'De historie van drankjes van de gebruiker is te zien'
  }
]
