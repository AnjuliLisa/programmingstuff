import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { HistoryComponent } from './history.component'
import { DayoverviewComponent } from './dayoverview/dayoverview.component'
import { DrinkDetailComponent } from './drink-overview/drink-detail/drink-detail.component'
import { MealDetailComponent } from './meal-overview/meal-detail/meal-detail.component'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { DrinkOverviewComponent } from './drink-overview/drink-overview.component'
import { MealOverviewComponent } from './meal-overview/meal-overview.component'
import { BrowserModule } from '@angular/platform-browser'

@NgModule({
  declarations: [
    HistoryComponent,
    DayoverviewComponent,
    DrinkDetailComponent,
    MealDetailComponent,
    DrinkOverviewComponent,
    MealOverviewComponent
  ],
  imports: [BrowserModule, CommonModule, NgbModule, FormsModule, ReactiveFormsModule]
})
export class HistoryModule {}
