import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { ReactiveFormsModule, FormsModule } from '@angular/forms'

import { HistoryComponent } from './history.component'
import { BehaviorSubject } from 'rxjs'
import { Day } from 'src/models/day'
import { HistoryService } from '../services/history.service'
import { RouterTestingModule } from '@angular/router/testing'

describe('HistoryComponent', () => {
  let component: HistoryComponent
  let fixture: ComponentFixture<HistoryComponent>
  let historyServiceStub

  beforeEach(async(() => {
    historyServiceStub = {
      getAllDays(): void {},
      days: new BehaviorSubject<Day[]>(null)
    }
    TestBed.configureTestingModule({
      declarations: [HistoryComponent],
      imports: [ReactiveFormsModule, FormsModule, RouterTestingModule],
      providers: [{ provide: HistoryService, useValue: historyServiceStub }]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
