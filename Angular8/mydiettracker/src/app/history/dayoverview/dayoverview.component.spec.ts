import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { DayoverviewComponent } from './dayoverview.component'
import { Observable } from 'rxjs'
import { Day } from 'src/models/day'
import { of } from 'rxjs'
import { RouterTestingModule } from '@angular/router/testing'
import { HistoryService } from 'src/app/services/history.service'
import { ActivatedRoute, Params } from '@angular/router'
import { Component } from '@angular/core'

@Component({ selector: 'app-meal-overview', template: '' })
class MealOverviewComponentStub {}

@Component({ selector: 'app-drink-overview', template: '' })
class DrinkOverviewComponentStub {}

describe('DayoverviewComponent', () => {
  let component: DayoverviewComponent
  let fixture: ComponentFixture<DayoverviewComponent>
  let historyServiceStub
  let activatedRouteStub
  const day = new Day()

  beforeEach(async(() => {
    historyServiceStub = {
      getDay(): Observable<Day> {
        return of(day)
      }
    }
    activatedRouteStub = {
      params: new Observable<Params>(null)
    }
    TestBed.configureTestingModule({
      declarations: [DayoverviewComponent, MealOverviewComponentStub, DrinkOverviewComponentStub],
      imports: [RouterTestingModule],
      providers: [
        { provide: HistoryService, useValue: historyServiceStub },
        { provide: ActivatedRoute, useValue: { params: of({ id: 123 }) } }
      ]
    }).compileComponents()

    fixture = TestBed.createComponent(DayoverviewComponent)
    component = fixture.componentInstance
  }))

  beforeEach(() => {
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
