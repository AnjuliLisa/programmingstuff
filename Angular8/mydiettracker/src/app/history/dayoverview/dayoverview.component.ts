import { Component, OnInit } from '@angular/core'
import { HistoryService } from 'src/app/services/history.service'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { ActivatedRoute, Router } from '@angular/router'
import { Day } from 'src/models/day'

@Component({
  selector: 'app-dayoverview',
  templateUrl: './dayoverview.component.html',
  styleUrls: ['./dayoverview.component.scss']
})
export class DayoverviewComponent implements OnInit {
  id: string
  day: Day
  private routeSub: any

  constructor(
    private historyService: HistoryService,
    private modalService: NgbModal,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.routeSub = this.route.params.subscribe(params => {
      this.id = params['id']
    })

    this.historyService.getDay(this.id).subscribe(response => {
      if (response) {
        this.day = response
      }
    })
  }

  delete() {
    this.historyService.deleteDay(this.id)
    this.router.navigate(['/history'])
  }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' })
  }

  deleteMeal(id: string) {
    this.historyService.deleteMeal(id)
  }
}
