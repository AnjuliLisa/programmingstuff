import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { DrinkOverviewComponent } from './drink-overview.component'
import { Component, Input } from '@angular/core'
import { BehaviorSubject } from 'rxjs'
import { Drink } from 'src/models/drink'
import { HistoryService } from 'src/app/services/history.service'

@Component({ selector: 'app-drink-detail', template: '' })
class DrinkDetailComponentStub {
  @Input() Drink
}

describe('DrinkOverviewComponent', () => {
  let component: DrinkOverviewComponent
  let fixture: ComponentFixture<DrinkOverviewComponent>
  let historyServiceStub

  beforeEach(async(() => {
    historyServiceStub = {
      getAllDrinks(): void {},
      drinks: new BehaviorSubject<Drink[]>(null)
    }
    TestBed.configureTestingModule({
      declarations: [DrinkOverviewComponent, DrinkDetailComponentStub],
      providers: [{ provide: HistoryService, useValue: historyServiceStub }]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(DrinkOverviewComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
