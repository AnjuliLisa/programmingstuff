import { Component, OnInit, Input } from '@angular/core'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { HistoryService } from 'src/app/services/history.service'
import { Drink } from 'src/models/drink'
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { validator } from '../../../assets/validator/validators'

@Component({
  selector: 'app-drink-overview',
  templateUrl: './drink-overview.component.html',
  styleUrls: ['./drink-overview.component.scss']
})
export class DrinkOverviewComponent implements OnInit {
  @Input() dayid: string
  drink: Drink
  drinks
  deleteForm: FormGroup
  drinkForm: FormGroup

  constructor(private modalService: NgbModal, private historyService: HistoryService) {}

  ngOnInit(): void {
    this.historyService.getAllDrinks(this.dayid)

    this.historyService.drinks.subscribe(data => {
      if (data) {
        this.drinks = data
        this.drink = null
      }
    })
    this.drinkForm = new FormGroup({
      name: new FormControl(null, [Validators.required, validator.validName.bind(this)]),
      quantity: new FormControl(null, [Validators.required, validator.validNumber.bind(this)])
    })

    this.deleteForm = new FormGroup({
      drinkid: new FormControl(null, [Validators.required])
    })
  }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' })
  }

  onSubmit() {
    if (this.drinkForm.valid) {
      const drink = new Drink({
        dayid: this.dayid,
        name: this.drinkForm.value.name,
        quantity: this.drinkForm.value.quantity,
        time: new Date()
      })
      this.historyService.addDrink(drink).subscribe(response => {
        if (response) {
          this.historyService.getAllDrinks(this.dayid)
          this.drink = null
        }
      })
      this.drinkForm.reset()
    }
  }

  onDelete() {
    this.historyService.deleteDrink(this.deleteForm.value.drinkid).subscribe(res => {
      if (res) {
        this.historyService.getAllDrinks(this.dayid)
        this.drink = null
      }
    })
    this.modalService.dismissAll()
  }
}
