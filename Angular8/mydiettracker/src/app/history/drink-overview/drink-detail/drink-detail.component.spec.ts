import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { DrinkDetailComponent } from './drink-detail.component'
import { ReactiveFormsModule, FormsModule } from '@angular/forms'
import { HistoryService } from 'src/app/services/history.service'
import { Drink } from 'src/models/drink'
import { Component } from '@angular/core'
import { AlertService } from 'src/app/alert/alert.service'

@Component({
  selector: `host-component`,
  template: `
    <app-drink-detail Drink=""></app-drink-detail>
  `
})
class TestHostComponent {
  Drink = new Drink({ name: 'Appel', quantity: 50 })
}

describe('DrinkDetailComponent', () => {
  let component: TestHostComponent
  let fixture: ComponentFixture<TestHostComponent>
  let historyServiceStub
  let AlertServiceStub

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DrinkDetailComponent, TestHostComponent],
      imports: [ReactiveFormsModule, FormsModule],
      providers: [
        { provide: HistoryService, useValue: historyServiceStub },
        { provide: AlertService, useValue: AlertServiceStub }
      ]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
