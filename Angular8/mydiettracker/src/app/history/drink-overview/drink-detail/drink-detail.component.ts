import { Component, OnInit, Input } from '@angular/core'
import { Drink } from 'src/models/drink'
import { HistoryService } from 'src/app/services/history.service'
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { validator } from '../../../../assets/validator/validators'
import { AlertService } from 'src/app/alert/alert.service'

@Component({
  selector: 'app-drink-detail',
  templateUrl: './drink-detail.component.html',
  styleUrls: ['./drink-detail.component.scss']
})
export class DrinkDetailComponent implements OnInit {
  @Input() Drink: Drink
  form: FormGroup

  constructor(private historyService: HistoryService, private alertService: AlertService) {}

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl(null, [Validators.required, validator.validName.bind(this)]),
      quantity: new FormControl(null, [Validators.required, validator.validNumber.bind(this)])
    })
  }

  onSubmit() {
    if (this.Drink.name === '' || this.Drink.quantity === undefined) {
      this.alertService.error('Naam en hoeveelheid mogen niet leeg zijn')
    } else {
      this.historyService.updateDrink(this.Drink).subscribe(response => {
        if (response) {
          this.historyService.getAllDrinks(this.Drink.dayid)
        }
      })
    }
  }
}
