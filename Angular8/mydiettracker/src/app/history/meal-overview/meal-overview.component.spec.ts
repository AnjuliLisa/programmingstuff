import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { MealOverviewComponent } from './meal-overview.component'
import { Component, Input } from '@angular/core'
import { HistoryService } from 'src/app/services/history.service'
import { BehaviorSubject } from 'rxjs'
import { Meal } from 'src/models/meal'
import { ReactiveFormsModule, FormsModule } from '@angular/forms'
@Component({ selector: 'app-meal-detail', template: '' })
class MealDetailComponentStub {
  @Input() Meal
}

describe('MealOverviewComponent', () => {
  let component: MealOverviewComponent
  let fixture: ComponentFixture<MealOverviewComponent>
  let historyServiceStub

  beforeEach(async(() => {
    historyServiceStub = {
      getAllMeals(): void {},
      meals: new BehaviorSubject<Meal[]>(null)
    }

    TestBed.configureTestingModule({
      declarations: [MealOverviewComponent, MealDetailComponentStub],
      imports: [ReactiveFormsModule, FormsModule],
      providers: [{ provide: HistoryService, useValue: historyServiceStub }]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(MealOverviewComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
