import { Component, OnInit, OnDestroy, Input } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { Meal } from 'src/models/meal'
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { HistoryService } from 'src/app/services/history.service'
import { validator } from '../../../assets/validator/validators'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'

@Component({
  selector: 'app-meal-overview',
  templateUrl: './meal-overview.component.html',
  styleUrls: ['./meal-overview.component.scss']
})
export class MealOverviewComponent implements OnInit, OnDestroy {
  @Input() dayid: string
  meal: Meal
  meals
  deleteForm: FormGroup
  mealForm: FormGroup
  ratings = [0, 25, 50, 75, 100]
  types = ['Breakfast', 'Lunch', 'Dinner', 'Snack']

  constructor(private historyService: HistoryService, private modalService: NgbModal) {}

  ngOnInit() {
    this.historyService.getAllMeals(this.dayid)
    this.historyService.meals.subscribe(data => {
      if (data) {
        this.meals = data
        this.meal = null
      }
    })
    this.mealForm = new FormGroup({
      name: new FormControl(null, [Validators.required, validator.validName.bind(this)]),
      rating: new FormControl(null, Validators.required),
      type: new FormControl(null, Validators.required)
    })
    this.deleteForm = new FormGroup({
      mealid: new FormControl(null, [Validators.required])
    })
  }

  onSubmit() {
    if (this.mealForm.valid) {
      const meal = new Meal({
        dayid: this.dayid,
        name: this.mealForm.value.name,
        rating: this.mealForm.value.rating,
        type: this.mealForm.value.type,
        time: new Date()
      })
      this.historyService.addMeal(meal).subscribe(response => {
        if (response) {
          this.historyService.getAllMeals(this.dayid)
          this.meal = null
        }
      })
      this.mealForm.reset()
    }
  }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' })
  }

  onDelete() {
    this.historyService.deleteMeal(this.deleteForm.value.mealid).subscribe(res => {
      if (res) {
        this.historyService.getAllMeals(this.dayid)
        this.meal = null
      }
    })
    this.modalService.dismissAll()
  }

  ngOnDestroy() {}
}
