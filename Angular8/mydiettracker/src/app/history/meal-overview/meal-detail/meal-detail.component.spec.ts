import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { MealDetailComponent } from './meal-detail.component'
import { Meal } from 'src/models/meal'
import { Component } from '@angular/core'
import { ReactiveFormsModule, FormsModule } from '@angular/forms'
import { HistoryService } from 'src/app/services/history.service'
import { AlertService } from 'src/app/alert/alert.service'

@Component({
  selector: `host-component`,
  template: `
    <app-meal-detail Meal=""></app-meal-detail>
  `
})
class TestHostComponent {
  input = new Meal({ type: 'Breakfast', name: 'Appel', rating: 50 })
}

describe('MealDetailComponent', () => {
  let component: TestHostComponent
  let fixture: ComponentFixture<TestHostComponent>
  let historyServiceStub
  let alertServiceStub

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MealDetailComponent, TestHostComponent],
      imports: [ReactiveFormsModule, FormsModule],
      providers: [
        { provide: HistoryService, useValue: historyServiceStub },
        { provide: AlertService, useValue: alertServiceStub }
      ]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
