import { Component, OnInit, Input } from '@angular/core'
import { Meal } from 'src/models/meal'
import { HistoryService } from 'src/app/services/history.service'
import { AlertService } from 'src/app/alert/alert.service'

@Component({
  selector: 'app-meal-detail',
  templateUrl: './meal-detail.component.html',
  styleUrls: ['./meal-detail.component.scss']
})
export class MealDetailComponent implements OnInit {
  @Input() Meal: Meal
  ratings = [0, 25, 50, 75, 100]
  types = ['Breakfast', 'Lunch', 'Dinner', 'Snack']
  constructor(private historyService: HistoryService, private alertService: AlertService) {}

  ngOnInit() {}

  onSubmit() {
    if (this.Meal.name === '' || this.Meal.rating === undefined || this.Meal.type === undefined) {
      this.alertService.error('Naam, waardering en type moeten ingevuld zijn')
    } else {
      this.historyService.updateMeal(this.Meal).subscribe(response => {
        if (response) {
          this.historyService.getAllMeals(this.Meal.dayid)
        }
      })
    }
  }
}
