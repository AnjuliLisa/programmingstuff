import { Component, OnInit, OnDestroy } from '@angular/core'
import { HistoryService } from '../services/history.service'
import { Day } from 'src/models/day'
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap'
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { validator } from 'src/assets/validator/validators'
import { Router } from '@angular/router'
import { Subscription } from 'rxjs'

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit, OnDestroy {
  days
  day
  daynumbers = [
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
    24,
    25,
    26,
    27,
    28,
    29,
    30,
    31
  ]
  monthnumbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
  addDayForm: FormGroup
  allDaysForm: FormGroup
  addsub = new Subscription()
  subs = new Subscription()

  constructor(
    private historyService: HistoryService,
    private modalService: NgbModal,
    private router: Router
  ) {}

  ngOnInit() {
    this.historyService.getAllDays()
    this.subs = this.historyService.days.subscribe(data => {
      if (data) {
        this.days = data
      }
    })
    this.allDaysForm = new FormGroup({
      currentDay: new FormControl(null, Validators.required)
    })

    this.addDayForm = new FormGroup({
      day: new FormControl(null, Validators.required),
      month: new FormControl(null, Validators.required),
      year: new FormControl(null, [Validators.required, validator.validNumber.bind(this)])
    })
  }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' })
  }

  addDay() {
    if (this.addDayForm.valid) {
      const day = new Day({
        date_year: this.addDayForm.value.year,
        date_month: this.addDayForm.value.month,
        date_day: this.addDayForm.value.day
      })
      this.addsub = this.historyService.addDay(day).subscribe(response => {
        if (response) {
          this.historyService.getAllDays()
        }
      })
      this.addDayForm.reset()
    }
  }

  goToDetails() {
    if (this.allDaysForm.valid) {
      this.router.navigate(['/history', this.allDaysForm.value.currentDay])
    }
  }
  ngOnDestroy() {
    this.addsub.unsubscribe()
    this.subs.unsubscribe()
  }
}
