import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { Component } from '@angular/core'
import { AlertComponent } from './alert.component'
import { RouterTestingModule } from '@angular/router/testing'
import { AlertService } from './alert.service'

@Component({
  selector: `host-component`,
  template: `
    <alert id="." fade="true"></alert>
  `
})
class TestHostComponent {
  id = 'default-alert'
  fade = true
}

describe('AlertComponent', () => {
  let component
  let fixture: ComponentFixture<TestHostComponent>
  let alertServiceSpy

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AlertComponent, TestHostComponent],
      imports: [RouterTestingModule],
      providers: [{ provide: AlertService, useValue: alertServiceSpy }]
    }).compileComponents()
    fixture = TestBed.createComponent(TestHostComponent)
    component = fixture.componentInstance
  }))

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
