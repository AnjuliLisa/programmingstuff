import { Component, OnInit, OnDestroy } from '@angular/core'
import { AuthService } from '../../services/auth.service'
import { Subscription } from 'rxjs'
import { Router } from '@angular/router'

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit, OnDestroy {
  subs: Subscription
  loggedIn: boolean
  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit() {
    this.subs = this.authService.userIsLoggedIn.subscribe(alreadyLoggedIn => {
      if (alreadyLoggedIn) {
        this.loggedIn = true
      } else {
        this.loggedIn = false
      }
    })
  }

  ngOnDestroy() {
    if (this.subs) {
      this.subs.unsubscribe()
    }
  }

  logOut() {
    this.authService.logout()
    this.loggedIn = false
    this.router.navigate(['/home'])
  }
}
