import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { NavbarComponent } from './navbar.component'
import { RouterTestingModule } from '@angular/router/testing'
import { of, BehaviorSubject } from 'rxjs'
import { AuthService } from 'src/app/services/auth.service'

describe('NavbarComponent', () => {
  let component: NavbarComponent
  let fixture: ComponentFixture<NavbarComponent>
  let authServiceStub: Partial<AuthService>

  beforeEach(async(() => {
    authServiceStub = {
      userIsLoggedIn: new BehaviorSubject<boolean>(true)
    }

    TestBed.configureTestingModule({
      declarations: [NavbarComponent],
      imports: [RouterTestingModule],
      providers: [{ provide: AuthService, useValue: authServiceStub }]
    }).compileComponents()
    fixture = TestBed.createComponent(NavbarComponent)
    component = fixture.componentInstance
  }))

  afterEach(() => {
    fixture.destroy()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should show logout button if user is logged in', async(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges()

      const compiled = fixture.debugElement.nativeElement
      expect(compiled.querySelector('#logoutButton')).toBeTruthy()
    })
  }))
})
