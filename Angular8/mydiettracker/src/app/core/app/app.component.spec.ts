import { TestBed, async, ComponentFixture } from '@angular/core/testing'
import { AppComponent } from './app.component'

import { Component } from '@angular/core'

@Component({ selector: 'router-outlet', template: '' })
class RouterOutletStubComponent {}

@Component({ selector: 'app-navbar', template: '' })
class NavbarStubComponent {}

@Component({ selector: 'alert', template: '' })
class AlertStubComponent {}

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>
  let app

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AppComponent, AlertStubComponent, NavbarStubComponent, RouterOutletStubComponent]
    }).compileComponents()
    fixture = TestBed.createComponent(AppComponent)
    app = fixture.componentInstance
  }))

  afterEach(() => {
    fixture.destroy()
  })

  it('should create the app', () => {
    fixture.detectChanges()
    expect(app).toBeTruthy()
  })

  it(`should have as title 'My Diet Tracker'`, () => {
    fixture.detectChanges()
    expect(app.title).toEqual('My Diet Tracker')
  })
})
