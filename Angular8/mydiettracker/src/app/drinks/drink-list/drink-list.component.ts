import { Component, OnInit, OnDestroy } from '@angular/core'
import { DashboardService } from 'src/app/services/dashboard.service'

@Component({
  selector: 'app-drink-list',
  templateUrl: './drink-list.component.html',
  styleUrls: ['./drink-list.component.scss']
})
export class DrinkListComponent implements OnInit, OnDestroy {
  drinks
  subs

  constructor(private dashboardService: DashboardService) {}

  ngOnInit() {
    //get all drinks
    this.subs = this.dashboardService.drinks.subscribe(data => {
      if (data) {
        this.drinks = data
      }
    })
  }
  ngOnDestroy() {
    this.subs.unsubscribe()
  }
}
