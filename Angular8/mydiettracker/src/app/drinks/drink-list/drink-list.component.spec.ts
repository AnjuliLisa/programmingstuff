import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { DrinkListComponent } from './drink-list.component'
import { Component } from '@angular/core'
import { DashboardService } from 'src/app/services/dashboard.service'
import { BehaviorSubject } from 'rxjs'
import { Drink } from 'src/models/drink'

@Component({ selector: 'app-drink-add', template: '' })
class DrinkAddStubComponent {}

describe('DrinkListComponent', () => {
  let component
  let fixture: ComponentFixture<DrinkListComponent>
  let dashboardServiceStub: Partial<DashboardService>

  beforeEach(async(() => {
    dashboardServiceStub = {
      drinks: new BehaviorSubject<Drink[]>(null)
    }

    TestBed.configureTestingModule({
      declarations: [DrinkListComponent, DrinkAddStubComponent],
      providers: [{ provide: DashboardService, useValue: dashboardServiceStub }]
    }).compileComponents()
    fixture = TestBed.createComponent(DrinkListComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  }))

  afterEach(() => {
    fixture.destroy()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
