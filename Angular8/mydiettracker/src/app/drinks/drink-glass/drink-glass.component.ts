import { Component, OnInit, OnDestroy } from '@angular/core'
import { DashboardService } from 'src/app/services/dashboard.service'
import { Subscription } from 'rxjs'

@Component({
  selector: 'app-drink-glass',
  templateUrl: './drink-glass.component.html',
  styleUrls: ['./drink-glass.component.scss']
})
export class DrinkGlassComponent implements OnInit, OnDestroy {
  total = 0
  constructor(private dashboardService: DashboardService) {}
  subs = new Subscription()

  ngOnInit() {
    this.subs = this.dashboardService.quantity.subscribe(data => {
      if (data) {
        this.total = data
      }
    })
  }
  ngOnDestroy() {
    this.subs.unsubscribe()
  }
}
