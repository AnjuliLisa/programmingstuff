import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { DrinkGlassComponent } from './drink-glass.component'
import { DashboardService } from 'src/app/services/dashboard.service'
import { BehaviorSubject } from 'rxjs'

describe('DrinkGlassComponent', () => {
  let component
  let fixture: ComponentFixture<DrinkGlassComponent>
  let dashboardServiceStub: Partial<DashboardService>

  beforeEach(async(() => {
    dashboardServiceStub = {
      quantity: new BehaviorSubject<number>(0)
    }
    TestBed.configureTestingModule({
      declarations: [DrinkGlassComponent],
      providers: [{ provide: DashboardService, useValue: dashboardServiceStub }]
    }).compileComponents()

    fixture = TestBed.createComponent(DrinkGlassComponent)
    component = fixture.componentInstance
  }))
  afterEach(() => {
    fixture.destroy()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
