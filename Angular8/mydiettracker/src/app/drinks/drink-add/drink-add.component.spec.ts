import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { DrinkAddComponent } from './drink-add.component'
import { ReactiveFormsModule, FormsModule } from '@angular/forms'
import { DashboardService } from 'src/app/services/dashboard.service'

describe('DrinkAddComponent', () => {
  let component
  let fixture: ComponentFixture<DrinkAddComponent>
  let dashboardServiceSpy

  beforeEach(async(() => {
    dashboardServiceSpy = jasmine.createSpyObj('dashboardService', ['addDrink'])

    TestBed.configureTestingModule({
      declarations: [DrinkAddComponent],
      imports: [ReactiveFormsModule, FormsModule],
      providers: [{ provide: DashboardService, useValue: dashboardServiceSpy }]
    }).compileComponents()
    fixture = TestBed.createComponent(DrinkAddComponent)
    component = fixture.componentInstance
  }))

  afterEach(() => {
    fixture.destroy()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
