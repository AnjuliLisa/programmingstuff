import { Component, OnInit } from '@angular/core'
import { FormGroup, Validators, FormControl } from '@angular/forms'
import { Drink } from 'src/models/drink'
import { DashboardService } from 'src/app/services/dashboard.service'
import { validator } from 'src/assets/validator/validators'

@Component({
  selector: 'app-drink-add',
  templateUrl: './drink-add.component.html',
  styleUrls: ['./drink-add.component.scss']
})
export class DrinkAddComponent implements OnInit {
  drinkForm: FormGroup

  constructor(private dashboardService: DashboardService) {}

  ngOnInit() {
    this.drinkForm = new FormGroup({
      name: new FormControl(null, [Validators.required, validator.validName.bind(this)]),
      quantity: new FormControl(null, [Validators.required, validator.validNumber.bind(this)])
    })
  }

  onSubmit() {
    if (this.drinkForm.valid) {
      const drink = new Drink({
        dayid: 'placeholder',
        name: this.drinkForm.value.name,
        quantity: this.drinkForm.value.quantity,
        time: new Date()
      })
      this.dashboardService.addDrink(drink)
      this.drinkForm.reset()
    }
  }
}
