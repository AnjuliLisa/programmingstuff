import { Component, OnInit } from '@angular/core'
import { DashboardService } from '../services/dashboard.service'

@Component({
  selector: 'app-drinks',
  templateUrl: './drinks.component.html',
  styleUrls: ['./drinks.component.scss']
})
export class DrinksComponent implements OnInit {
  constructor(private dashboardService: DashboardService) {}

  ngOnInit() {
    this.dashboardService.getDrinks()
  }
}
