import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { DrinksComponent } from './drinks.component'
import { Component } from '@angular/core'
import { DashboardService } from '../services/dashboard.service'

@Component({ selector: 'app-drink-glass', template: '' })
class DrinkGlassStubComponent {}

@Component({ selector: 'app-drink-list', template: '' })
class DrinkListtubComponent {}

describe('DrinksComponent', () => {
  let component
  let fixture: ComponentFixture<DrinksComponent>
  let dashboardServiceSpy

  beforeEach(async(() => {
    dashboardServiceSpy = jasmine.createSpyObj('dashboardService', ['getDrinks'])

    TestBed.configureTestingModule({
      declarations: [DrinksComponent, DrinkGlassStubComponent, DrinkListtubComponent],
      providers: [{ provide: DashboardService, useValue: dashboardServiceSpy }]
    }).compileComponents()

    fixture = TestBed.createComponent(DrinksComponent)
    component = fixture.componentInstance
  }))

  afterEach(() => {
    fixture.destroy()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
