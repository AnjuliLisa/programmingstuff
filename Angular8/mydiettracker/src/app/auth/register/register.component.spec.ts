import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { RegisterComponent } from './register.component'
import { AuthService } from '../../services/auth.service'
import { BehaviorSubject } from 'rxjs'
import { ReactiveFormsModule } from '@angular/forms'
import { RouterTestingModule } from '@angular/router/testing'

describe('RegisterComponent', () => {
  let component
  let fixture: ComponentFixture<RegisterComponent>
  let authServiceStub: Partial<AuthService>

  beforeEach(async(() => {
    authServiceStub = {
      userIsLoggedIn: new BehaviorSubject<boolean>(true)
    }
    TestBed.configureTestingModule({
      declarations: [RegisterComponent],
      imports: [ReactiveFormsModule, RouterTestingModule],
      providers: [{ provide: AuthService, useValue: authServiceStub }]
    }).compileComponents()
    fixture = TestBed.createComponent(RegisterComponent)
    component = fixture.componentInstance
  }))
  afterEach(() => {
    fixture.destroy()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
