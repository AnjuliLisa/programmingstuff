import { Component, OnInit } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { AuthService } from '../../services/auth.service'
import { Router } from '@angular/router'
import { validator } from 'src/assets/validator/validators'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup
  days = [
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
    24,
    25,
    26,
    27,
    28,
    29,
    30,
    31
  ]

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit() {
    this.registerForm = new FormGroup({
      firstname: new FormControl(null, [Validators.required, validator.validName.bind(this)]),
      lastname: new FormControl(null, [Validators.required, validator.validName.bind(this)]),
      dateOfBirth_day: new FormControl(null, [Validators.required]),
      dateOfBirth_month: new FormControl(null, [Validators.required]),
      dateOfBirth_year: new FormControl(null, [Validators.required, validator.validYear.bind(this)]),
      gender: new FormControl(null, [Validators.required]),
      email: new FormControl(null, [Validators.required, validator.validEmail.bind(this)]),
      password: new FormControl(null, [Validators.required, validator.validPassword.bind(this)]),
      checkFV: new FormControl(null),
      checkLV: new FormControl(null),
      checkGV: new FormControl(null),
      checkKA: new FormControl(null)
    })

    this.authService.userIsLoggedIn.subscribe(alreadyLoggenIn => {
      if (alreadyLoggenIn) {
        this.router.navigate(['/dashboard'])
      }
    })
  }

  onSubmit() {
    if (this.registerForm.valid) {
      const firstname = this.registerForm.value.firstname
      const lastname = this.registerForm.value.lastname
      const email = this.registerForm.value.email
      const password = this.registerForm.value.password
      const gender = this.registerForm.value.gender
      const dateOfBirth_day = parseInt(this.registerForm.value.dateOfBirth_day)
      let dateOfBirth_month = parseInt(this.registerForm.value.dateOfBirth_month)
      const dateOfBirth_year = parseInt(this.registerForm.value.dateOfBirth_year)
      const diet = []

      if (this.registerForm.value.checkLV) {
        diet.push('Lactosevrij')
      }
      if (this.registerForm.value.checkGV) {
        diet.push('Glutenvrij')
      }
      if (this.registerForm.value.checkFV) {
        diet.push('Fructosevrij')
      }
      if (this.registerForm.value.checkKA) {
        diet.push('Koolhydraatarm')
      }

      this.authService
        .register(
          firstname,
          lastname,
          password,
          email,
          dateOfBirth_year,
          dateOfBirth_month,
          dateOfBirth_day,
          gender,
          diet
        )
        .subscribe(response => {
          if (response) {
            this.router.navigate(['/dashboard'])
          }
        })
    }
  }
}
