import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { LoginComponent } from './login.component'
import { ReactiveFormsModule } from '@angular/forms'
import { AuthService } from '../../services/auth.service'
import { BehaviorSubject } from 'rxjs'
import { RouterTestingModule } from '@angular/router/testing'

describe('LoginComponent', () => {
  let component
  let fixture: ComponentFixture<LoginComponent>
  let authServiceStub: Partial<AuthService>

  beforeEach(async(() => {
    authServiceStub = {
      userIsLoggedIn: new BehaviorSubject<boolean>(true)
    }

    TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [ReactiveFormsModule, RouterTestingModule],
      providers: [{ provide: AuthService, useValue: authServiceStub }]
    }).compileComponents()

    fixture = TestBed.createComponent(LoginComponent)
    component = fixture.componentInstance
  }))

  afterEach(() => {
    fixture.destroy()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
