import { Component, OnInit } from '@angular/core'
import { AuthService } from '../../services/auth.service'
import { Router } from '@angular/router'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { validator } from 'src/assets/validator/validators'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup
  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl(null, [Validators.required, validator.validEmail.bind(this)]),
      password: new FormControl(null, [Validators.required, validator.validPassword.bind(this)])
    })

    this.authService.userIsLoggedIn.subscribe(alreadyLoggedIn => {
      if (alreadyLoggedIn) {
        this.router.navigate(['/dashboard'])
      }
    })
  }

  onSubmit() {
    if (this.loginForm.valid) {
      const email = this.loginForm.value.email
      const password = this.loginForm.value.password
      this.authService.login(email, password).subscribe({
        next: (response: any) => {
          if (response) {
            this.router.navigate(['/dashboard'])
          }
        }
      })
    }
  }
}
