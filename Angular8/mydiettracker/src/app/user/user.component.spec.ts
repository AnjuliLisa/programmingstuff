import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { UserComponent } from './user.component'
import { RouterTestingModule } from '@angular/router/testing'
import { Observable, of } from 'rxjs'
import { User } from 'src/models/user'
import { UserService } from '../services/user.service'

describe('UserComponent', () => {
  let component: UserComponent
  let fixture: ComponentFixture<UserComponent>
  let userServiceStub
  const users = []

  beforeEach(async(() => {
    userServiceStub = {
      getAllUsers(): Observable<User[]> {
        return of(users)
      }
    }
    TestBed.configureTestingModule({
      declarations: [UserComponent],
      imports: [RouterTestingModule],
      providers: [{ provide: UserService, useValue: userServiceStub }]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(UserComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
