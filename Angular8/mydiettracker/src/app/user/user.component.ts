import { Component, OnInit } from '@angular/core'
import { UserService } from '../services/user.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  users

  constructor(private userService: UserService, private router: Router) {}

  ngOnInit() {
    this.userService.getAllUsers().subscribe({
      next: data => {
        this.users = data
      },
      error: message => {
        this.router.navigate(['/login'])
      }
    })
  }
}
