import { TestBed, async, getTestBed } from '@angular/core/testing'
import { AuthService } from './auth.service'
import { throwError, of } from 'rxjs'
import { RouterTestingModule } from '@angular/router/testing'
import { Router } from '@angular/router'
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'

describe('AuthService', () => {
  let httpClientSpy: { get: jasmine.Spy; post: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy }
  let service: AuthService
  let alertServiceSpy

  beforeEach(() => {
    alertServiceSpy = jasmine.createSpyObj('AlertService', ['error'])
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete'])
    const routerSpy = jasmine.createSpyObj('Router', ['navigate'])

    httpClientSpy.get.and.returnValue(
      of([
        { user: { name: 'Lisa' }, token: 'token' },
        { user: { name: 'Gijs' }, token: 'token' }
      ])
    )

    service = new AuthService(alertServiceSpy, httpClientSpy as any, routerSpy)
    TestBed.configureTestingModule({})
  })

  it('should be created', () => {
    expect(service).toBeTruthy()
  })
  it('should login', () => {
    const email = 'lisa@appels.nl'
    const password = 'Appels'
    httpClientSpy.post.and.returnValue(of({ user: { name: 'Lisa' }, token: 'token' }))

    const subs = service.login(email, password).subscribe(response => {
      expect(response).toEqual({ user: { name: 'Lisa' }, token: 'token' })
    })
    expect(httpClientSpy.post.calls.count()).toBe(1, 'one call')
    subs.unsubscribe()
  })

  it('should register', () => {
    const firstname = 'Lisa'
    const lastname = 'Appels'
    const password = 'Appels'
    const email = 'lisa@appels.nl'
    const dateOfBirth_year = 1998
    const dateOfBirth_month = 11
    const dateOfBirth_day = 22
    const gender = 'female'
    const diet = ['fructosevrij', 'glutenvrij']

    httpClientSpy.post.and.returnValue(of({ user: { name: 'Lisa' }, token: 'token' }))

    const subs = service
      .register(
        firstname,
        lastname,
        password,
        email,
        dateOfBirth_year,
        dateOfBirth_month,
        dateOfBirth_day,
        gender,
        diet
      )
      .subscribe(response => {
        expect(response).toEqual({ user: { name: 'Lisa' }, token: 'token' })
      })
    expect(httpClientSpy.post.calls.count()).toBe(1, 'one call')
    subs.unsubscribe()
  })
})
