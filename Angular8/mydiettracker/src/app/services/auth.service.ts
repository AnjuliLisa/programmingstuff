import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http'
import { BehaviorSubject, Observable, of, throwError } from 'rxjs'
import { User } from '../../models/user'
import { Router } from '@angular/router'
import { environment } from '../../environments/environment.prod'
import { tap, catchError } from 'rxjs/operators'
import { AlertService } from '../alert/alert.service'

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public isLoggedInUser = new BehaviorSubject<boolean>(false)
  private readonly currenttoken = 'token'
  private readonly currentuser = 'currentuser'
  headers = new BehaviorSubject<HttpHeaders>(new HttpHeaders())

  constructor(private alertService: AlertService, private http: HttpClient, private router: Router) {
    this.getCurrentUser().subscribe({
      next: (user: User) => {
        this.isLoggedInUser.next(true)
      },
      error: message => {
        this.isLoggedInUser.next(false)
        this.router.navigate(['/login'])
      }
    })
  }

  //POST login with email and password
  login(email: string, password: string) {
    return this.http
      .post<any>(`${environment.apiUrl}/user/login`, { email, password })
      .pipe(
        catchError(this.handleError),
        tap((response: any) => {
          if (response !== null) {
            const user = new User(response.user)
            this.saveCurrentUser(user, response.token)
            this.isLoggedInUser.next(true)
            this.headers.next(
              new HttpHeaders({
                'Content-Type': 'application/json',
                Authorization: 'Bearer ' + response.token
              })
            )
          }
        })
      )
  }

  //POST register a new user
  register(
    firstname: string,
    lastname: string,
    password: string,
    email: string,
    dateOfBirth_year: number,
    dateOfBirth_month: number,
    dateOfBirth_day: number,
    gender: string,
    diet: string[]
  ) {
    return this.http
      .post(`${environment.apiUrl}/user/register`, {
        firstname,
        lastname,
        password,
        email,
        dateOfBirth_year,
        dateOfBirth_month,
        dateOfBirth_day,
        gender,
        diet
      })
      .pipe(
        catchError(this.handleError),
        tap((response: any) => {
          const currentUser = new User(response.user)
          this.saveCurrentUser(currentUser, response.token)
          this.isLoggedInUser.next(true)
          this.headers.next(
            new HttpHeaders({
              'Content-Type': 'application/json',
              Authorization: 'Bearer ' + response.token
            })
          )
        })
      )
  }

  //Delete setting from localstorage when user clicks on logout
  logout() {
    localStorage.removeItem(this.currenttoken)
    localStorage.removeItem(this.currentuser)
    localStorage.removeItem('day')
    this.isLoggedInUser.next(false)
    this.headers.next(null)
    this.alertService.success('Je bent nu uitgelogd')
  }

  //Save user and token in local storage
  private saveCurrentUser(user: User, token: string): void {
    user.password = '...'
    localStorage.setItem(this.currentuser, JSON.stringify(user))
    localStorage.setItem(this.currenttoken, token)
  }

  //Check if user if logged in
  get userIsLoggedIn(): Observable<boolean> {
    return this.isLoggedInUser.asObservable()
  }

  public getCurrentUser(): Observable<User> {
    return new Observable(observer => {
      const localuser: any = JSON.parse(localStorage.getItem(this.currentuser))
      if (localuser) {
        observer.next(new User(localuser))
        observer.complete()
      } else {
        observer.error('No local user found')
        observer.complete()
      }
    })
  }

  //Retrieve the token saved in localstorage
  public getCurrentToken(): Observable<string> {
    return new Observable(observer => {
      const token: any = localStorage.getItem(this.currenttoken)
      if (token) {
        observer.next(token)
        observer.complete()
      } else {
        observer.error('no token found')
        observer.complete()
      }
    })
  }

  //Handle a http-response error if one occurs
  private handleError = (error: HttpErrorResponse) => {
    if (error.status === 401) {
      this.alertService.error('Verkeerde inloggevens')
    } else {
      this.alertService.error(`Server geeft de volgende melding: ${error.message}`)
    }
    return of(null)
  }
}
