import { TestBed } from '@angular/core/testing'
import { of, Observable, BehaviorSubject } from 'rxjs'
import { DayService } from './day.service'
import { Day } from 'src/models/day'
import { HttpHeaders } from '@angular/common/http'

describe('DayService', () => {
  let httpClientSpy: { get: jasmine.Spy; post: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy }
  let dayService: DayService
  let alertServiceSpy
  let authServiceSpy

  beforeEach(() => {
    alertServiceSpy = jasmine.createSpyObj('alertService', ['error'])
    authServiceSpy = {
      headers: new BehaviorSubject<HttpHeaders>(
        new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + 'token'
        })
      )
    }
    httpClientSpy = jasmine.createSpyObj('httpClient', ['get', 'post', 'put', 'delete'])
    const routerSpy = jasmine.createSpyObj('Router', ['navigate'])

    dayService = new DayService(httpClientSpy as any, authServiceSpy, routerSpy, alertServiceSpy)
    TestBed.configureTestingModule({})
  })

  it('should be created', () => {
    expect(dayService).toBeTruthy()
  })

  it('should get all days', () => {
    const expectedres = [
      new Day({ date_year: 1998, date_month: 12, date_day: 12 }),
      new Day({ date_year: 1998, date_month: 12, date_day: 12 })
    ]

    httpClientSpy.get.and.returnValue(
      of([
        { date_year: 1998, date_month: 12, date_day: 12 },
        { date_year: 1998, date_month: 12, date_day: 12 }
      ])
    )
    const subs = dayService.getAllDays().subscribe(response => {
      expect(response).toEqual(expectedres)
    })

    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call')
    subs.unsubscribe()
  })

  it('should save a day', () => {
    const day = { date_year: 1998, date_month: 12, date_day: 12 }
    const expectedresult = new Day({ _id: 'id', date_year: 1998, date_month: 12, date_day: 12 })

    httpClientSpy.post.and.returnValue(of(expectedresult))
    const subs = dayService.saveDay(new Day(day)).subscribe(response => {
      expect(response).toEqual(expectedresult)
    })
    expect(httpClientSpy.post.calls.count()).toBe(1, 'one call')

    subs.unsubscribe()
  })
})
