import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http'
import { BehaviorSubject, Observable, throwError } from 'rxjs'
import { User } from '../../models/user'
import { Router } from '@angular/router'
import { environment } from '../../environments/environment.prod'
import { map, tap, catchError } from 'rxjs/operators'
import { AuthService } from './auth.service'
import { AlertService } from '../alert/alert.service'

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private headers

  constructor(
    private http: HttpClient,
    private authService: AuthService,
    private router: Router,
    private alertService: AlertService
  ) {
    this.authService.headers.subscribe(data => {
      if (data !== null) {
        this.headers = data
      } else {
        this.router.navigate(['/login'])
      }
    })
  }

  currentUser(): Observable<User> {
    return new Observable(observer => {
      const user: any = JSON.parse(localStorage.getItem('currentuser'))
      if (user) {
        observer.next(new User(user))
        observer.complete()
      } else {
        observer.error('No user in local storage')
        observer.complete()
      }
    })
  }

  getAllUsers() {
    return this.http
      .get<any>(`${environment.apiUrl}/user/`, { headers: this.headers })
      .pipe(
        catchError(this.handleError),
        map(users => users.map(data => new User(data)))
      )
  }

  handleError = (error: HttpErrorResponse) => {
    if (error.status === 401) {
      this.authService.logout()
      this.router.navigate(['/login'])
    } else {
      this.alertService.error(`Server geeft de volgende melding: ${error.message}`)
    }
    return throwError('Probeer het later nogmaals')
  }
}
