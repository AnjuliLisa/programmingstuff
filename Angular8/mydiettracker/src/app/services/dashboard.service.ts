import { Injectable } from '@angular/core'
import { Day } from 'src/models/day'
import { DayService } from './day.service'
import { DrinkService } from './drink.service'
import { MealService } from './meal.service'
import { BehaviorSubject } from 'rxjs'
import { Drink } from 'src/models/drink'
import { Meal } from 'src/models/meal'

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  today: Day
  public drinks = new BehaviorSubject<Drink[]>(null)
  public meals = new BehaviorSubject<Meal[]>(null)
  public quantity = new BehaviorSubject<number>(0)

  constructor(private drinkService: DrinkService, private mealService: MealService) {}

  public getDrinks() {
    this.drinkService.getAllDrinks(this.today._id).subscribe(response => {
      if (response) {
        this.drinks.next(response)
        let amount = 0
        this.drinks.subscribe(data => {
          data.forEach(el => {
            amount += el.quantity
          })
        })
        this.quantity.next(amount)
      }
    })
  }

  public getMeals() {
    this.mealService.getAllMeals(this.today._id).subscribe(response => {
      if (response) {
        this.meals.next(response)
      }
    })
  }

  public addDrink(drink: Drink) {
    drink.dayid = this.today._id
    this.drinkService.saveDrink(drink).subscribe(response => {
      if (response) {
        this.getDrinks()
      }
    })
  }
  public addMeal(meal: Meal) {
    meal.dayid = this.today._id
    return this.mealService.saveMeal(meal)
  }
  public deleteDrink(id: string) {
    this.drinkService.deleteDrink(id)
    this.getDrinks()
  }
  public updateMeal(meal: Meal) {
    return this.mealService.updateMeal(meal)
  }

  clearAll() {
    this.meals.next(null)
    this.drinks.next(null)
    this.quantity.next(null)
    this.today = null
  }
}
