import { TestBed } from '@angular/core/testing'
import { HistoryService } from './history.service'
import { BehaviorSubject } from 'rxjs'
import { Day } from 'src/models/day'
import { DashboardService } from './dashboard.service'

describe('DashboardService', () => {
  let mealServiceSpy
  let drinkServiceSpy
  let service: DashboardService

  beforeEach(() => {
    drinkServiceSpy = jasmine.createSpyObj('drinkService', ['deleteDrink', 'getAllDrinks', 'saveDrink'])
    mealServiceSpy = jasmine.createSpyObj('mealService', ['saveMeal', 'getAllMeals', 'updateMeal'])

    service = new DashboardService(drinkServiceSpy, mealServiceSpy)
    TestBed.configureTestingModule({})
  })

  it('should be created', () => {
    expect(service).toBeTruthy()
  })
})
