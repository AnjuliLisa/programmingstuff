import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http'
import { Router } from '@angular/router'
import { AuthService } from './auth.service'
import { Drink } from 'src/models/drink'
import { environment } from 'src/environments/environment.prod'
import { map, catchError, tap } from 'rxjs/operators'
import { AlertService } from '../alert/alert.service'
import { throwError } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class DrinkService {
  private headers

  constructor(
    private http: HttpClient,
    private authService: AuthService,
    private alertService: AlertService,
    private router: Router
  ) {
    this.authService.headers.subscribe(data => {
      if (data !== null) {
        this.headers = data
      } else {
        this.router.navigate(['/login'])
      }
    })
  }

  public getDrinks(id: string) {
    return this.http
      .get<any>(`${environment.apiUrl}/day/${id}/drinks`, { headers: this.headers })
      .pipe(
        catchError(this.handleError),
        map(drinks => drinks.map(data => new Drink(data)))
      )
  }

  public deleteDrink(id: string) {
    return this.http
      .delete(`${environment.apiUrl}/drink/${id}`, { headers: this.headers })
      .pipe(catchError(this.handleError))
  }

  //POST a new drink
  public saveDrink(drink: Drink) {
    return this.http
      .post<any>(`${environment.apiUrl}/drink/`, JSON.stringify(drink), { headers: this.headers })
      .pipe(
        catchError(this.handleError),
        map(data => new Drink(data))
      )
  }

  public updateDrink(drink: Drink) {
    return this.http
      .put<any>(`${environment.apiUrl}/drink/${drink._id}`, JSON.stringify(drink), { headers: this.headers })
      .pipe(
        catchError(this.handleError),
        map(data => new Drink(data))
      )
  }

  public getAllDrinks(dayid) {
    return this.http
      .get<any>(`${environment.apiUrl}/drink/day/${dayid}`, { headers: this.headers })
      .pipe(
        catchError(this.handleError),
        map(drinks => drinks.map(data => new Drink(data)))
      )
  }

  handleError = (error: HttpErrorResponse) => {
    if (error.status === 401) {
      this.authService.logout()
      this.router.navigate(['/login'])
    } else {
      this.alertService.error(`Server geeft de volgende melding: ${error.message}`)
    }
    return throwError('Probeer het later nogmaals')
  }
}
