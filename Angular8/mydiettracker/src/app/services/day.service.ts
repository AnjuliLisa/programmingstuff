import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http'
import { throwError, BehaviorSubject } from 'rxjs'
import { Day } from '../../models/day'
import { Router } from '@angular/router'
import { environment } from '../../environments/environment.prod'
import { map, tap, catchError } from 'rxjs/operators'
import { AuthService } from './auth.service'
import { AlertService } from '../alert/alert.service'

@Injectable({
  providedIn: 'root'
})
export class DayService {
  private headers
  public day = new BehaviorSubject<Day>(null)

  constructor(
    private http: HttpClient,
    private authService: AuthService,
    private router: Router,
    private alertService: AlertService
  ) {
    this.authService.headers.subscribe(data => {
      if (data !== null) {
        this.headers = data
      } else {
        this.router.navigate(['/login'])
      }
    })
  }

  //POST a new day
  public saveDay(day: Day) {
    return this.http
      .post<any>(`${environment.apiUrl}/day/`, JSON.stringify(day), { headers: this.headers })
      .pipe()
  }

  //GET today
  public getToday() {
    return this.http
      .get<any>(`${environment.apiUrl}/day/today`, { headers: this.headers })
      .pipe(
        catchError(this.handleError),
        tap(data => this.day.next(data))
      )
  }

  //GET single day
  public getDay(id: string) {
    return this.http
      .get<any>(`${environment.apiUrl}/day/${id}`, { headers: this.headers })
      .pipe(
        catchError(this.handleError),
        tap(data => new Day(data))
      )
  }

  //GET all days for logged in user
  public getAllDays() {
    return this.http
      .get<any>(`${environment.apiUrl}/day/`, { headers: this.headers })
      .pipe(
        map(data => data.map(day => new Day(day))),
        catchError(this.handleError)
      )
  }

  //DELETE a day
  public deleteDay(id: string) {
    this.http
      .delete(`${environment.apiUrl}/day/${id}`, { headers: this.headers })
      .pipe(catchError(this.handleError))
      .subscribe()
  }

  handleError = (error: HttpErrorResponse) => {
    if (error.status === 401) {
      this.authService.logout()
      this.router.navigate(['/login'])
    } else {
      this.alertService.error(`Server geeft de volgende melding: ${error.message}`)
    }
    return throwError('Probeer het later nogmaals')
  }
}
