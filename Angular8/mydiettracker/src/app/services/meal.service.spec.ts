import { TestBed } from '@angular/core/testing'
import { of, Observable, BehaviorSubject } from 'rxjs'
import { MealService } from './meal.service'
import { Meal } from 'src/models/meal'
import { HttpHeaders } from '@angular/common/http'

describe('MealService', () => {
  let httpClientSpy: { get: jasmine.Spy; post: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy }
  let mealService: MealService
  let alertServiceSpy
  let authServiceSpy

  beforeEach(() => {
    alertServiceSpy = jasmine.createSpyObj('alertService', ['error'])
    authServiceSpy = {
      headers: new BehaviorSubject<HttpHeaders>(
        new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + 'token'
        })
      )
    }
    httpClientSpy = jasmine.createSpyObj('httpClient', ['get', 'post', 'put', 'delete'])
    const routerSpy = jasmine.createSpyObj('Router', ['navigate'])

    mealService = new MealService(httpClientSpy as any, authServiceSpy, routerSpy, alertServiceSpy)
    TestBed.configureTestingModule({})
  })

  it('should be created', () => {
    expect(mealService).toBeTruthy()
  })

  it('should get all meals', () => {
    const id = 'dayid'
    const expectedres = [
      new Meal({ name: 'tosti', rating: 100, type: 'Breakfast', time: new Date() }),
      new Meal({ name: 'tosti', rating: 100, type: 'Breakfast', time: new Date() })
    ]

    httpClientSpy.get.and.returnValue(
      of([
        { name: 'tosti', rating: 100, type: 'Breakfast', time: new Date() },
        { name: 'tosti', rating: 100, type: 'Breakfast', time: new Date() }
      ])
    )

    const subs = mealService.getAllMeals(id).subscribe(response => {
      expect(response).toEqual(expectedres)
    })

    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call')
    subs.unsubscribe()
  })

  it('should add a meal', () => {
    const meal = new Meal({ name: 'tosti', rating: 100, type: 'Breakfast', time: new Date() })
    const expectedresult = {
      _id: 'id',
      dayid: 'id',
      name: 'tosti',
      rating: 100,
      type: 'Breakfast',
      time: new Date()
    }
    const expectedmeal = new Meal(expectedresult)

    httpClientSpy.post.and.returnValue(of(expectedmeal))
    const subs = mealService.saveMeal(meal).subscribe(response => {
      expect(response).toEqual(expectedmeal)
    })
    expect(httpClientSpy.post.calls.count()).toBe(1, 'one call')

    subs.unsubscribe()
  })
})
