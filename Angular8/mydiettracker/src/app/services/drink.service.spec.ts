import { TestBed } from '@angular/core/testing'
import { of, Observable, BehaviorSubject } from 'rxjs'
import { DrinkService } from './drink.service'
import { Drink } from 'src/models/drink'
import { HttpHeaders } from '@angular/common/http'

describe('DrinkService', () => {
  let httpClientSpy: { get: jasmine.Spy; post: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy }
  let drinkService: DrinkService
  let alertServiceSpy
  let authServiceSpy

  beforeEach(() => {
    alertServiceSpy = jasmine.createSpyObj('alertService', ['error'])
    authServiceSpy = {
      headers: new BehaviorSubject<HttpHeaders>(
        new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + 'token'
        })
      )
    }
    httpClientSpy = jasmine.createSpyObj('httpClient', ['get', 'post', 'put', 'delete'])
    const routerSpy = jasmine.createSpyObj('Router', ['navigate'])

    drinkService = new DrinkService(httpClientSpy as any, authServiceSpy, alertServiceSpy, routerSpy)
    TestBed.configureTestingModule({})
  })

  it('should be created', () => {
    expect(drinkService).toBeTruthy()
  })
  it('should get all drinks', () => {
    const id = 'dayid'
    const expectedres = [
      new Drink({ name: 'water', quantity: 200 }),
      new Drink({ name: 'water', quantity: 200 })
    ]

    httpClientSpy.get.and.returnValue(
      of([
        { name: 'water', quantity: 200 },
        { name: 'water', quantity: 200 }
      ])
    )

    const subs = drinkService.getAllDrinks(id).subscribe(response => {
      expect(response).toEqual(expectedres)
    })

    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call')
    subs.unsubscribe()
  })

  it('should add a drink', () => {
    const expectedresult = { _id: 'id', dayid: 'id', name: 'water', quantity: 200, time: new Date() }
    const expecteddrink = new Drink(expectedresult)
    httpClientSpy.post.and.returnValue(of(expectedresult))
    const subs = drinkService.saveDrink(expecteddrink).subscribe(response => {
      expect(response).toEqual(expecteddrink)
    })
    expect(httpClientSpy.post.calls.count()).toBe(1, 'one call')

    subs.unsubscribe()
  })
})
