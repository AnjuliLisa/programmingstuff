import { Injectable } from '@angular/core'
import { Drink } from 'src/models/drink'
import { BehaviorSubject } from 'rxjs'
import { Day } from 'src/models/day'
import { Meal } from 'src/models/meal'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { AuthService } from './auth.service'
import { Router } from '@angular/router'
import { DayService } from './day.service'
import { MealService } from './meal.service'
import { DrinkService } from './drink.service'

@Injectable({
  providedIn: 'root'
})
export class HistoryService {
  public days = new BehaviorSubject<Day[]>(null)
  public drinks = new BehaviorSubject<Drink[]>(null)
  public meals = new BehaviorSubject<Meal[]>(null)

  constructor(
    private dayService: DayService,
    private mealService: MealService,
    private drinkService: DrinkService
  ) {}

  //Add a day with dayService
  addDay(day: Day) {
    return this.dayService.saveDay(day)
  }

  //Add a drink with drinkService
  addDrink(drink: Drink) {
    return this.drinkService.saveDrink(drink)
  }

  //Add a meal with mealService
  addMeal(meal: Meal) {
    return this.mealService.saveMeal(meal)
  }

  //Get all days for logged in user
  public async getAllDays() {
    this.dayService.getAllDays().subscribe({
      next: (response: Day[]) => {
        this.days.next(response)
      }
    })
  }

  //Get one day from dayService
  public getDay(id: string) {
    return this.dayService.getDay(id)
  }

  //Get all drinks from drinkService
  getAllDrinks(id: string) {
    this.drinkService.getAllDrinks(id).subscribe({
      next: (data: Drink[]) => {
        this.drinks.next(data)
      }
    })
  }

  //Get all meals from mealService
  getAllMeals(id: string) {
    this.mealService.getAllMeals(id).subscribe({
      next: (data: Meal[]) => {
        this.meals.next(data)
      }
    })
  }

  //Update a drink with drinkService
  updateDrink(drink: Drink) {
    return this.drinkService.updateDrink(drink)
  }

  //Update a meal with mealService
  updateMeal(meal: Meal) {
    return this.mealService.updateMeal(meal)
  }

  //Delete a day with dayService
  public async deleteDay(id: string) {
    this.dayService.deleteDay(id)
    await this.getAllDays()
  }

  //Delete a drink with drinkService
  deleteDrink(id: string) {
    return this.drinkService.deleteDrink(id)
  }

  //Delete a meal with mealService
  deleteMeal(id: string) {
    return this.mealService.deleteMeal(id)
  }

  clearAll() {
    this.meals.next(null)
    this.drinks.next(null)
    this.days.next(null)
  }
}
