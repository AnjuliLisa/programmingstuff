import { Injectable } from '@angular/core'
import { Meal } from 'src/models/meal'
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http'
import { AuthService } from './auth.service'
import { Router } from '@angular/router'
import { environment } from 'src/environments/environment.prod'
import { tap, map, catchError } from 'rxjs/operators'
import { AlertService } from '../alert/alert.service'
import { throwError } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class MealService {
  private headers

  constructor(
    private http: HttpClient,
    private authService: AuthService,
    private alertService: AlertService,
    private router: Router
  ) {
    //User has to be logged in
    this.authService.headers.subscribe(data => {
      if (data !== null) {
        this.headers = data
      } else {
        this.router.navigate(['/login'])
      }
    })
  }

  // GET all meals with given day._id
  public getAllMeals(dayid) {
    return this.http
      .get<any>(`${environment.apiUrl}/meal/day/${dayid}`, { headers: this.headers })
      .pipe(
        catchError(this.handleError),
        map(meals => meals.map(data => new Meal(data)))
      )
  }

  //POST a new meal
  public saveMeal(meal: Meal) {
    return this.http
      .post<any>(`${environment.apiUrl}/meal/`, JSON.stringify(meal), { headers: this.headers })
      .pipe(catchError(this.handleError))
  }

  //PUT a updated meal
  public updateMeal(meal: Meal) {
    return this.http
      .put<any>(`${environment.apiUrl}/meal/${meal._id}`, JSON.stringify(meal), { headers: this.headers })
      .pipe(
        catchError(this.handleError),
        tap(data => data)
      )
  }

  //DELETE a meal
  public deleteMeal(id: string) {
    return this.http
      .delete(`${environment.apiUrl}/meal/${id}`, { headers: this.headers })
      .pipe(catchError(this.handleError))
  }

  handleError = (error: HttpErrorResponse) => {
    if (error.status === 401) {
      this.authService.logout()
      this.router.navigate(['/login'])
    } else {
      this.alertService.error(`Server geeft de volgende melding: ${error.message}`)
    }
    return throwError('Probeer het later nogmaals')
  }
}
