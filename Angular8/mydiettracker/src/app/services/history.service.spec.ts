import { TestBed } from '@angular/core/testing'
import { HistoryService } from './history.service'
import { Observable, of } from 'rxjs'
import { Day } from 'src/models/day'
import { Drink } from 'src/models/drink'
import { Meal } from 'src/models/meal'

describe('HistoryService', () => {
  let dayServiceSpy
  let mealServiceSpy
  let drinkServiceSpy
  let service: HistoryService

  const mockday = new Day({ date_year: 2020, date_month: 2, date_day: 22 })
  const mockdrink = new Drink({ name: 'water', quantity: 200, time: new Date() })
  const mockmeal = new Meal({ name: 'Tosti', rating: 200, type: 'Breakfast', time: new Date() })

  beforeEach(() => {
    dayServiceSpy = {
      saveDay(day: Day): Observable<Day> {
        return of(mockday)
      },
      getAllDays(): Observable<Day[]> {
        return of([mockday, mockday])
      },
      getDay(): Observable<Day> {
        return of(mockday)
      },
      deleteDay(id: string): void {}
    }

    drinkServiceSpy = {
      deleteDrink(id: string): Observable<Drink> {
        return of(mockdrink)
      },
      updateDrink(drink: Drink): Observable<Drink> {
        return of(mockdrink)
      },
      getAllDrinks(id: string): Observable<Drink[]> {
        return of([mockdrink, mockdrink])
      },
      saveDrink(drink: Drink): Observable<Drink> {
        return of(mockdrink)
      }
    }

    mealServiceSpy = {
      addMeal(meal: Meal): Observable<Meal> {
        return of(mockmeal)
      },
      getAllMeals(id: string): void {},
      updateMeal(meal: Meal): Observable<Meal> {
        return of(mockmeal)
      },
      deleteMeal(id: string): Observable<Meal> {
        return of(mockmeal)
      }
    }

    service = new HistoryService(dayServiceSpy, drinkServiceSpy, mealServiceSpy)
    TestBed.configureTestingModule({})
  })

  it('should be created', () => {
    expect(service).toBeTruthy()
  })

  it('should return a day as observable', () => {
    const subs = service.getDay('id').subscribe(response => {
      expect(response).toEqual(mockday)
    })
    subs.unsubscribe()
  })
  it('should add and return a day as observable', () => {
    const subs = service.addDay(mockday).subscribe(response => {
      expect(response).toEqual(mockday)
    })
    subs.unsubscribe()
  })
})
