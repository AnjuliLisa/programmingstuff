import { TestBed } from '@angular/core/testing'
import { of, Observable, BehaviorSubject } from 'rxjs'
import { UserService } from './user.service'
import { HttpHeaders } from '@angular/common/http'

describe('UserService', () => {
  let httpClientSpy: { get: jasmine.Spy; post: jasmine.Spy; put: jasmine.Spy; delete: jasmine.Spy }
  let userService: UserService
  let alertServiceSpy
  let authServiceSpy

  beforeEach(() => {
    alertServiceSpy = jasmine.createSpyObj('alertService', ['error'])
    authServiceSpy = {
      headers: new BehaviorSubject<HttpHeaders>(
        new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + 'token'
        })
      )
    }
    httpClientSpy = jasmine.createSpyObj('httpClient', ['get', 'post', 'put', 'delete'])
    const routerSpy = jasmine.createSpyObj('Router', ['navigate'])

    userService = new UserService(httpClientSpy as any, authServiceSpy, routerSpy, alertServiceSpy)
    TestBed.configureTestingModule({})
  })

  it('should be created', () => {
    expect(userService).toBeTruthy()
  })
})
