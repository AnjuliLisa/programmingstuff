import { Component, OnInit, OnDestroy } from '@angular/core'
import { AuthService } from '../services/auth.service'
import { Router } from '@angular/router'
import { User } from '../../models/user'
import { DayService } from '../services/day.service'
import { Subscription } from 'rxjs'
import { DashboardService } from '../services/dashboard.service'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
  username
  date
  authsub = new Subscription()
  subs = new Subscription()

  constructor(
    private authService: AuthService,
    private dayService: DayService,
    private router: Router,
    private dashboardService: DashboardService
  ) {}

  ngOnInit() {
    this.authsub = this.authService.getCurrentUser().subscribe({
      next: (user: User) => {
        this.username = user.firstname
      },
      error: message => {
        this.router.navigate(['/login'])
      }
    })

    this.dayService.getToday().subscribe(data => {
      if (data) {
        this.date = data.date_day + '-' + data.date_month + '-' + data.date_year
        this.dashboardService.today = data
      }
    })
  }
  ngOnDestroy() {
    this.subs.unsubscribe()
    this.authsub.unsubscribe()
  }
}
