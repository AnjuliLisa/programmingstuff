import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { DashboardComponent } from './dashboard.component'
import { UserService } from '../services/user.service'
import { RouterTestingModule } from '@angular/router/testing'
import { of, Observable, BehaviorSubject } from 'rxjs'
import { Component } from '@angular/core'
import { DayService } from '../services/day.service'
import { AuthService } from '../services/auth.service'
import { User } from 'src/models/user'
import { Day } from 'src/models/day'
import { DashboardService } from '../services/dashboard.service'

@Component({ selector: 'app-drinks', template: '' })
class DrinksStubComponent {}

@Component({ selector: 'app-meal-list', template: '' })
class MealListStubComponent {}

const dummyUser = new User({
  _id: '5de12e2de1fc546b80835a58',
  firstname: 'Lisa',
  lastname: 'Appel',
  password: '...',
  dateOfBirth_year: 1998,
  dateOfBirth_month: 11,
  dateOfBirth_day: 22,
  diet: ['Lactosevrij', 'Fructosevrij'],
  email: 'example@mdt.nl'
})
const dummyDay = {
  _id: '5e5edd861445070f4c52b604',
  date_year: 2020,
  date_month: 3,
  date_day: 12
}

describe('DashboardComponent', () => {
  let component
  let fixture: ComponentFixture<DashboardComponent>
  let authServiceSpy
  let dayServiceSpy
  let dashboarddservicespy

  beforeEach(async(() => {
    authServiceSpy = {
      getCurrentUser(): Observable<User> {
        return of(dummyUser)
      }
    }
    dayServiceSpy = {
      getToday(): Observable<Day> {
        return of(new Day())
      },
      day: new BehaviorSubject<Day>(dummyDay)
    }

    dashboarddservicespy = {
      today: Day
    }
    TestBed.configureTestingModule({
      declarations: [DashboardComponent, DrinksStubComponent, MealListStubComponent],
      imports: [RouterTestingModule],
      providers: [
        { provide: AuthService, useValue: authServiceSpy },
        { provide: DayService, useValue: dayServiceSpy },
        { provide: DashboardService, useValue: dashboarddservicespy }
      ]
    }).compileComponents()
    fixture = TestBed.createComponent(DashboardComponent)
    component = fixture.componentInstance
  }))

  afterEach(() => {
    fixture.destroy()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should display a h1', async(() => {
    const compiled = fixture.debugElement.nativeElement
    expect(compiled.querySelector('h1')).toBeTruthy()
  }))

  it('should have property username', async(() => {
    fixture.detectChanges()
    expect(component.username).toEqual('Lisa')
  }))
})
