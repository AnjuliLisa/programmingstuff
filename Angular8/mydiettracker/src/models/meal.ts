export class Meal {
  _id: string
  dayid: string
  name: string
  rating: number
  type: string
  time: Date

  constructor(values = {}) {
    Object.assign(this, values)
  }
}
