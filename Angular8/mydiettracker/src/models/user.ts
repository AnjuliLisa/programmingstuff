export class User {
  _id: string
  firstname: string
  lastname: string
  email: string
  password: string
  diet: [string]
  dateOfBirth_year: number
  dateOfBirth_month: number
  dateOfBirth_day: number
  gender: string

  constructor(values = {}) {
    Object.assign(this, values)
  }
}
