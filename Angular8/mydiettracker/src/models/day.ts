import { Drink } from './drink'
import { Meal } from './meal'

export class Day {
  _id: string
  date_year: number
  date_month: number
  date_day: number

  constructor(values = {}) {
    Object.assign(this, values)
  }
}
