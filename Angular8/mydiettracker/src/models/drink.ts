export class Drink {
  _id: string
  dayid: string
  name: string
  quantity: number
  time: Date

  constructor(values = {}) {
    Object.assign(this, values)
  }
}
