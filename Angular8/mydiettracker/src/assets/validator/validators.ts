import { FormControl } from '@angular/forms'

export const validator = {
  validName(control: FormControl): { [s: string]: boolean } {
    const name = control.value
    const regexp = new RegExp('^[a-zA-Z-\\s]')
    if (regexp.test(name) !== true) {
      return { name: false }
    } else {
      return null
    }
  },
  validNumber(control: FormControl): { [s: string]: boolean } {
    const quantity = control.value
    const regex = new RegExp('^[0-9]')
    if (regex.test(quantity) !== true) {
      return { quantity: false }
    } else {
      return null
    }
  },
  validEmail(control: FormControl): { [s: string]: boolean } {
    const email = control.value
    const regexp = new RegExp('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
    if (regexp.test(email) !== true) {
      return { email: false }
    } else {
      return null
    }
  },
  validPassword(control: FormControl): { [s: string]: boolean } {
    const password = control.value
    const regexp = new RegExp('^[a-zA-Z]([a-zA-Z0-9]){2,14}')
    if (regexp.test(password) !== true) {
      return { password: false }
    } else {
      return null
    }
  },

  validYear(control: FormControl): { [s: string]: boolean } {
    const year = control.value
    const regexp = new RegExp('^(19|20)\\d\\d$')
    if (regexp.test(year) !== true) {
      return { year: false }
    } else {
      return null
    }
  }
}
