const express = require('express')
const port = 3000

let app = express()
let routes = require('express').Router()

app.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', 'http://avanscswf.herokuapp.com/')
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE')
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type')
  res.setHeader('Access-Control-Allow-Credentials', true)
  next()
})

routes.post('/login', (req, res, next) => {
  res.status(200).json({
    user: {
      _id: '5de12e2de1fc546b80835a58',
      firstname: 'Firstname',
      lastname: 'Lastname',
      password: '...',
      dateOfBirth_year: 1998,
      dateOfBirth_month: 11,
      dateOfBirth_day: 22,
      diet: ['Lactosevrij', 'Fructosevrij'],
      email: 'example@mdt.nl'
    },
    token:
      'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NzUwMjIzNjQsImlhdCI6MTU3NDE1ODM2NCwic3ViIjp7ImVtYWlsIjoiYWRtaW5AYXZhbnMubmwiLCJpZCI6IjVkYzlhY2Y3NmUzOTVhMTY1ODkwMjk2MiJ9fQ.qRPy-lTPIopAJPrarJYZkxK0suUJF_XZ9szeTtie4nc'
  })
})

routes.post('/register', (req, res, next) => {
  res.status(200).json({
    user: {
      _id: '5de12e2de1fc546b80835a58',
      firstname: 'Firstname',
      lastname: 'Lastname',
      password: '...',
      dateOfBirth_year: 1998,
      dateOfBirth_month: 11,
      dateOfBirth_day: 22,
      diet: ['Lactosevrij', 'Fructosevrij'],
      email: 'example@mdt.nl'
    },
    token:
      'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NzUwMjIzNjQsImlhdCI6MTU3NDE1ODM2NCwic3ViIjp7ImVtYWlsIjoiYWRtaW5AYXZhbnMubmwiLCJpZCI6IjVkYzlhY2Y3NmUzOTVhMTY1ODkwMjk2MiJ9fQ.qRPy-lTPIopAJPrarJYZkxK0suUJF_XZ9szeTtie4nc'
  })
})

routes.post('/day', (req, res, next) => {
  res.status(202).json({
    date: '2019-12-01T23:00:00.000Z',
    drinks: [],
    _id: '5de446d6078c5c2f68ebb23d',
    meals: [],
    mealsRating: null,
    quantityDrinks: 0,
    userid: '5de12e2de1fc546b80835a58'
  })
})

routes.get('/day/5de61d917fb10001d0aefb87/meals', (req, res, next) => {
  res.status(200).json({
    _id: '5de61d917fb10001d0aefb87',
    date: '2019-12-03T02:00:00.000Z',
    userid: '5de12e2de1fc546b80835a58',
    drinks: [
      {
        name: 'Water',
        quantity: 200,
        _id: '37eefc70-15e3-11ea-b301-0b3ded731ea0',
        id: '37eefc70-15e3-11ea-b301-0b3ded731ea0'
      },
      {
        name: 'Koffie',
        quantity: 150,
        _id: 'a3248350-1608-11ea-9687-31ce5f67cbfb',
        id: 'a3248350-1608-11ea-9687-31ce5f67cbfb'
      },
      {
        name: 'Koffie',
        quantity: 150,
        _id: 'a5eb6b80-1608-11ea-9687-31ce5f67cbfb',
        id: 'a5eb6b80-1608-11ea-9687-31ce5f67cbfb'
      },
      {
        name: 'Water',
        quantity: 150,
        _id: 'adfcd980-1608-11ea-9687-31ce5f67cbfb',
        id: 'adfcd980-1608-11ea-9687-31ce5f67cbfb'
      }
    ],
    meals: [
      { _id: '5de69d1d2711d214d8d70e96', type: 'Dinner', rating: 75, name: 'Pizza mozzerella' },
      { _id: '5de69d1f2711d214d8d70e97', type: 'Snack', rating: 25, name: 'Chocola' },
      { _id: '5de681f37e6c8a67444d0107', type: 'Breakfast', rating: 100, name: 'Tosti ham kaas' },
      { _id: '5de69d1d2711d214d8d70e95', type: 'Lunch', rating: 100, name: 'Boterham met pindakaas' }
    ],
    __v: 73,
    quantityDrinks: 650,
    mealsRating: 75,
    id: '5de61d917fb10001d0aefb87'
  })
})
app.use(routes)

app.use('*', function(req, res, next) {
  next({ error: 'Non-existing endpoint' })
})

app.use((err, req, res, next) => {
  res.status(400).json(err)
})

app.listen(port, () => {})
