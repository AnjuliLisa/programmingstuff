﻿using DomainServices.Repositories;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace UserInterfaceCustomer.ViewComponents
{
    public class NavigationMenuViewComponent : ViewComponent
    {
        private IDishRepository repository;

        public NavigationMenuViewComponent(IDishRepository repo)
        {
            repository = repo;
        }

        public IViewComponentResult Invoke()
        {
            ViewBag.SelectedCategory = RouteData?.Values["category"];
            return View(repository.Dishes
                .Select(x => x.Diet)
                .Distinct()
                .OrderBy(x => x));
        }
    }
}