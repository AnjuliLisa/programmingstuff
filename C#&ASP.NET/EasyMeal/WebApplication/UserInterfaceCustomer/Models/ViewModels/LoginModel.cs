﻿using Domain;
using System.ComponentModel.DataAnnotations;

namespace UserInterfaceCustomer.Models.ViewModels
{
    public class LoginModel
    {
        [Required(ErrorMessage = Constants.NameRequired)]
        public string Name { get; set; }
        [Required(ErrorMessage = Constants.PasswordRequired)]
        [UIHint("password")]
        public string Password { get; set; }
        public string ReturnUrl { get; set; } = "/";
    }
}

