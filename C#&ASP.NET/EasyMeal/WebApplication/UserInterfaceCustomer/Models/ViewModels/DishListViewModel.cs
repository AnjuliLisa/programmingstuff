﻿using Domain.CookDomain;
using System.Collections.Generic;

namespace UserInterfaceCustomer.Models.ViewModels
{
    public class DishListViewModel
    {
        public IEnumerable<Dish> Dishes { get; set; }
        public string CurrentCategory { get; set; }

        public string CustomerName { get; set; }
    }
}
