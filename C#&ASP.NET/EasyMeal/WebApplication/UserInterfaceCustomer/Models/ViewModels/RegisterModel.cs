﻿using Domain;
using System;
using System.ComponentModel.DataAnnotations;

namespace UserInterfaceCustomer.Models.ViewModels
{
    public class RegisterModel
    {
        [Required(ErrorMessage = Constants.NameRequired)]
        public string Name { get; set; }

        [Required(ErrorMessage = Constants.EmailRequired)]
        [EmailAddress(ErrorMessage = Constants.EmailInvalid)]
        public string Email { get; set; }

        [Required(ErrorMessage = Constants.PhoneRequired)]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = Constants.StreetAddressRequired)]
        public string StreetAddress { get; set; }

        [Required(ErrorMessage = Constants.PostalCodeRequired)]
        public string PostalCode { get; set; }

        [Required(ErrorMessage = Constants.DateOfBirthRequired)]
        [DataType(DataType.Date)]
        [Range(typeof(DateTime), "1/1/1900", "1/1/2018",
            ErrorMessage = "Geboortedatum moet binnen de datum {1:d} en datum {2:d} vallen")]
        public DateTime Birthdate { get; set; }

        public string Dieterypreference { get; set; }

        [Required(ErrorMessage = Constants.PasswordRequired)]
        [UIHint("password")]
        public string Password { get; set; }

        public string ReturnUrl { get; set; } = "/";
    }
}
