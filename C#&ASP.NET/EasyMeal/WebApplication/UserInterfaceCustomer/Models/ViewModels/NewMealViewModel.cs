﻿namespace UserInterfaceCustomer.Models.ViewModels
{
    public class NewMealViewModel
    {
        public string CurrentDish { get; set; }
        public string CategoryPreDishes { get; set; }
        public string CategoryDessertDishes { get; set; }
        public int MealId { get; set; }
        public string StarterDish { get; set; }
        public string DessertDish { get; set; }
        public string Day { get; set; }
        public int Size { get; set; }
    }
}
