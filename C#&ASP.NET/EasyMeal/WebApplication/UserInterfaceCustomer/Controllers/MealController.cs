﻿using Domain.CookDomain;
using Domain.CustomerDomain;
using DomainServices.Repositories;
using Infrastructure.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using UserInterfaceCustomer.Models.ViewModels;

namespace UserInterfaceCustomer.Controllers
{
    [Authorize]
    public class MealController : Controller
    {
        private IDishRepository repository;
        private IMealRepository mealRepository;
        private IQueryable<Dish> Dishes;

        public MealController(IDishRepository repo, IMealRepository mealrepo)
        {
            repository = repo;
            mealRepository = mealrepo;
            Dishes = repository.Dishes;
        }


        public IActionResult Details(int dishId)
            => View(new NewMealViewModel
            {
                CurrentDish = Dishes.Single(d => d.DishId == dishId).Name,
                CategoryDessertDishes = Dishes.Single(d => d.Diet == (Dishes.Single(p => p.DishId == dishId).Diet) && d.Type == "Nagerecht").Name.ToString(),
                CategoryPreDishes = Dishes.Single(d => d.Diet == (Dishes.Single(p => p.DishId == dishId).Diet) && d.Type == "Voorgerecht").Name.ToString()
            });

        [HttpPost]
        public IActionResult Details(NewMealViewModel mealVM)
        {

            if (ModelState.IsValid)
            {
                decimal preDishPrice = Dishes.Single(d => d.Name == mealVM.StarterDish).Price;
                decimal dessertDishPrice = Dishes.Single(d => d.Name == mealVM.DessertDish).Price;
                decimal mainDishPrice = Dishes.Single(d => d.Name == mealVM.CurrentDish).Price;

                var meal = new Meal
                {
                    MainDish = Dishes.First(d => d.Name == mealVM.CurrentDish).DishId,
                    StarterDish = Dishes.First(d => d.Name == mealVM.StarterDish).DishId,
                    DessertDish = Dishes.First(d => d.Name == mealVM.DessertDish).DishId,
                    DateOfOrder = DateTime.Today.GetDateByDay(mealVM.Day),
                    Size = mealVM.Size,
                    TotalPrice = dessertDishPrice + preDishPrice + mainDishPrice
                };

                meal.SetTotalPrice(mealVM.Size);

                if (meal.IsValid())
                {
                    mealRepository.SaveMeal(meal);
                    return View("Overview", meal);
                }
            }
            ModelState.AddModelError("", "Je hebt niet een voorgerecht of nagerecht gekozen");
            return View(mealVM);

        }
    }
}

