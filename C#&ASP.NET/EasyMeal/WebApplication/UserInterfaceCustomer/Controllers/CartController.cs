﻿using Domain.CustomerDomain;
using DomainServices.Repositories;
using Infrastructure.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using UserInterfaceCustomer.Models.ViewModels;


namespace UserInterfaceCustomer.Controllers
{
    [Authorize]
    public class CartController : Controller
    {
        private IMealRepository repository;
        public CartController(IMealRepository repo)
        {
            repository = repo;

        }
        public ViewResult Index(string returnUrl)
        {
            return View(new CartIndexViewModel
            {
                Cart = GetCart(),
                ReturnUrl = "/Meal/List"
            });
        }

        public RedirectToActionResult AddToCart(int mealId, string returnUrl)
        {
            Meal meal = repository.Meals.First(m => m.MealId == mealId);

            if (meal != null)
            {
                Cart cart = GetCart();
                cart.AddItem(meal);
                SaveCart(cart);
            }
            return RedirectToAction("Index", new { returnUrl });
        }
        public RedirectToActionResult RemoveFromCart(int mealId,
            string returnUrl)
        {
            Meal meal = repository.Meals
                .FirstOrDefault(p => p.MealId == mealId);
            if (meal != null)
            {
                Cart cart = GetCart();
                cart.RemoveLine(meal);
                repository.DeleteMeal(meal.MealId);
                SaveCart(cart);
            }
            return RedirectToAction("Index", new { returnUrl });
        }

        private Cart GetCart()
        {
            Cart cart = HttpContext.Session.GetJson<Cart>("Cart") ?? new Cart();
            return cart;
        }
        private void SaveCart(Cart cart)
        {
            HttpContext.Session.SetJson("Cart", cart);
        }
    }
}