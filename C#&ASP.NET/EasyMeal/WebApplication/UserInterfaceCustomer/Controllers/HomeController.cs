﻿using Microsoft.AspNetCore.Mvc;

namespace UserInterfaceCustomer.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
