﻿using DomainServices.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using UserInterfaceCustomer.Models.ViewModels;

namespace UserInterfaceCustomer.Controllers
{
    [Authorize]
    public class DishController : Controller
    {
        private IDishRepository repository;
        private ICustomerRepository customerRepository;


        public DishController(IDishRepository repo, ICustomerRepository cRepo)
        {
            repository = repo;
            customerRepository = cRepo;
        }

        public ViewResult List(string category)
        {
            ViewBag.Diet = customerRepository.GetCustomerByName(User.Identity.Name).DieteryRestriction;
            return View(new DishListViewModel
            {
                Dishes = repository.Dishes.Where(d =>
                    d.Type == "Hoofdgerecht" && (category == null || d.Diet == category)),
                CurrentCategory = category

            });
        }
    }
}