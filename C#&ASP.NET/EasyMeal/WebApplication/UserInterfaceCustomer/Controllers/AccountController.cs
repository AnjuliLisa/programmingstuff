﻿using Domain.CustomerDomain;
using DomainServices.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using UserInterfaceCustomer.Models.ViewModels;

namespace UserInterfaceCustomer.Controllers
{

    [Authorize]
    public class AccountController : Controller
    {
        private UserManager<IdentityUser> userManager;
        private SignInManager<IdentityUser> signInManager;
        private ICustomerRepository repository;

        public AccountController(UserManager<IdentityUser> userMgr,
            SignInManager<IdentityUser> signInMgr, ICustomerRepository repo)
        {
            userManager = userMgr;
            signInManager = signInMgr;
            repository = repo;
        }

        [AllowAnonymous]
        public ViewResult Login(string returnUrl)
        {
            return View(new LoginModel
            {
                ReturnUrl = returnUrl
            });
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModel loginModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Customer customer = repository.Customers.Single(c => c.Email == loginModel.Name);

                    IdentityUser user =
                        await userManager.FindByNameAsync(customer.Name);
                    if (user != null)
                    {
                        await signInManager.SignOutAsync();
                        if ((await signInManager.PasswordSignInAsync(user,
                            loginModel.Password, false, false)).Succeeded)
                        {
                            return RedirectToAction("Index", "Customer");

                        }
                    }
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "Ongeldige naam of wachtwoord");
                    return View(loginModel);
                }
            }
            ModelState.AddModelError("", "Ongeldige naam of wachtwoord");
            return View(loginModel);
        }

        [AllowAnonymous]
        public ViewResult Register(string returnUrl)
        {
            return View(new RegisterModel
            {
                ReturnUrl = returnUrl
            });
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Register(RegisterModel registerModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Customer customer = repository.GetCustomerByName(registerModel.Name);
                    if (customer != null)
                    {
                        ModelState.AddModelError("", "Deze naam bestaat al in de database");
                        return View(registerModel);
                    }

                    IdentityUser user = new IdentityUser(registerModel.Name);
                    await userManager.CreateAsync(user, registerModel.Password);

                    customer = new Customer
                    {
                        Name = registerModel.Name,
                        Birthdate = registerModel.Birthdate,
                        Email = registerModel.Email,
                        DieteryRestriction = registerModel.Dieterypreference,
                        PhoneNumber = registerModel.PhoneNumber,
                        StreetAddress = registerModel.StreetAddress,
                        PostalCode = registerModel.PostalCode
                    };

                    repository.SaveCustomer(customer);
                    return RedirectToAction("Index", "Customer");
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "Naam bestaat al in de database");
                    return View(registerModel);
                }

            }
            ModelState.AddModelError("", "Naam bestaat al in de database");
            return View(registerModel);
        }

        public async Task<RedirectResult> Logout(string returnUrl = "/Home/Index")
        {
            await signInManager.SignOutAsync();
            return Redirect(returnUrl);
        }
    }
}