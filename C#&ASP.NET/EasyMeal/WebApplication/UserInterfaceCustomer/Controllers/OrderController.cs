﻿using Domain.CustomerDomain;
using DomainServices.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;


namespace UserInterfaceCustomer.Controllers
{
    [Authorize]
    public class OrderController : Controller
    {
        private IOrderRepository repository;
        private ICustomerRepository customerRepository;
        private Cart cart;

        public OrderController(IOrderRepository repoService, ICustomerRepository custRepo, Cart cartService)
        {
            repository = repoService;
            customerRepository = custRepo;
            cart = cartService;
        }

        public ViewResult Checkout()
        {
            ViewBag.Meals = cart.Lines;
            return View(new Order
            {
                Meals = cart.Lines
            });
        }


        public ViewResult Index() => View("Index");

        [HttpPost]
        public IActionResult Checkout(Order order)
        {
            if (!cart.Lines.Any())
            {
                ModelState.AddModelError("", "Sorry, je winkelmandje is leeg!");
            }

            if (!cart.IsValid())
            {
                ModelState.AddModelError("", "Je hebt nog niet voor minimaal 4 dagen in de werkweek besteld");
            }
            if (ModelState.IsValid)
            {
                repository.SaveOrder(order);
                Customer customer = customerRepository.GetCustomerByName(User.Identity.Name);
                customer.Orders.Add(order);
                cart.Clear();
                return RedirectToAction(nameof(Completed));
            }
            else
            {
                return View(order);
            }
        }
        public IActionResult Completed()
        {
            cart.Clear();
            return RedirectToAction("Index", "Customer");
        }
    }
}