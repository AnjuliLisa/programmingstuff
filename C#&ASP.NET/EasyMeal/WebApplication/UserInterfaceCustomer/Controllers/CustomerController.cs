﻿using Domain.CustomerDomain;
using DomainServices.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;

namespace UserInterfaceCustomer.Controllers
{
    [Authorize]
    public class CustomerController : Controller
    {
        private ICustomerRepository repository;
        private IOrderRepository orderRepository;
        private IMealRepository mealRepository;

        public CustomerController(ICustomerRepository repo, IOrderRepository orderRepo, IMealRepository mealRepo)
        {
            repository = repo;
            orderRepository = orderRepo;
            mealRepository = mealRepo;
        }


        public IActionResult Index()
        {
            Customer customer = repository.GetCustomerByName(User.Identity.Name);
            ViewBag.CanOrder = true; //aanpassen

            customer.Orders.Add(new Order { Meals = mealRepository.Meals.Where(m => m.DateOfOrder.Month == DateTime.Today.Month).ToList() });
            var dept = customer.GetDeptByMonth(DateTime.Today.Month);

            ViewBag.Dept = dept;
            return View(customer);
        }


        public IActionResult Months()
        {
            Customer customer = repository.GetCustomerByName(User.Identity.Name);
            customer.Orders.Add(new Order { Meals = mealRepository.Meals.Where(m => m.DateOfOrder.Month == DateTime.Today.Month).ToList() });
            var quantityDiscount = false;
            var BDdiscount = false;
            var mealscounts = 0;
            var orders = orderRepository.Orders;
            var meals = mealRepository.Meals.ToList();
            foreach (var order in orders)
            {
                meals.AddRange(order.GetMealsWithMonth(DateTime.Today.Month));
            }

            foreach (var meal in meals)
            {
                if (customer.Birthdate.Day == meal.DateOfOrder.Day &&
                    customer.Birthdate.Month == meal.DateOfOrder.Month)
                {
                    BDdiscount = true;
                }
                else
                {
                    mealscounts++;
                }
            }

            if (mealscounts >= 15)
            {
                quantityDiscount = true;
            }

            ViewBag.BDdiscount = BDdiscount;
            ViewBag.Qdiscount = quantityDiscount;
            ViewBag.Meals = meals;
            ViewBag.Dept = customer.GetDeptByMonth(DateTime.Today.Month);

            return View();
        }

    }
}