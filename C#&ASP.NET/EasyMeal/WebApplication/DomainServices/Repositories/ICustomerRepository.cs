﻿using Domain.CustomerDomain;
using System.Linq;

namespace DomainServices.Repositories
{
    public interface ICustomerRepository
    {
        IQueryable<Customer> Customers { get; }

        void SaveCustomer(Customer customer);

        Customer GetCustomerByName(string name);

    }
}
