﻿using Domain.CustomerDomain;
using System.Linq;

namespace DomainServices.Repositories
{
    public interface IMealRepository
    {
        IQueryable<Meal> Meals { get; }
        void SaveMeal(Meal meal);
        Meal DeleteMeal(int mealId);
        Meal GetMealById(int mealId);
    }
}
