﻿using Domain.CookDomain;
using System.Linq;

namespace DomainServices.Repositories
{
    public interface ICookRepository
    {
        IQueryable<Cook> Cooks { get; }

        void SaveCook(Cook cook);
        Cook DeleteCook(int cookID);

        Cook GetCookById(int dishID);
    }
}
