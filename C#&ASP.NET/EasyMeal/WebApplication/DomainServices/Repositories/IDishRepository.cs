﻿using Domain.CookDomain;
using System.Linq;

namespace DomainServices.Repositories
{
    public interface IDishRepository
    {
        IQueryable<Dish> Dishes { get; }
        void SaveDish(Dish dish);
        Dish DeleteDish(int dishID);

        Dish GetDishById(int dishID);
    }
}
