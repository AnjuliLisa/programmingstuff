﻿using Domain.CustomerDomain;
using System.Linq;

namespace DomainServices.Repositories
{
    public interface IOrderRepository
    {
        IQueryable<Order> Orders { get; }
        void SaveOrder(Order order);
        Order GetOrderById(int orderId);
    }
}
