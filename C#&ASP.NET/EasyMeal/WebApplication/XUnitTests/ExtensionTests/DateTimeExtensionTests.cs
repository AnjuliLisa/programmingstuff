﻿using Infrastructure.Extensions;
using System;
using Xunit;

namespace XUnitTests.ExtensionTests
{
    public class DateTimeExtensionTests
    {

        [Fact]
        public void GetDateByDayShouldReturnMonday()
        {
            var date = new DateTime(2019, 10, 10); //Is op een donderdag
            var day = "Maandag";               //Volgende maandag

            var DateOfOrder = date.GetDateByDay(day);
            var expected = new DateTime(2019, 10, 14);

            Assert.Equal(expected, DateOfOrder);
        }
    }
}
