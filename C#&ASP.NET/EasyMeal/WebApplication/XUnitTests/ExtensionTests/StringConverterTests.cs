﻿using Infrastructure.Extensions;
using System;
using Xunit;

namespace XUnitTests.ExtensionTests
{
    public class StringConverterTests
    {
        [Fact]
        public void TestGetWeekOfDayByDutchName()
        {
            var dag = "Maandag";
            var result = StringConverters.GetDayOfWeekByDutchName(dag);


            Assert.Equal(DayOfWeek.Monday, result);
        }
    }
}
