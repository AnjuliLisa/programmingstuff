﻿using Domain.CustomerDomain;
using System;
using System.Collections.Generic;
using Xunit;

namespace XUnitTests.DomainTest
{
    public class CustomerTest
    {

        [Fact]
        public void CheckGetDeptByMonthReturnsDept()
        {
            Customer customer = new Customer
            {
                Orders = new List<Order>
                {
                    new Order
                    {
                        Meals = new List<Meal>
                        {
                            new Meal {TotalPrice = 12m},
                            new Meal {TotalPrice = 12m}
                        }
                    },
                    new Order
                    {
                        Meals = new List<Meal>
                        {
                            new Meal {DateOfOrder = DateTime.Today, TotalPrice = 12m},
                            new Meal {DateOfOrder = DateTime.Today, TotalPrice = 12m}
                         }
                    }
                }
            };

            var result = customer.GetDeptByMonth(DateTime.Today.Month);

            Assert.Equal(24m, result);

        }

        [Fact]
        public void CheckGetDeptByMonthReturnsQuantityDiscount()
        {
            Customer customer = new Customer
            {
                Orders = new List<Order>
                {
                    new Order
                    {
                        Meals = new List<Meal>
                        {
                            new Meal {DateOfOrder = DateTime.Today, TotalPrice = 12m},
                            new Meal {DateOfOrder = DateTime.Today, TotalPrice = 12m},
                            new Meal {DateOfOrder = DateTime.Today, TotalPrice = 12m},
                            new Meal {DateOfOrder = DateTime.Today, TotalPrice = 12m},
                            new Meal {DateOfOrder = DateTime.Today, TotalPrice = 12m},
                            new Meal {DateOfOrder = DateTime.Today, TotalPrice = 12m},
                            new Meal {DateOfOrder = DateTime.Today, TotalPrice = 12m},
                            new Meal {DateOfOrder = DateTime.Today, TotalPrice = 12m},
                            new Meal {DateOfOrder = DateTime.Today, TotalPrice = 12m},
                            new Meal {DateOfOrder = DateTime.Today, TotalPrice = 12m},
                            new Meal {DateOfOrder = DateTime.Today, TotalPrice = 12m},
                            new Meal {DateOfOrder = DateTime.Today, TotalPrice = 12m},
                            new Meal {DateOfOrder = DateTime.Today, TotalPrice = 12m},
                            new Meal {DateOfOrder = DateTime.Today, TotalPrice = 12m},
                            new Meal {DateOfOrder = DateTime.Today, TotalPrice = 12m},
                            new Meal {DateOfOrder = DateTime.Today, TotalPrice = 12m},
                            new Meal {DateOfOrder = DateTime.Today, TotalPrice = 12m},
                            new Meal {DateOfOrder = DateTime.Today, TotalPrice = 12m},
                            new Meal {DateOfOrder = DateTime.Today, TotalPrice = 12m},
                            new Meal {DateOfOrder = DateTime.Today, TotalPrice = 12m}
                        }
                    }
                }
            };

            var result = customer.GetDeptByMonth(DateTime.Today.Month);

            Assert.Equal(216m, result);

        }

        [Fact]
        public void CheckGetDeptByMonthBirthDayDiscount()
        {
            Customer customer = new Customer
            {
                Birthdate = new DateTime(1997, 07, 22),
                Orders = new List<Order>
                {
                    new Order
                    {
                        Meals = new List<Meal>
                        {
                            new Meal {TotalPrice = 12m},
                            new Meal {TotalPrice = 12m}
                        }
                    },
                    new Order
                    {
                        Meals = new List<Meal>
                        {
                            new Meal {DateOfOrder = new DateTime(2019, 07,22), TotalPrice = 12m},
                            new Meal {DateOfOrder = DateTime.Today, TotalPrice = 12m}
                        }
                    }
                }
            };

            var result = customer.GetDeptByMonth(DateTime.Today.Month);

            Assert.Equal(12m, result);

        }
    }
}
