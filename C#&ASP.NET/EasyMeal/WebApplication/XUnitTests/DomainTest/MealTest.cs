﻿using Domain.CookDomain;
using Domain.CustomerDomain;
using Xunit;

namespace XUnitTests.DomainTest
{
    public class MealTest
    {

        [Fact]
        public void CheckMealIsValid()
        {

            //Arrange

            var d1 = new Dish { DishId = 1, Name = "Macarons", Description = "Macarons met salted caramel", Price = 4.50m };
            var d2 = new Dish
            {
                DishId = 2,
                Name = "Kippensoep",
                Description = "Kippensoep met wortel en kipfilet",
                Price = 3.00m
            };
            var d3 = new Dish
            {
                DishId = 3,
                Name = "Risotto alla milanese",
                Description = "Italiaanse risotto met winterpeen",
                Price = 8.00m
            };


            Meal meal = new Meal
            {
                MainDish = d2.DishId,
                DessertDish = d3.DishId,
                StarterDish = d1.DishId
            };

            //Act

            var result = meal.IsValid();
            //Assert

            Assert.True(result);

        }

        [Fact]
        public void CheckMealIsNotValidWhenMainDishIdIsNull()
        {

            //Arrange

            var d1 = new Dish { DishId = 1, Name = "Macarons", Description = "Macarons met salted caramel", Price = 4.50m };
            var d2 = new Dish
            {
                DishId = 2,
                Name = "Kippensoep",
                Description = "Kippensoep met wortel en kipfilet",
                Price = 3.00m
            };
            var d3 = new Dish
            {
                DishId = 3,
                Name = "Risotto alla milanese",
                Description = "Italiaanse risotto met winterpeen",
                Price = 8.00m
            };


            //
            Meal meal = new Meal
            {
                MainDish = 0,
                DessertDish = d3.DishId,
                StarterDish = d1.DishId
            };

            //Act

            var result = meal.IsValid();
            //Assert

            Assert.False(result);

        }

        [Fact]
        public void CheckMealNotValidWhenStartAndDessertDishIdIsNull()
        {

            //Arrange

            var d1 = new Dish { DishId = 1, Name = "Macarons", Description = "Macarons met salted caramel", Price = 4.50m };
            var d2 = new Dish
            {
                DishId = 2,
                Name = "Kippensoep",
                Description = "Kippensoep met wortel en kipfilet",
                Price = 3.00m
            };
            var d3 = new Dish
            {
                DishId = 3,
                Name = "Risotto alla milanese",
                Description = "Italiaanse risotto met winterpeen",
                Price = 8.00m
            };


            Meal meal = new Meal
            {
                MainDish = d1.DishId,
                DessertDish = 0,
                StarterDish = 0
            };

            //Act

            var result = meal.IsValid();
            //Assert

            Assert.False(result);

        }

        [Fact]
        public void TotalPriceIsLessWhenMealSizeIsSmall()
        {
            //Arrange
            Meal meal = new Meal
            {
                TotalPrice = 10m
            };

            int size = 1;
            //Act
            meal.SetTotalPrice(size);

            //Assert
            Assert.Equal(8m, meal.TotalPrice);

        }
        [Fact]
        public void TotalPriceIsMoreWhenMealSizeIsBig()
        {
            //Arrange
            Meal meal = new Meal
            {
                TotalPrice = 10m
            };

            int size = 3;
            //Act
            meal.SetTotalPrice(size);

            //Assert
            Assert.Equal(12m, meal.TotalPrice);

        }

        [Fact]
        public void TotalPriceRemainsTheSame()
        {
            //Arrange
            Meal meal = new Meal
            {
                TotalPrice = 10m
            };

            int size = 2;
            //Act
            meal.SetTotalPrice(size);

            //Assert
            Assert.Equal(10m, meal.TotalPrice);

        }

    }
}
