﻿using Domain.CustomerDomain;
using System;
using System.Linq;
using Xunit;

namespace XUnitTests.DomainTest
{
    public class CartTests
    {

        [Fact]
        public void Can_Add_New_Lines()
        {

            Meal p1 = new Meal { MealId = 1, MainDish = 1 };
            Meal p2 = new Meal { MealId = 2, MainDish = 2 };

            Cart target = new Cart();

            target.AddItem(p1);
            target.AddItem(p2);
            Meal[] results = target.Lines.ToArray();

            Assert.Equal(2, results.Length);
            Assert.Equal(p1, results[0]);
            Assert.Equal(p2, results[1]);
        }

        [Fact]
        public void Can_Remove_Line()
        {

            Meal p1 = new Meal { MealId = 1, TotalPrice = 1m };
            Meal p2 = new Meal { MealId = 2, TotalPrice = 2m };
            Meal p3 = new Meal { MealId = 3, TotalPrice = 2m };

            Cart target = new Cart();

            target.AddItem(p1);
            target.AddItem(p2);
            target.AddItem(p3);
            target.AddItem(p2);

            target.RemoveLine(p2);

            Assert.Equal(0, target.Lines.Count(c => c == p2));
            Assert.Equal(2, target.Lines.Count());
        }

        [Fact]
        public void Calculate_Cart_Total()
        {
            Meal p1 = new Meal { MealId = 1, TotalPrice = 100M };
            Meal p2 = new Meal { MealId = 2, TotalPrice = 50M };

            Cart target = new Cart();

            target.AddItem(p1);
            target.AddItem(p2);
            decimal result = target.ComputeTotalValue();

            Assert.Equal(150M, result);

        }

        [Fact]
        public void Can_Clear_Contents()
        {

            Meal p1 = new Meal { MealId = 1, TotalPrice = 100M };
            Meal p2 = new Meal { MealId = 2, TotalPrice = 50M };

            Cart target = new Cart();

            target.AddItem(p1);
            target.AddItem(p2);

            target.Clear();
            int lines = target.Lines.Count();

            Assert.Equal(0, lines);
        }

        [Fact]

        public void HasNotOrderedFor4Workdays()
        {

            Meal p1 = new Meal { MealId = 1, DateOfOrder = new DateTime(2019, 10, 10), TotalPrice = 100M };
            Meal p2 = new Meal { MealId = 2, DateOfOrder = new DateTime(2019, 10, 11), TotalPrice = 50M };

            Cart target = new Cart();
            target.AddItem(p1);
            target.AddItem(p2);


            var result = target.IsValid();

            Assert.False(result);

        }

        [Fact]

        public void HasOrderedFor4Workdays()
        {

            Meal p1 = new Meal { MealId = 1, DateOfOrder = new DateTime(2019, 10, 10), TotalPrice = 100M };
            Meal p2 = new Meal { MealId = 2, DateOfOrder = new DateTime(2019, 10, 11), TotalPrice = 50M };
            Meal p3 = new Meal { MealId = 3, DateOfOrder = new DateTime(2019, 10, 9), TotalPrice = 50M };
            Meal p4 = new Meal { MealId = 4, DateOfOrder = new DateTime(2019, 10, 8), TotalPrice = 50M };

            Cart target = new Cart();
            target.AddItem(p1);
            target.AddItem(p2);
            target.AddItem(p3);
            target.AddItem(p4);


            var result = target.IsValid();

            Assert.True(result);

        }
    }
}