﻿using Domain.CookDomain;
using DomainServices.Repositories;
using Halcyon.HAL;
using Halcyon.Web.HAL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebAPI_Level3.Controllers
{
    [Route("api/dishes")]
    [ApiController]
    public class DishController : ControllerBase
    {
        private IDishRepository repository;

        public DishController(IDishRepository repo)
        {
            repository = repo;
        }


        [HttpGet]
        public async Task<ActionResult<IEnumerable<Dish>>> Get()
        {
            return await repository.Dishes.ToListAsync();
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {

            var dish = repository.GetDishById(id);

            if (dish == null)
            {
                return NotFound("Item does not exists in the database");
            }

            return this.HAL(dish, new Link[]
            {
                new Link("self", $"/api/dishes/{id}"),
            });
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, Dish dish)
        {
            return BadRequest("You are not allowed to modify");
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            return BadRequest("You are not allowed to delete");
        }

        [HttpPost]
        public IActionResult Post(Dish dish)
        {
            return BadRequest("You are not allowed to add");
        }
    }


}

