﻿using Domain.CookDomain;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Context
{
    public class CookDbContext : DbContext
    {
        public CookDbContext(DbContextOptions<CookDbContext> options) : base(options) { }

        public virtual DbSet<Cook> Cooks { get; set; }
        public virtual DbSet<Dish> Dishes { get; set; }


    }
}
