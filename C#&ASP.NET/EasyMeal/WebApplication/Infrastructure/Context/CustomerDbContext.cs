﻿using Domain.CustomerDomain;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Context
{
    public class CustomerDbContext : DbContext
    {
        public CustomerDbContext(DbContextOptions<CustomerDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>()
                .HasMany(o => o.Orders).WithOne();
            modelBuilder.Entity<Order>()
                .HasMany(o => o.Meals).WithOne();
        }

        public virtual DbSet<Meal> Meals { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
    }
}
