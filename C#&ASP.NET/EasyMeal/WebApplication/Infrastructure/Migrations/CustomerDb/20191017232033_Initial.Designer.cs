﻿// <auto-generated />
using System;
using Infrastructure.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Infrastructure.Migrations.CustomerDb
{
    [DbContext(typeof(CustomerDbContext))]
    [Migration("20191017232033_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.6-servicing-10079")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Domain.CustomerDomain.Customer", b =>
                {
                    b.Property<int>("CustomerId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("Birthdate");

                    b.Property<int>("CustomerNumber");

                    b.Property<int>("DietaryRestrictions");

                    b.Property<string>("Email");

                    b.Property<string>("Name");

                    b.Property<string>("PhoneNumber");

                    b.Property<string>("PostalCode");

                    b.Property<string>("StreetAddress");

                    b.HasKey("CustomerId");

                    b.ToTable("Customers");
                });

            modelBuilder.Entity("Domain.CustomerDomain.Meal", b =>
                {
                    b.Property<int>("MealId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("DateOfOrder");

                    b.Property<int>("DessertDish");

                    b.Property<int>("MainDish");

                    b.Property<int?>("OrderId");

                    b.Property<int>("Size");

                    b.Property<int>("StarterDish");

                    b.Property<decimal>("TotalPrice")
                        .HasColumnType("decimal(8, 2)");

                    b.HasKey("MealId");

                    b.HasIndex("OrderId");

                    b.ToTable("Meals");
                });

            modelBuilder.Entity("Domain.CustomerDomain.Order", b =>
                {
                    b.Property<int>("OrderId")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("CustomerId");

                    b.Property<decimal>("TotalPrice")
                        .HasColumnType("decimal(8, 2)");

                    b.Property<int>("WeekNumber");

                    b.HasKey("OrderId");

                    b.HasIndex("CustomerId");

                    b.ToTable("Orders");
                });

            modelBuilder.Entity("Domain.CustomerDomain.Meal", b =>
                {
                    b.HasOne("Domain.CustomerDomain.Order")
                        .WithMany("Meals")
                        .HasForeignKey("OrderId");
                });

            modelBuilder.Entity("Domain.CustomerDomain.Order", b =>
                {
                    b.HasOne("Domain.CustomerDomain.Customer")
                        .WithMany("Orders")
                        .HasForeignKey("CustomerId");
                });
#pragma warning restore 612, 618
        }
    }
}
