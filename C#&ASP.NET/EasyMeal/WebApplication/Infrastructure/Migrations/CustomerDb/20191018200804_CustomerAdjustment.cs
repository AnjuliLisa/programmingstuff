﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Migrations.CustomerDb
{
    public partial class CustomerAdjustment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DietaryRestrictions",
                table: "Customers");

            migrationBuilder.AddColumn<string>(
                name: "DieteryRestriction",
                table: "Customers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DieteryRestriction",
                table: "Customers");

            migrationBuilder.AddColumn<int>(
                name: "DietaryRestrictions",
                table: "Customers",
                nullable: false,
                defaultValue: 0);
        }
    }
}
