﻿using Domain.CookDomain;
using DomainServices.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace Infrastructure.Repositories
{
    public class FakeDishRepository : IDishRepository
    {

        private IQueryable<Dish> dishes = new List<Dish>
        {
            new Dish {Name = "Spaghetti Bolognese", Description = "Spaghetti met bolognese saus en gehakt", Price = 12.50M, Type = "Hoofdgerecht", Diet = "Lactosevrij"},
            new Dish { Name = "Brownies", Description = "Brownies gemaakt van amandelmeel met kokosrasp", Diet = "Fructosevrij", Price = 4.00M, Type = "Nagerecht"},
            new Dish { Name = "Macarons", Description = "Macarons met salted caramel", Diet = "Glutenvrij", Price = 4.50M, Type = "Nagerecht"},
            new Dish { Name = "Kippensoep", Description = "Kippensoep met wortel en kipfilet", Diet = "Lactosevrij", Price = 3.00M, Type = "Voorgerecht"},
            new Dish { Name = "Risotto alla milanese", Description = "Italiaanse risotto met winterpeen", Diet = "Glutenvrij", Price = 8.00M, Type = "Hoofdgerecht"},
            new Dish {Name = "Carpaccio met rucola", Description = "Verse carpaccio met pijnboompitten, truffelmayonaise en rucola", Price = 5.50M, Type = "Voorgerecht", Diet = "Fructosevrij"}

        }.AsQueryable();


        public IQueryable<Dish> Dishes => dishes;

        public void SaveDish(Dish dish)
        {
            dishes.Append(dish);
        }

        public Dish DeleteDish(int dishID)
        {
            return dishes.Single(d => d.DishId == dishID);
        }

        public Dish GetDishById(int dishID)
        {
            return dishes.Single(d => d.DishId == dishID);
        }
    }
}
