﻿using Domain;
using Domain.CookDomain;
using DomainServices.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class APIDishRepository : IDishRepository
    {
        public IQueryable<Dish> Dishes => GetDishes();

        public void SaveDish(Dish dish)
        {
            throw new NotImplementedException();
        }

        public Dish DeleteDish(int dishID)
        {
            throw new NotImplementedException();
        }

        public Dish GetDishById(int dishID)
        {
            return Dishes.Single(d => d.DishId == dishID);
        }

        public static async Task<List<Dish>> GetAllDishes()
        {
            HttpClient client = new HttpClient();
            string path = "dishes";
            client.BaseAddress = new Uri(Constants.URL_Level2);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));


            List<Dish> dishes = new List<Dish>();
            HttpResponseMessage response = await client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                dishes = await response.Content.ReadAsAsync<List<Dish>>();
            }

            return dishes;

        }

        public static IQueryable<Dish> GetDishes()
        {
            return GetAllDishes().Result.AsQueryable();
        }



    }
}
