﻿using Domain.CookDomain;
using DomainServices.Repositories;
using Infrastructure.Context;
using System.Linq;

namespace Infrastructure.Repositories
{
    public class EFCookRepository : ICookRepository
    {

        private CookDbContext context;

        public EFCookRepository(CookDbContext ctx) => context = ctx;

        public IQueryable<Cook> Cooks => context.Cooks;

        public void SaveCook(Cook cook)
        {
            if (cook.CookId == 0)
            {
                context.Cooks.Add(cook);
            }
            else
            {
                Cook dbEntry = context.Cooks
                    .FirstOrDefault(p => p.CookId == cook.CookId);
                if (dbEntry != null)
                {
                    dbEntry.Name = cook.Name;
                    dbEntry.Email = cook.Email;
                    dbEntry.PhoneNumber = cook.PhoneNumber;
                }
            }

            context.SaveChanges();
        }

        public Cook DeleteCook(int cookID)
        {
            Cook dbEntry = context.Cooks
                .FirstOrDefault(p => p.CookId == cookID);

            if (dbEntry != null)
            {
                context.Cooks.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }

        public Cook GetCookById(int cookID)
        {
            return Cooks.Single(c => c.CookId == cookID);
        }
    }
}
