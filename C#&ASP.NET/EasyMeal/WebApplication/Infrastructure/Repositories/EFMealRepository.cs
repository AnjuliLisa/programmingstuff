﻿using Domain.CustomerDomain;
using DomainServices.Repositories;
using Infrastructure.Context;
using System;
using System.Linq;

namespace Infrastructure.Repositories
{
    public class EFMealRepository : IMealRepository
    {
        private CustomerDbContext context;

        public EFMealRepository(CustomerDbContext ctx)
        {
            context = ctx;
        }

        public IQueryable<Meal> Meals => context.Meals;
        public void SaveMeal(Meal meal)
        {
            if (meal.MealId == 0)
            {
                context.Meals.Add(meal);
            }
            else
            {
                Meal dbEntry = context.Meals
                    .First(m => m.MealId == meal.MealId);
                if (dbEntry != null)
                {
                    dbEntry.MainDish = meal.MainDish;
                    dbEntry.StarterDish = meal.StarterDish;
                    dbEntry.DessertDish = meal.DessertDish;
                    dbEntry.DateOfOrder = meal.DateOfOrder;
                    dbEntry.Size = meal.Size;
                    dbEntry.TotalPrice = meal.TotalPrice;
                }
            }
            context.SaveChanges();
        }

        public Meal DeleteMeal(int MealId)
        {
            Meal dbEntry = context.Meals
                .FirstOrDefault(m => m.MealId == MealId);
            if (dbEntry != null)
            {
                context.Meals.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }


        public Meal GetMealById(int MealId)
        {
            Meal meal;
            try
            {
                meal = Meals.Single(m => m.MealId == MealId);
            }
            catch (Exception)
            {
                return null;
            }

            return meal;
        }
    }

}
