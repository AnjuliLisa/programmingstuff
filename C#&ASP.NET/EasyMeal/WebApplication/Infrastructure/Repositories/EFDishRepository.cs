﻿using Domain.CookDomain;
using DomainServices.Repositories;
using Infrastructure.Context;
using System;
using System.Linq;

namespace Infrastructure.Repositories
{
    public class EFDishRepository : IDishRepository
    {
        private CookDbContext context;

        public EFDishRepository(CookDbContext ctx) => context = ctx;


        public IQueryable<Dish> Dishes => context.Dishes;

        public void SaveDish(Dish dish)
        {
            if (dish.DishId == 0)
            {
                context.Dishes.Add(dish);
            }
            else
            {
                Dish dbEntry = context.Dishes
                    .First(d => d.DishId == dish.DishId);
                if (dbEntry != null)
                {
                    dbEntry.Name = dish.Name;
                    dbEntry.Description = dish.Description;
                    dbEntry.Price = dish.Price;
                    dbEntry.Type = dish.Type;
                    dbEntry.Image = dish.Image;
                    dbEntry.Diet = dish.Diet;
                }
            }
            context.SaveChanges();
        }
        public Dish DeleteDish(int dishId)
        {
            Dish dbEntry = context.Dishes
                .FirstOrDefault(d => d.DishId == dishId);
            if (dbEntry != null)
            {
                context.Dishes.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }

        public Dish GetDishById(int dishID)
        {
            Dish dish;
            try
            {
                dish = Dishes.Single(d => d.DishId == dishID);
            }
            catch (Exception)
            {
                return null;
            }

            return dish;
        }
    }
}
