﻿using Domain.CustomerDomain;
using DomainServices.Repositories;
using Infrastructure.Context;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Infrastructure.Repositories
{
    public class EFOrderRepository : IOrderRepository
    {
        private CustomerDbContext context;

        public EFOrderRepository(CustomerDbContext ctx)
        {
            context = ctx;
        }
        public IQueryable<Order> Orders => context.Orders
            .Include(o => o.Meals);

        public void SaveOrder(Order order)
        {
            if (order.OrderId == 0)
            {
                context.Orders.Add(order);
            }
            context.SaveChanges();
        }

        public Order GetOrderById(int orderId)
        {
            return Orders.Single(o => o.OrderId == orderId);
        }
    }
}
