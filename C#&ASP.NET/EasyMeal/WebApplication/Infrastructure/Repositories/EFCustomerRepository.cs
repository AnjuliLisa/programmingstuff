﻿using Domain.CustomerDomain;
using DomainServices.Repositories;
using Infrastructure.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Infrastructure.Repositories
{
    public class EFCustomerRepository : ICustomerRepository
    {
        private CustomerDbContext context;

        public EFCustomerRepository(CustomerDbContext ctx)
        {
            context = ctx;
        }

        public IQueryable<Customer> Customers => context.Customers.Include(c => c.Orders);
        public void SaveCustomer(Customer customer)
        {
            if (customer.CustomerId == 0)
            {
                context.Add(customer);
            }

            context.SaveChanges();
        }

        public Customer GetCustomerByName(string name)
        {
            Customer customer;
            try
            {
                customer = Customers.Single(d => d.Name == name);
            }
            catch (Exception)
            {
                return null;
            }

            return customer;
        }
    }
}
