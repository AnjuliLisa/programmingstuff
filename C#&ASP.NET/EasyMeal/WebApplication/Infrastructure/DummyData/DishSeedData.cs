﻿using Domain.CookDomain;
using Infrastructure.Context;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace Infrastructure.DummyData
{
    public static class DishSeedData
    {
        public static void EnsurePopulated(IApplicationBuilder app)
        {
            CookDbContext context = app.ApplicationServices
                .GetRequiredService<CookDbContext>();
            context.Database.Migrate();
            if (!context.Dishes.Any())
            {
                context.Dishes.AddRange(
                    new Dish { Name = "Brownies", Description = "Brownies gemaakt van amandelmeel met kokosrasp", Diet = "Fructosevrij", Price = 4.00M, Type = "Nagerecht" },
                    new Dish { Name = "Macarons", Description = "Macarons met salted caramel", Diet = "Glutenvrij", Price = 4.50M, Type = "Nagerecht" },
                    new Dish { Name = "Kippensoep", Description = "Kippensoep met wortel en kipfilet", Diet = "Lactosevrij", Price = 3.00M, Type = "Voorgerecht" },
                    new Dish { Name = "Risotto alla milanese", Description = "Italiaanse risotto met winterpeen", Diet = "Glutenvrij", Price = 8.00M, Type = "Hoofdgerecht" }
                );
                context.SaveChanges();
            }
        }
    }
}