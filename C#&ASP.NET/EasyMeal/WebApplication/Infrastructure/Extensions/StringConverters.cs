﻿using System;

namespace Infrastructure.Extensions
{
    public static class StringConverters
    {
        public static DayOfWeek GetDayOfWeekByDutchName(string name)
        {
            DayOfWeek result = DayOfWeek.Monday;
            if (name == "Maandag")
            {
                result = DayOfWeek.Monday;
            }

            if (name == "Dinsdag")
            {
                result = DayOfWeek.Tuesday;
            }

            if (name == "Woensdag")
            {
                result = DayOfWeek.Wednesday;
            }

            if (name == "Donderdag")
            {
                result = DayOfWeek.Thursday;
            }

            if (name == "Vrijdag")
            {
                result = DayOfWeek.Friday;
            }

            if (name == "Zaterdag")
            {
                result = DayOfWeek.Saturday;
            }

            if (name == "Zondag")
            {
                result = DayOfWeek.Sunday;
            }

            return result;
        }

        public static int GetSize(string name)
        {
            int size = 0;
            if (name == "Standaard")
            {
                size = 2;
            }

            if (name == "Klein")
            {
                size = 1;
            }

            if (name == "Groot")
            {
                size = 3;
            }
            else
            {
                size = 2;
            }

            return size;
        }
    }
}
