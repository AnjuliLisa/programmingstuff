﻿using System;
using System.Globalization;

namespace Infrastructure.Extensions
{
    public static class DateTimeExtensions
    {
        public static int GetWeekNumber(this DateTime datetime)
        {
            CultureInfo myCI = new CultureInfo("nl-NL");
            Calendar calendar = myCI.Calendar;
            CalendarWeekRule rule = myCI.DateTimeFormat.CalendarWeekRule;
            DayOfWeek dayOfWeek = myCI.DateTimeFormat.FirstDayOfWeek;

            int week = calendar.GetWeekOfYear(datetime, rule, dayOfWeek);

            return week;
        }

        public static string GetDutchDay(this DateTime datetime)
        {
            string day = null;

            if (datetime.DayOfWeek == DayOfWeek.Monday)
            {
                day = "Maandag";
            }

            if (datetime.DayOfWeek == DayOfWeek.Tuesday)
            {
                day = "Dinsdag";
            }

            if (datetime.DayOfWeek == DayOfWeek.Wednesday)
            {
                day = "Woensdag";
            }

            if (datetime.DayOfWeek == DayOfWeek.Thursday)
            {
                day = "Donderdag";
            }

            if (datetime.DayOfWeek == DayOfWeek.Friday)
            {
                day = "Vrijdag";
            }

            if (datetime.DayOfWeek == DayOfWeek.Saturday)
            {
                day = "Zaterdag";
            }

            if (datetime.DayOfWeek == DayOfWeek.Sunday)
            {
                day = "Zondag";
            }

            return day;
        }

        public static bool IsWeekDay(this DateTime datetime)
        {
            if (datetime.DayOfWeek == DayOfWeek.Monday)
            {
                return true;
            }

            if (datetime.DayOfWeek == DayOfWeek.Tuesday)
            {
                return true;
            }

            if (datetime.DayOfWeek == DayOfWeek.Wednesday)
            {
                return true;
            }

            if (datetime.DayOfWeek == DayOfWeek.Thursday)
            {
                return true;
            }

            if (datetime.DayOfWeek == DayOfWeek.Friday)
            {
                return true;
            }

            if (datetime.DayOfWeek == DayOfWeek.Saturday)
            {
                return false;
            }

            if (datetime.DayOfWeek == DayOfWeek.Sunday)
            {
                return false;
            }
            else
            {
                return false;
            }
        }

        public static bool IsAllowedOrderDay(this DateTime datetime)
        {
            if (datetime.DayOfWeek == DayOfWeek.Monday)
            {
                return true;
            }

            if (datetime.DayOfWeek == DayOfWeek.Tuesday)
            {
                return true;
            }

            if (datetime.DayOfWeek == DayOfWeek.Wednesday)
            {
                return true;
            }

            if (datetime.DayOfWeek == DayOfWeek.Thursday)
            {
                return true;
            }

            if (datetime.DayOfWeek == DayOfWeek.Friday)
            {
                return false;
            }

            if (datetime.DayOfWeek == DayOfWeek.Saturday)
            {
                return false;
            }

            if (datetime.DayOfWeek == DayOfWeek.Sunday)
            {
                return false;
            }
            else
            {
                return false;
            }
        }

        public static DateTime GetDateByDay(this DateTime dateTime, string day)
        {
            DateTime monday = new DateTime();

            if (dateTime.DayOfWeek == DayOfWeek.Monday)
            {
                monday = dateTime.AddDays(7);
            }

            if (dateTime.DayOfWeek == DayOfWeek.Tuesday)
            {
                monday = dateTime.AddDays(6);
            }

            if (dateTime.DayOfWeek == DayOfWeek.Wednesday)
            {
                monday = dateTime.AddDays(5);
            }

            if (dateTime.DayOfWeek == DayOfWeek.Thursday)
            {
                monday = dateTime.AddDays(4);
            }

            if (dateTime.DayOfWeek == DayOfWeek.Friday)
            {
                monday = dateTime.AddDays(3);
            }

            if (dateTime.DayOfWeek == DayOfWeek.Saturday)
            {
                monday = dateTime.AddDays(2);
            }

            if (dateTime.DayOfWeek == DayOfWeek.Sunday)
            {
                monday = dateTime.AddDays(1);
            }

            if (day == "Dinsdag")
            {
                return monday.AddDays(1);
            }

            if (day == "Woensdag")
            {
                return monday.AddDays(2);
            }

            if (day == "Donderdag")
            {
                return monday.AddDays(3);
            }

            if (day == "Vrijdag")
            {
                return monday.AddDays(4);
            }

            if (day == "Zaterdag")
            {
                return monday.AddDays(5);
            }

            if (day == "Zondag")
            {
                return monday.AddDays(6);
            }
            else
            {
                return monday;
            }
        }
    }
}
