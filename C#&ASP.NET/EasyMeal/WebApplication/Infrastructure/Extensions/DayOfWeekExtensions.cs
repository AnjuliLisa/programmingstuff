﻿using System;

namespace Infrastructure.Extensions
{
    public static class DayOfWeekExtensions
    {
        public static DayOfWeek GetDayOfWeekByDutchName(this DayOfWeek dow, string dutchName)
        {
            if (dutchName == "Maandag")
            {
                return DayOfWeek.Monday;
            }

            if (dutchName == "Dinsdag")
            {
                return DayOfWeek.Tuesday;
            }

            if (dutchName == "Woensdag")
            {
                return DayOfWeek.Wednesday;
            }

            if (dutchName == "Donderdag")
            {
                return DayOfWeek.Thursday;
            }

            if (dutchName == "Vrijdag")
            {
                return DayOfWeek.Friday;
            }

            if (dutchName == "Zaterdag")
            {
                return DayOfWeek.Saturday;
            }
            else
            {
                return DayOfWeek.Sunday;
            }
        }
    }
}
