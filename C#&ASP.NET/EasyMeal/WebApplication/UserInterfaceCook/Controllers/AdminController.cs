﻿using Domain.CookDomain;
using DomainServices.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using UserInterfaceCook.Models.ViewModels;

namespace UserInterfaceCook.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        private IDishRepository repository;
        public AdminController(IDishRepository repo)
        {
            repository = repo;
        }
        public ViewResult Index() => View(repository.Dishes);

        public ViewResult Edit(int dishId)
        {
            ViewBag.Title = "Gerecht aanpassen";
            Dish currentDish = repository.GetDishById(dishId);

            return View(new NewDishModel
            { DishId = dishId, Description = currentDish.Description, Name = currentDish.Name, Price = currentDish.Price, Diet = currentDish.Diet, Type = currentDish.Type });
        }

        [HttpPost]
        public IActionResult Edit(NewDishModel dishVm)
        {
            if (ModelState.IsValid)
            {
                byte[] arrayBytes = null;
                if (dishVm.Image != null)
                {
                    MemoryStream ms = new MemoryStream();
                    dishVm.Image.CopyTo(ms);
                    arrayBytes = ms.ToArray();
                }

                Dish dish = new Dish
                {
                    DishId = dishVm.DishId,
                    Description = dishVm.Description,
                    Name = dishVm.Name,
                    Image = arrayBytes,
                    Diet = dishVm.Diet,
                    Price = dishVm.Price,
                    Type = dishVm.Type
                };
                repository.SaveDish(dish);
                TempData["message"] = $"{dishVm.Name} is opgeslagen";
                return RedirectToAction("Index");
            }
            else
            {
                return View(dishVm);
            }
        }

        public ViewResult Create()
        {
            ViewBag.Title = "Nieuw gerecht aanmaken";
            return View("Edit", new NewDishModel());
        }

        [HttpPost]
        public IActionResult Delete(int dishId)
        {
            Dish deletedDish = repository.DeleteDish(dishId);
            if (deletedDish != null)
            {
                TempData["message"] = $"{deletedDish.Name} is verwijderd";
            }
            return RedirectToAction("Index");
        }
    }
}