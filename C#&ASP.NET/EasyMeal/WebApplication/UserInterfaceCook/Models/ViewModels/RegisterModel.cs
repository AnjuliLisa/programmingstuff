﻿using Domain;
using System.ComponentModel.DataAnnotations;

namespace UserInterfaceCook.Models.ViewModels
{
    public class RegisterModel
    {
        [Required(ErrorMessage = Constants.NameRequired)]
        public string Name { get; set; }

        [Required(ErrorMessage = Constants.EmailRequired)]
        [EmailAddress(ErrorMessage = Constants.EmailInvalid)]
        public string Email { get; set; }

        [Required(ErrorMessage = Constants.PhoneRequired)]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = Constants.PasswordRequired)]
        [UIHint("password")]
        public string Password { get; set; }

        public string ReturnUrl { get; set; }
    }
}
