﻿using Domain;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace UserInterfaceCook.Models.ViewModels
{
    public class NewDishModel
    {
        public int DishId { get; set; }

        [Required(ErrorMessage = Constants.NameRequired)]
        public string Name { get; set; }

        [Required(ErrorMessage = Constants.DescriptionRequired)]
        public string Description { get; set; }

        public string Diet { get; set; }

        [Required(ErrorMessage = Constants.TypeRequired)]
        public string Type { get; set; }


        [Required(ErrorMessage = Constants.PriceRequired)]
        [Range(0.01, double.MaxValue,
            ErrorMessage = Constants.PriceInvalid)]
        public decimal Price { get; set; }

        public IFormFile Image { get; set; }
    }
}
