﻿
namespace Domain.CookDomain
{
    public class Cook
    {
        public int CookId { get; set;}

        public string Name { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }
    }
}
