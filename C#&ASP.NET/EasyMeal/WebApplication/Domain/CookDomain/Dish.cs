﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.CookDomain
{
    public class Dish 
    {
        public int DishId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Diet { get; set; }

        public string Type { get; set; }

        [Column(TypeName = "decimal(8, 2)")]
        public decimal Price { get; set; }

        public byte[] Image { get; set; }

    }
}
