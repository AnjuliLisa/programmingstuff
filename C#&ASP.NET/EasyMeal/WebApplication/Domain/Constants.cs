﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain
{
    public class Constants
    {
        public const string NameRequired = "Vul een naam in";
        public const string EmailRequired = "Vul een emailadres in";
        public const string DescriptionRequired = "Vul een omschrijving in";
        public const string StreetAddressRequired = "Vul een adres in";
        public const string PostalCodeRequired = "Vul een postcode in";
        public const string DateOfBirthRequired = "Vul een geboortedatum in";
        public const string PhoneRequired = "Vul een telefoonnummer in";
        public const string PasswordRequired = "Vul een wachtwoord in";
        public const string MainDishRequired = "Hoofdgerecht is verplicht";
        public const string PriceRequired = "Vul een prijs in";
        public const string TypeRequired = "Vul minimaal 1 type in";


        public const string PostalCodeInvalid = "Vul een geldige postcode in";
        public const string EmailInvalid = "Vul een geldig emailadres in";
        public const string PriceInvalid = "Vul een geldige prijs in";

        public const string URL_Level2 = "https://easymealapi2.azurewebsites.net/api/";
        public const string URL_Level3 = "https://easymealapi3.azurewebsites.net/api/";

    }
}
