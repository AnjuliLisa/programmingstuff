﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Domain.CookDomain;

namespace Domain.CustomerDomain
{
    public class Meal
    {
        public int MealId { get; set; }
       
        public int StarterDish { get; set; }
        public int MainDish { get; set; }
        public int DessertDish { get; set; }
        public DateTime DateOfOrder { get; set; }
        public int Size { get; set; } = 2;

        [Column(TypeName = "decimal(8, 2)")]
        public decimal TotalPrice { get; set; }


        public bool IsValid()
        {
            if (this.MainDish == 0)
            {
                return false;
            }else if (this.DessertDish == 0 && this.StarterDish == 0)
            {
                return false;
            }

            return true;
        }

        public void SetTotalPrice(int size)
        {
            if(size == 1) TotalPrice *= 0.8m;
            if (size == 3) TotalPrice *= 1.2m;
        }

        public string GetDutchDayOfWeek()
        {
            var current = DateOfOrder.DayOfWeek;

            if (current == DayOfWeek.Monday) return "Maandag";
            if (current == DayOfWeek.Tuesday) return "Dinsdag";
            if (current == DayOfWeek.Wednesday) return "Woensdag";
            if (current == DayOfWeek.Thursday) return "Donderdag";
            if (current == DayOfWeek.Friday) return "Vrijdag";
            if (current == DayOfWeek.Saturday) return "Zaterdag";
            if (current == DayOfWeek.Sunday) return "Zondag";
            else
            {
                return "Maandag";
            }
        }
    }
}
