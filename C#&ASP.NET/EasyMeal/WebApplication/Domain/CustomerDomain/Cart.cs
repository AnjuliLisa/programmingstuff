﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.CustomerDomain
{
    public class Cart
    {
        private List<Meal> lineCollection = new List<Meal>();
        public virtual void AddItem(Meal meal)
        {
            lineCollection.Add(meal);
        }

        public virtual void RemoveLine(Meal meal) =>
            lineCollection.RemoveAll(l => l.MealId == meal.MealId);
        public virtual decimal ComputeTotalValue() =>
            lineCollection.Sum(e => e.TotalPrice);
        public virtual void Clear() => lineCollection.Clear();
        public virtual List<Meal> Lines => lineCollection;

        public bool IsValid()
        {
            int weekday = 0;
            int weekendday = 0;

            foreach (Meal m in lineCollection)
            {
                if (m.DateOfOrder.DayOfWeek == DayOfWeek.Monday) weekday++;
                if (m.DateOfOrder.DayOfWeek == DayOfWeek.Tuesday) weekday++;
                if (m.DateOfOrder.DayOfWeek == DayOfWeek.Wednesday) weekday++;
                if (m.DateOfOrder.DayOfWeek == DayOfWeek.Thursday) weekday++;
                if (m.DateOfOrder.DayOfWeek == DayOfWeek.Friday) weekday++;
                if (m.DateOfOrder.DayOfWeek == DayOfWeek.Saturday) weekendday++;
                else weekendday++;
            }

            if (weekday > 3) return true;
            return false;
        }
    }
}
