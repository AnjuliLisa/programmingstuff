﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain.CustomerDomain
{
    public class Customer
    {
        public int CustomerId { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }


        public string PhoneNumber { get; set; }


        public string StreetAddress { get; set; }


        public string PostalCode { get; set; }

        public int CustomerNumber { get; set; } = GenerateCustomerNr();

        public DateTime Birthdate { get; set; }

        public string DieteryRestriction { get; set; }

        public List<Order> Orders { get; set; }

        public static int GenerateCustomerNr()
        {
            Random random = new Random();

            int customerNr = random.Next(10000, 100000);
            return customerNr;
        }


        public decimal GetDeptByMonth(int month)
        {
            decimal dept = 0;
            var count = 0;

            if (Orders != null)
            {
                foreach (var order in Orders)
                {
                    foreach (var orderMeal in order.GetMealsWithMonth(month))
                    {
                        if (orderMeal.DateOfOrder == Birthdate)
                        {
                        }
                        else
                        {
                            count++;
                            dept += orderMeal.TotalPrice;
                        }
                    }
                }

            }

            if (count >= 15)
            {
                dept *= 0.9m;
            }

            return dept;

        }
    }

    

}
