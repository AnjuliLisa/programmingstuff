﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;


namespace Domain.CustomerDomain
{
    public class Order
    {
        public int OrderId { get; set; }

        public List<Meal> Meals { get; set; }

        public int WeekNumber { get; set; }
        [Column(TypeName = "decimal(8, 2)")]
        public decimal TotalPrice { get; set; }

        public List<Meal> GetMealsWithMonth(int month)
        {
            List<Meal> result = new List<Meal>();

            foreach (Meal meal in Meals)
            {
                if (meal.DateOfOrder.Month == month)
                {
                    result.Add(meal);
                }
            }
            return result;
        }


    }
}
