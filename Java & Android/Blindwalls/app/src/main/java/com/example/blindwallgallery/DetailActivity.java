package com.example.blindwallgallery;

import android.content.Intent;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity implements OnMapReadyCallback {

    private static final String TAG = "DetailActivity";

    private ImageView image;
    private TextView title;
    private TextView material;
    private TextView address;
    private TextView photographer;
    private TextView description;

    private GoogleMap mMap;
    double latitude;
    double longitude;
    String street;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Log.v(TAG, "onCreate() aangeroepen");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Intent intent = getIntent();

        final Blindwall blindwall = (Blindwall) intent.getExtras().getSerializable("Blindwall");
        String language = intent.getExtras().getString("language");

        image = findViewById(R.id.iv_details_image);
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent detailImagesIntent = new Intent(DetailActivity.this, ImagesActivity.class );
                detailImagesIntent.putExtra("Blindwall", blindwall);

                DetailActivity.this.startActivity(detailImagesIntent);

            }
        });

        title = findViewById(R.id.tv_details_title);
        material = findViewById(R.id.tv_details_material);
        address = findViewById(R.id.tv_details_address);
        photographer = findViewById(R.id.tv_details_photographer);
        description = findViewById(R.id.tv_details_description);

        String randomImageUrl = blindwall.getImageURLDetails().get(0);
        Picasso.get().load(randomImageUrl).into(image);

        if (language.equals("nl")) {

            title.setText(blindwall.getTitleNL());
            material.setText(blindwall.getMaterialNL());
            address.setText(blindwall.getAddressNL());
            photographer.setText(blindwall.getPhotographerNL());
            description.setText(blindwall.getDescriptionNL());

        } else {
            title.setText(blindwall.getTitleEN());
            material.setText(blindwall.getMaterialEN());
            address.setText(blindwall.getAddressEN());
            photographer.setText(blindwall.getPhotographerEN());
            description.setText(blindwall.getDescriptionEN());
        }


        //Creeeren van de map
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //Latitude, longitude en address ophalen uit blindwall object
        latitude = blindwall.getLatitude();
        longitude = blindwall.getLongitude();
        street = blindwall.getAddressEN();

        //Terugbutton op de menubalk
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    //Implementatie van de terugbutton
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.v(TAG, "onOptionsItemSelected() aangeroepen");
        int id = item.getItemId();
        if (id == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }


    public void onMapReady(GoogleMap googleMap) {
        Log.v(TAG, "onMapReady() aangeroepen");
        mMap = googleMap;

        //Marker maken met de gegevens van de api
        LatLng latLng = new LatLng(latitude, longitude);
        mMap.addMarker(new MarkerOptions().position(latLng).title("" + street).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE)));

        //Dit is waar de camera begint en hoeveel de camera is ingezoomd
        float zoomLevel =  14.0f; //Kan tot 21
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel));
    }
}
