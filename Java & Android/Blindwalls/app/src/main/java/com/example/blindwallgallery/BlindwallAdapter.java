package com.example.blindwallgallery;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class BlindwallAdapter extends RecyclerView.Adapter<BlindwallAdapter.ViewHolder> {

    private List<Blindwall> blindwalls;
    private Context context;
    private String language;
    private static final String TAG = "BlindwallAdapter";

    public BlindwallAdapter(Context context) {
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        Log.v(TAG, "onCreateViewHolder aangeroepen");

        Context context = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View wallItem =inflater.inflate(R.layout.wall_item, viewGroup, false);
        BlindwallAdapter.ViewHolder viewHolder = new ViewHolder(wallItem);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Log.d(TAG, "onBindViewHolder aangeroepen");
        final Blindwall blindwall = blindwalls.get(i);

        viewHolder.name.setText(blindwall.getTitleEN());

        Picasso.get().load(blindwall.getImageURLFrontpage()).into(viewHolder.image);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {

            //OnclickListener hierheen verplaatst zodat het Blindwall Object meegegeven kan worden
            //naar de DetailActivity
            //context is gehaald uit MainActivity met de method setContext()
            @Override
            public void onClick(View v) {

                Class destinationClass = DetailActivity.class;
                Intent intent = new Intent(context, destinationClass);
                intent.putExtra("Blindwall", blindwall);
                intent.putExtra("language", language);

                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {

        Log.v(TAG, "getItemCount() aangeroepen");
        if (null == blindwalls) return 0;
        return blindwalls.size();
    }


    public void setBlindwallsData(List<Blindwall> list){
        Log.v(TAG, "setBlindwallsData() aangeroepen");
        this.blindwalls = list;
        //Dit zorgt ervoor dat de recyclerview opnieuw gevuld wordt als de data veranderd
        notifyDataSetChanged();
    }

    public void setContext(Context context) {
        Log.v(TAG, "setContext aangeroepen" );
        this.context = context;
    }



    public class ViewHolder extends RecyclerView.ViewHolder{

        private static final String TAG = "ViewHolder";

        //ViewHolder class groepeerd de ImageView en TextViews
        //Zodat ze in één ViewHolder object zitten
        //Die gecreëerd kan worden door de Adapter
        //En gevuld kan worden door de Adapter

        public TextView name;
        public ImageView image;

        public ViewHolder(View listItemView) {
            super(listItemView);
            image = listItemView.findViewById(R.id.iv_Blindwall_item_image);
            name = listItemView.findViewById(R.id.tv_Blindwall_item_name);

        }

    }

    public void setLanguage(String input){
        Log.v(TAG, "setLanguage aangeroepen");
        this.language = input;
    }

}
