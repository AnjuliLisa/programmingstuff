package com.example.blindwallgallery;

import android.util.Log;

import java.io.Serializable;

import java.util.List;

public class Blindwall implements Serializable{

    private static final String TAG = "Blindwall";

    private String titleNL;
    private String titleEN;
    private String descriptionEN;
    private String descriptionNL;
    private double latitude;
    private double longitude;
    private String photographer;
    private String author;
    private String materialEN;
    private String materialNL;
    private String imageURLFrontpage;
    private List<String> imageURLDetails;
    private String address;

    public String getTitleNL() {
        Log.v(TAG, "getTitleNL() aangeroepen");
        return titleNL;
    }

    public void setTitleNL(String titleNL) {
        Log.v(TAG, "setTitleNL() aangeroepen");
        this.titleNL = titleNL;
    }

    public String getTitleEN() {
        Log.v(TAG, "getTitleEN() aangeroepen");
        return titleEN;
    }

    public void setTitleEN(String titleEN) {
        Log.v(TAG, "setTitleEN() aangeroepen");
        this.titleEN = titleEN;
    }

    public String getDescriptionEN() {
        Log.v(TAG, "getDescriptionEN() aangeroepen");
        return descriptionEN;
    }

    public void setDescriptionEN(String descriptionEN) {
        Log.v(TAG, "setDescriptionEN() aangeroepen");
        this.descriptionEN = descriptionEN;
    }

    public String getDescriptionNL() {
        Log.v(TAG, "getDescriptionNL() aangeroepen");
        return descriptionNL;
    }

    public void setDescriptionNL(String descriptionNL) {
        Log.v(TAG, "setDescriptionEN() aangeroepen");
        this.descriptionNL = descriptionNL;
    }

    public double getLatitude() {
        Log.v(TAG, "getLatitude() aangeroepen");
        return latitude;
    }

    public void setLatitude(double latitude) {
        Log.v(TAG, "setLatitude() aangeroepen");
        this.latitude = latitude;
    }

    public double getLongitude() {
        Log.v(TAG, "getLongitude() aangeroepen");
        return longitude;
    }

    public void setLongitude(double longitude) {
        Log.v(TAG, "setLongitude() aangeroepen");
        this.longitude = longitude;
    }

    public String getPhotographerEN() {
        Log.v(TAG, "getPhotograpgerEN() aangeroepen");
        return "photographer: " + this.photographer;
    }

    public String getPhotographerNL() {
        Log.v(TAG, "getPhotographerNL() aangeroepen");
        return "fotograaf: " + this.photographer;
    }

    public void setPhotographer(String photographer) {
        Log.v(TAG, "setPhotographer() aangeroepen");

        this.photographer = photographer;
    }

    public String getAuthor() {
        Log.v(TAG, "getAuthor() aangeroepen");

        return author;
    }

    public void setAuthor(String author) {
        Log.v(TAG, "setAuthor() aangeroepen");
        this.author = author;
    }

    public String getMaterialEN() {
        Log.v(TAG, "getMaterialEN() aangeroepen");
        return "Material: " + materialEN;
    }

    public void setMaterialEN(String materialEN) {
        Log.v(TAG, "setMaterialEN aangeroepen");
        this.materialEN = materialEN;

    }

    public String getMaterialNL() {
        Log.v(TAG, "getMaterialNL() aangeroepen");
        return "Materiaal: " + materialNL;
    }

    public void setMaterialNL(String materialNL) {
        Log.v(TAG, "setMaterialNL() aangeroepen");
        this.materialNL = materialNL;
    }

    public String getImageURLFrontpage() {
        Log.v(TAG, "getImageURLFrontpage() aangeroepen");
        return imageURLFrontpage;
    }

    public void setImageURLFrontpage(String imageURLFrontpage) {
        Log.v(TAG, "setImageURLFrontpage() aangeroepen");
        this.imageURLFrontpage = imageURLFrontpage;
    }

    public List<String> getImageURLDetails() {
        Log.v(TAG, "getImageURLDetails() aangeroepen");
        return imageURLDetails;
    }

    public void setImageURLDetails(List<String> imageURLDetails) {
        Log.v(TAG, "setImageURLDetails() aangeroepen");
        this.imageURLDetails = imageURLDetails;
    }

    public String getAddressEN() {
        Log.v(TAG, "getAddressEN() aangeroepen");
        return "Address: " + address;
    }

    public String getAddressNL(){

        Log.v(TAG, "getAddressNL() aangeroepen");
        return "Adres: " + this.address;
    }

    public void setAddress(String address) {
        Log.v(TAG, "setAddress() aangeroepen");
        this.address = address;
    }


}

