package com.example.blindwallgallery;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.nfc.Tag;
import android.util.Log;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class OpenJsonUtils {

    private static final String TAG = "OpenJsonUtils";

    public static List<Blindwall> getJsonData(Context context, String inputString) throws JSONException{
        Log.v(TAG, "getJsonData() aangeroepen");

        //Lijst met alle Blindwall objecten
        List<Blindwall> list = new ArrayList<>();

        //Object waar alle JSON data in komt te zitten
        JSONArray dataArray = new JSONArray(inputString);

        //Hier wordt geloopt door elke Blindwall in JSON object dataArray
        for (int i = 0; i < dataArray.length(); i++){

            //Huidige object wordt geïnstantieerd, waardoor het gebruikt kan worden om de details te koppelen
            JSONObject objectWall = dataArray.getJSONObject(i);

            String titleNL;
            String titleEN;
            String descriptionEN;
            String descriptionNL;
            double latitude;
            double longitude;
            String photographer;
            String author;
            String materialEN;
            String materialNL;
            String imageURLFrontpage = null;
            List<String> imageURLDetails = new ArrayList<>();
            String address;

            //Title array in een object zetten en hieruit de nl en en versie lezen
            JSONObject titleObject = objectWall.getJSONObject("title");
            titleEN = titleObject.getString("en");
            titleNL = titleObject.getString("nl");

            JSONObject description = objectWall.getJSONObject("description");
            descriptionEN = description.getString("en");
            descriptionNL = description.getString("nl");

            latitude = objectWall.getDouble("latitude");
            longitude = objectWall.getDouble("longitude");
            photographer = objectWall.getString("photographer");
            author = objectWall.getString("author");

            JSONObject material = objectWall.getJSONObject("material");
            materialEN = material.getString("en");
            materialNL = material.getString("nl");

            address = objectWall.getString("address") + " " + objectWall.getInt("numberOnMap");

            JSONArray images = objectWall.getJSONArray("images");
            for (int j = 0; j < images.length(); j++){
                JSONObject img = images.getJSONObject(j);
                if(img.getString("type").equals("frontpage")){
                    imageURLFrontpage = "https://api.blindwalls.gallery/" + img.getString("url");

                }else if (img.getString("type").equals("wall")){
                    imageURLDetails.add("https://api.blindwalls.gallery/" + img.getString("url"));

                }
            }

            //Nieuw Blindwall object aanmaken en data toevoegen
            Blindwall blindwall = new Blindwall();
            blindwall.setTitleEN(titleEN);
            blindwall.setTitleNL(titleNL);
            blindwall.setDescriptionEN(descriptionEN);
            blindwall.setDescriptionNL(descriptionNL);
            blindwall.setLatitude(latitude);
            blindwall.setLongitude(longitude);
            blindwall.setPhotographer(photographer);
            blindwall.setAuthor(author);
            blindwall.setMaterialEN(materialEN);
            blindwall.setMaterialNL(materialNL);
            blindwall.setImageURLFrontpage(imageURLFrontpage);
            blindwall.setImageURLDetails(imageURLDetails);
            blindwall.setAddress(address);

            //Log.v(TAG , blindwall.getTitleEN());

            list.add(blindwall);

        }

        return list;


    }



}
