package com.example.blindwallgallery;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class ImagesActivity extends AppCompatActivity {

    private static final String TAG = "ImagesActivity";

    private ImageView imageView;
    private int index = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Log.v(TAG, "onCreate() aangeroepen");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_images);

        imageView = findViewById(R.id.iv_images_fullscreen);

        final Intent intent = getIntent();
        final Blindwall blindwall = (Blindwall) intent.getExtras().getSerializable("Blindwall");

        Picasso.get().load(blindwall.getImageURLDetails().get(0)).into(imageView);

        String message = "Er zijn in totaal " + blindwall.getImageURLDetails().size() + " Afbeeldingen";
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();


        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (index != (blindwall.getImageURLDetails().size()-1)){
                    index++;
                    Picasso.get().load(blindwall.getImageURLDetails().get(index)).into(imageView);
                }else {
                    index = 0;
                    Picasso.get().load(blindwall.getImageURLDetails().get(index)).into(imageView);

                }
            }
        });

        //Terugbutton op de menubalk
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    //Zorgt ervoor dat de terugbutton werkt
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.v(TAG, "onOptionsItemSelected() aangeroepen");

        int id = item.getItemId();
        if (id == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
