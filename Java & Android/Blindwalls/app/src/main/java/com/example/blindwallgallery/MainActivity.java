package com.example.blindwallgallery;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private String sourceUrl = "https://api.blindwalls.gallery/apiv2/murals";
    private String language = "en";


    //Elke RecyclerView activity moet deze drie variabelen gedefinieerd hebben:
    private RecyclerView mRecyclerView;
    private BlindwallAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private List<Blindwall> blindwalls;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.v(TAG, "onCreate aangeroepen");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //Hiermee wordt de geselecteerde URL uit de settings opgehaald
        setupSharedPreferences();


        //RecyclerView moet gekoppeld worden aan de layout.xml die de layout van de activity beschrijft (activity_main.xml)
        mRecyclerView = findViewById(R.id.recycler_view_Blindwall_list);

        //LayoutManager moet een handige LayoutManager zijn, in dit geval LinearLayout
        mLayoutManager = new LinearLayoutManager(this);

        //LayoutManager koppelen aan de RecyclerView
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new BlindwallAdapter(this);

        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setContext(this);

        setData();
        Toast.makeText(this, "Data has been retrieved", Toast.LENGTH_SHORT).show();
    }

    public void setData() {
        Log.v(TAG, "setData() aangeroepen");
        mRecyclerView.setVisibility(View.VISIBLE);
        new FetchBlindwallsAsyncTask().execute();

    }


    public class FetchBlindwallsAsyncTask extends AsyncTask<Void, Void, List<Blindwall>> {

        //Als deze wordt aangeroepen maakt het contact met het netwerk

        private static final String TAG = "FetchBlindwalsAsyncTask";

        @Override
        protected List<Blindwall> doInBackground(Void... voids) {

            Log.v(TAG, "doInBackground() aangeroepen");

            URL inputUrl = NetworkUtils.buildUrl(sourceUrl);
            String jsonResponse = null;
            List<Blindwall> result;


            try {
                //JSON ophalen van server
                jsonResponse = NetworkUtils.getResponseFromHttpUrl(inputUrl);

                result = OpenJsonUtils.getJsonData(MainActivity.this, jsonResponse);
                return result;

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

        }

        @Override
        protected void onPostExecute(List<Blindwall> list) {

            Log.v(TAG, "onPostExecute aangeroepen");

            if (list != null) {
                mAdapter.setBlindwallsData(list);
                mAdapter.setLanguage(language);

            }
        }
    }

    //Vullen van menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        Log.v(TAG, "onCreateOptionsMenu() aangeroepen");

        //Zorgt ervoor dat je drie puntjes krijgt
        //Met daarin het menu
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    //Deze methode wordt aangeroepen als er op een menuItem geklikt wordt
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Log.v(TAG, "onOptionsIntemSelected() aangeroepen");

        int itemThatWasClickedId = item.getItemId();

        if (itemThatWasClickedId == R.id.action_settings) {
            Intent intentPreferences = new Intent(getApplicationContext(),
                    SettingsActivity.class);
            startActivity(intentPreferences);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void setupSharedPreferences() {
        //Ophalen van alle waardes uit de settings

        Log.v(TAG, "loadUrlFromPreferences aangeroepen");

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        loadUrlFromPreferences(sharedPreferences);
        loadLanguageFromPreferences(sharedPreferences);
    }

    //De geselecteerde string wordt ingesteld als sourceUrl
    private void loadUrlFromPreferences(SharedPreferences sharedPreferences) {

        Log.v(TAG, "loadLanguageFromPreferences() aangeroepen");

        sourceUrl = sharedPreferences.getString("pref_url", "https://api.blindwalls.gallery/apiv2/murals");
    }

    private void loadLanguageFromPreferences(SharedPreferences sharedPreferences) {

        Log.v(TAG, "loadLanguageFromPreferences() aangeroepen");

        language = sharedPreferences.getString("pref_language", "en");
    }


    @Override
    public void onRestart() {
        //Home pagina altijd up to date
        Log.v(TAG, "MainActivity restarted");

        super.onRestart();
        finish();
        startActivity(getIntent());
    }

}
