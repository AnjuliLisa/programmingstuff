package Data;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AccountTest {

    @Test
    void TestGetLastNameReturnsLastName() {
        //Arrange: het opzetten van objecten en andere zaken die nodig zijn voor de test
        Account newAccount = new Account( "Henk", "Berg", "Pannenkoekstraat", 34, "", "Rotterdam", "3094KM");
        // Act: het daadwerkelijk aanroepen van de unit die je wil testen

        String lastName = newAccount.getLastName();
        // Assert: Vergelijk de uitkomst van de methode met je verwachting, dit doe je met assertion.
        Assertions.assertTrue(lastName.equals("Berg"));


    }
}