package Data;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;

public class GetdataTest {


    @Test
    void testFilmIDIs11(){
        //Arrange
        String movieTitle = "A Clockwork Orange";
        MovieRepository repository = new MovieRepository();
        //Act
        int id = repository.getMovieID(movieTitle);
        //Assert
        Assertions.assertTrue(id == 11);
    }

    @Test
        // Test if a DatabaseConnection is possible.
    void testDatabaseConnection() {
        // Arrange
        boolean hasConnection;
        SqlConnection databaseConnection = new SqlConnection();
        // Act
        databaseConnection.connectDatabase();
        Connection connected = databaseConnection.getConnection();
        hasConnection = connected != null;
        // Assert
        Assertions.assertTrue(hasConnection, "Database connection could not be established.");
        databaseConnection.disconnectDatabase();
    }

}
