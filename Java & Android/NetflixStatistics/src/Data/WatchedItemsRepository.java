package Data;

import java.sql.ResultSet;
import java.util.ArrayList;

public class WatchedItemsRepository {

    //class that fetched all the data from the database related to watched episodes and movies connected to a profile

    SqlConnection sqlConnection;

    //Constructor
    public WatchedItemsRepository(){
        sqlConnection = new SqlConnection();
    }

    //Reads everything from watched items
    public ArrayList<WatchedItems> readAll(){
        ArrayList<WatchedItems> list = new ArrayList<>();
        try{
            sqlConnection.connectDatabase();
            String query = "SELECT * FROM Kijkgedrag;";
            ResultSet rs = sqlConnection.executeSql(query);
            while(rs.next()){
                list.add(new WatchedItems(rs.getInt("ProfileID"), rs.getString("Serie"), rs.getInt("AfleveringID"), rs.getInt("FilmID"), rs.getDouble("Percentage_gekeken")));
            }
            sqlConnection.disconnectDatabase();
        }catch (Exception e){
            System.out.println(e);
        }

        return list;

    }

    //Method that return only watched episodes
    public ArrayList<String> readWatchedEpisodes(int profileID) {
        ArrayList<String> list = new ArrayList<>();
        try {
            sqlConnection.connectDatabase();
            String query = "SELECT Aflevering.Titel FROM Aflevering " +
                    "INNER JOIN Kijkgedrag ON Kijkgedrag.AfleveringID = Aflevering.AfleveringID " +
                    "WHERE Kijkgedrag.ProfileID = " + profileID + ";";
            ResultSet rs = sqlConnection.executeSql(query);
            while (rs.next()) {
                list.add(rs.getString("Titel"));
            }

            sqlConnection.disconnectDatabase();
        } catch (Exception e) {
            System.out.println(e);
        }

        return list;

    }

    //Method that returns all watched movies from a given ProfileID
    public ArrayList<String> readWatchedMovies(int profileID){
        ArrayList<String> list = new ArrayList<>();
        try{
            sqlConnection.connectDatabase();
            String query = "SELECT Film.Titel FROM Film " +
                    "INNER JOIN Kijkgedrag ON Kijkgedrag.FilmID = Film.FilmID " +
                    "WHERE Kijkgedrag.ProfileID = " + profileID + ";";
            ResultSet rs = sqlConnection.executeSql(query);
            while (rs.next()){
                list.add(rs.getString("Titel"));
            }

            sqlConnection.disconnectDatabase();
        }catch (Exception e){
            System.out.println(e);
        }

        return list;
    }

    //Method that updates Serie/Film with given WatchedItemId
    public void updateWatchedItem(int ID, double data){
        try{
            sqlConnection.connectDatabase();
            String query = "UPDATE Kijkgedrag " +
                    "SET Percentage_gekeken = " + data +
                    " WHERE ID = " +  ID +  ";";
            sqlConnection.executeSqlNoResult(query);
            sqlConnection.disconnectDatabase();
        }catch (Exception e){
            System.out.println(e);
        }
    }

    //Method to get ID watched serie from Database
    public int getIDSerie(int accountID, int profileID, int episodeID, String serie){
        int serieID = 0;
        try{
            sqlConnection.connectDatabase();
            String query = "SELECT Kijkgedrag.ID " +
                    "FROM Kijkgedrag " +
                    "JOIN [Profile] ON [Profile].ProfileID = Kijkgedrag.ProfileID " +
                    "WHERE Serie = '" + serie + "' " +
                    "AND Kijkgedrag.AfleveringID = " + episodeID +
                    "AND Kijkgedrag.ProfileID = " + profileID +
                    "AND [Profile].AccountID = " + accountID + ";";
            ResultSet rs = sqlConnection.executeSql(query);
            while (rs.next()){
                serieID = rs.getInt("ID");
            }
            sqlConnection.disconnectDatabase();
        }catch (Exception e){
            System.out.println(e);
        }
        return serieID;

    }

    //Method to get ID watched film from database
    public int getIDFilm(int accountID, int profileID, int filmID){
        int ID = 0;
        try{
            sqlConnection.connectDatabase();
            String query = "SELECT Kijkgedrag.ID " +
                    "FROM Kijkgedrag " +
                    "JOIN [Profile] ON [Profile].ProfileID = Kijkgedrag.ProfileID\n" +
                    "WHERE Kijkgedrag.FilmID = " + filmID +
                    " AND Kijkgedrag.ProfileID = " + profileID +
                    " AND [Profile].AccountID = " + accountID + ";";
            ResultSet rs = sqlConnection.executeSql(query);
            while(rs.next()){
                ID = rs.getInt("ID");
            }
            sqlConnection.disconnectDatabase();
        }catch (Exception e){
            System.out.println(e);
        }
        return ID;
    }

    //Method that returns the watched series
    public ArrayList<String> readWatchedSeries(int profileID){
        ArrayList<String> list = new ArrayList<>();
        try{
            sqlConnection.connectDatabase();
            String query = "SELECT Serie " +
                    "FROM Kijkgedrag " +
                    "WHERE ProfileID = " + profileID +  " AND Serie IS NOT Null;";
            ResultSet rs = sqlConnection.executeSql(query);
            while(rs.next()){
                list.add(rs.getString("Serie"));
            }
            sqlConnection.disconnectDatabase();
        }catch (Exception e){
            System.out.println(e);
        }
        return list;
    }

    //Method to add a movie to database
    public void addFilm(int profileID, String film, double data ){
        try{
            sqlConnection.connectDatabase();
            String query = "INSERT INTO Kijkgedrag(ProfileID, FilmID, Percentage_gekeken) \n" +
                    "VALUES(" + profileID + ", (SELECT  FilmID FROM Film WHERE Titel = '" + film  + "'), " + data + ");";
            sqlConnection.executeSqlNoResult(query);
            sqlConnection.disconnectDatabase();
        }catch (Exception e){
            System.out.println(e);
        }
    }

    //Method to add serie-episode to database
    public void addSerie(int profileID, String serie, String episode, double data){
        try{
            sqlConnection.connectDatabase();
            String query = "INSERT INTO Kijkgedrag(ProfileID, Serie, AfleveringID, Percentage_gekeken) \n" +
                    "VALUES(" + profileID + ", '" + serie + "', (SELECT AfleveringID FROM Aflevering WHERE Titel = '" + episode + "'), " + data + " );";
            sqlConnection.executeSqlNoResult(query);
            sqlConnection.disconnectDatabase();
        }catch (Exception e){
            System.out.println(e);
        }
    }

    //Method to delete Film/Serie with given itemID
    public void deleteWatchedItem(int itemID){
        try{
            sqlConnection.connectDatabase();
            String query = "DELETE Kijkgedrag " +
                    "WHERE ID = " + itemID + ";";
            sqlConnection.executeSqlNoResult(query);
            sqlConnection.disconnectDatabase();
        }catch (Exception e){
            System.out.println(e);
        }
    }



}
