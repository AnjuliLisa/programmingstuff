package Data;

import java.util.ArrayList;

public class Serie {

    private ArrayList<Episode> epsisodes;
    private String serie;
    private String related;

    public Serie(String serie, String related) {
        this.serie = serie;
        this.related = related;
    }

    public String getSerie() {
        return serie;
    }


    public String toString(){
        return this.serie;
    }
}
