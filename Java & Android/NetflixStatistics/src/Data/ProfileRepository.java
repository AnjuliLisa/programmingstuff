package Data;

import java.sql.*;
import java.util.*;

public class ProfileRepository {

    //class that fetched all the data from the database related to profiles


    private SqlConnection sqlConnection;

    //Constructor
    public ProfileRepository() {
        sqlConnection = new SqlConnection();
    }

    //Get all profile data from database
    public ArrayList<Profile> readAll() {
        ArrayList<Profile> list = new ArrayList<>();
        try {
            sqlConnection.connectDatabase();
            String query = "SELECT * FROM Profile";
            ResultSet rs = sqlConnection.executeSql(query);
            while(rs.next()) {
                list.add(new Profile(rs.getInt("AccountID"), rs.getString("ProfielNaam"), rs.getInt("Leeftijd")));
            }
            sqlConnection.disconnectDatabase();
        }
        catch(Exception e) {
            System.out.println(e);
        }
        return list;
    }

    //method that reads all data from one profile with given profileID
    public ArrayList<Profile> readProfile(int profileID){
        ArrayList<Profile> list = new ArrayList<>();
        try {
            sqlConnection.connectDatabase();

            String query = "SELECT * FROM Profile WHERE ProfileID = " + profileID + ";";
            ResultSet rs = sqlConnection.executeSql(query);
            while(rs.next()) {
                list.add(new Profile(rs.getInt("AccountID") ,rs.getString("ProfielNaam"), rs.getInt("Leeftijd")));
            }
            sqlConnection.disconnectDatabase();
        }
        catch(Exception e) {
            System.out.println(e);
        }
        return list;
    }

    //Method to delete profile
    public void deleteProfile(int profileID){

            try {
                sqlConnection.connectDatabase();

                String query = "DELETE [Profile] WHERE ProfileID = " + profileID;
                sqlConnection.executeSqlNoResult(query);

                sqlConnection.disconnectDatabase();
            }
            catch(Exception e) {
                System.out.println(e);
            }

    }

    //method to read profiles connected to given account ID
    public ArrayList<Profile> readFromAccount(int accountID){
        ArrayList<Profile> list = new ArrayList<>();
        try {
            sqlConnection.connectDatabase();
            String query = "SELECT * FROM Profile WHERE AccountID = " + accountID + ";";
            ResultSet rs = sqlConnection.executeSql(query);
            while(rs.next()) {
                list.add(new Profile(rs.getInt("AccountID") ,rs.getString("ProfielNaam"), rs.getInt("Leeftijd")));
            }
            sqlConnection.disconnectDatabase();
        }
        catch(Exception e) {
            System.out.println(e);
        }
        return list;
    }

    //Method to create profile
    public void createProfile(int accountID, String name, int age){
        try{
            sqlConnection.connectDatabase();
            String query = "INSERT INTO Profile(AccountID, ProfielNaam, Leeftijd) " +
                    "VALUES(" + accountID + ", '" + name + "', " + age + ");";
            sqlConnection.executeSqlNoResult(query);
            sqlConnection.disconnectDatabase();
        }catch (Exception e){
            System.out.println(e);
        }
    }

    //method to get a profileID
    public int getProfileID(int accountID, String name){
        int profileID = 0;
        try{
            sqlConnection.connectDatabase();
            String query = "SELECT ProfileID FROM Profile WHERE AccountID = " + accountID + " AND ProfielNaam = '" + name + "';";
            ResultSet rs =  sqlConnection.executeSql(query);

            while(rs.next()){
                profileID = rs.getInt("ProfileID");
            }
            sqlConnection.disconnectDatabase();
        }catch (Exception e){
            System.out.println(e);
        }
        return profileID;
    }

    //Method to update a profile with given profileID
    public void updateProfile(String newData, String column, int profileID){
        try
        {
            sqlConnection.connectDatabase();
            String query = "UPDATE Profile SET " + column + " = '" + newData + "' WHERE ProfileID = '" + profileID + "';";
            sqlConnection.executeSqlNoResult(query);
            sqlConnection.disconnectDatabase();
        }
        catch(Exception e) {
            System.out.println(e);
        }
    }

    //Method to get a profile name by given profileID
    public String getProfileName(int profileID){
        String profileName = null;
        try{
            sqlConnection.connectDatabase();
            String query = "SELECT ProfielNaam FROM Profile WHERE ProfileID = " + profileID + ";";
            ResultSet rs = sqlConnection.executeSql(query);
            while(rs.next()){
                profileName = rs.getString("ProfielNaam");
            }
            sqlConnection.disconnectDatabase();
        }catch (Exception e){
            System.out.println(e);
        }

        return profileName;
    }

    //method to give all profile names in an ArrayList
    public ArrayList<String> getAllProfileNames(){
        ArrayList<String> list = new ArrayList<>();
        try{
            sqlConnection.connectDatabase();
            String query = "SELECT ProfielNaam FROM Profile;";
            ResultSet rs = sqlConnection.executeSql(query);
            while (rs.next()){
                list.add(rs.getString("ProfielNaam"));
            }
            sqlConnection.disconnectDatabase();
        }catch (Exception e){
            System.out.println(e);
        }
        return list;
    }

    //method to het the age of a given profileID
    public int getProfileAge(int profileID){
        int profileAge = 0;
        try{
            sqlConnection.connectDatabase();
            String query = "SELECT Leeftijd FROM Profile WHERE ProfileID = " + profileID + ";";
            ResultSet rs = sqlConnection.executeSql(query);
            while (rs.next()){
                profileAge = rs.getInt("Leeftijd");
            }
            sqlConnection.disconnectDatabase();
        } catch (Exception e){
            System.out.println(e);
        }
        return profileAge;
    }

    //method to get the amount of profiles a account has
    public int getAmountOfProfiles(int accountID){
        int amount = 0;
        try{
            sqlConnection.connectDatabase();
            String query = "  SELECT COUNT(ProfileID) AS AantalAccounts FROM Profile WHERE AccountID = " + accountID + " GROUP BY AccountID;";
            ResultSet rs = sqlConnection.executeSql(query);
            while (rs.next()){
                amount = rs.getInt("AantalAccounts");
            }
            sqlConnection.disconnectDatabase();
        }catch (Exception e){
            System.out.println(e);
        }
        return amount;

    }


}

