package Data;

import java.sql.Time;


public class Movie {

    private String title;
    private String ageCategory;
    private String language;
    private Time playingtTime;
    private String genre;

    public Movie(String title, String ageCategory, String language, Time playingtTime, String genre) {
        this.title = title;
        this.ageCategory = ageCategory;
        this.language = language;
        this.playingtTime = playingtTime;
        this.genre = genre;
    }

    @Override
    public String toString() {
        return this.title;
    }

}