package Data;

import java.sql.ResultSet;
import java.util.ArrayList;

public class SerieRepository {

    //class that fetched all the data from the database related to Series and episodes

    private SqlConnection sqlConnection = new SqlConnection();

    //Constructor
    public SerieRepository(){
    }

    //Method to read all the Serie names from the database and return an Arraylist of it
    public ArrayList<Serie> readAll() {

        ArrayList<Serie> list = new ArrayList<>();
        try {
            sqlConnection.connectDatabase();
            ResultSet rs = sqlConnection.executeSql("SELECT * FROM Serie");
            //Resultaten uit Resultset toevoegen aan Accounts
            while(rs.next()) {
                list.add(new Serie(rs.getString("SerieNaam"), rs.getString("Vergelijkbaar_aan")));
            }
            //Connectie met database afsluiten
            sqlConnection.disconnectDatabase();
        }
        catch(Exception e) {
            System.out.println(e);
        }
        return list;
    }

    //method to read all the episodes of a given serie
    public ArrayList<Episode> readEpisodes(String serie){
        ArrayList<Episode> list = new ArrayList<>();
        try{
            sqlConnection.connectDatabase();
            String query = "SELECT * " +
                    "FROM Aflevering " +
                    "WHERE [Serie]= '" + serie + "';";
            ResultSet rs = sqlConnection.executeSql(query);
            while(rs.next()){
                list.add(new Episode(rs.getString("Serie"), rs.getString("Titel"), rs.getTime("Tijdsduur"), rs.getString("Leeftijdscategorie"), rs.getString("Taal"), rs.getString("Genre")));
            }
            sqlConnection.disconnectDatabase();
        }catch (Exception e){
            System.out.println(e);
        }

        return list;
    }

    //method to get the episodeID with an given episode name
    public int EpisodeID(String episode){
        int episodeID = 0;
        try{
            sqlConnection.connectDatabase();
            String query = "SELECT AfleveringID FROM Aflevering WHERE Titel = '" + episode + "';";
            ResultSet rs = sqlConnection.executeSql(query);
            while (rs.next()){
                episodeID = rs.getInt("AfleveringID");
            }
            sqlConnection.disconnectDatabase();
        }catch (Exception e){
            System.out.println(e);
        }
        return episodeID;
    }

}
