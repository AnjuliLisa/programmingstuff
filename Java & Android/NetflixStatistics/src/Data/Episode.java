package Data;

import java.sql.Time;

public class Episode {

    private String serie;
    private String title;
    private Time playingTime;
    private String ageCategory;
    private String language;
    private String genre;

    public Episode(String serie, String title, Time playingTime, String ageCategory, String language, String genre) {
        this.serie = serie;
        this.title = title;
        this.playingTime = playingTime;
        this.ageCategory = ageCategory;
        this.language = language;
        this.genre = genre;
    }

    public String toString(){
        return this.title;
    }
}
