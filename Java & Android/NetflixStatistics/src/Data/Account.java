package Data;

import java.util.ArrayList;

public class Account {

    private ArrayList<Profile> profiles;
    private String firstName;
    private String lastName;
    private String streetName;
    private int houseNumber;
    private String numberSuffix;
    private String city;
    private String postalCode;

    public Account(String firstName, String lastName, String streetName, int houseNumber, String numberSuffix, String city, String postalCode) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.streetName = streetName;
        this.houseNumber = houseNumber;
        this.numberSuffix = numberSuffix;
        this.city = city;
        this.postalCode = postalCode;
    }


    public String getFirstName() {
        return firstName;
    }


    public String getLastName() {
        return lastName;
    }


    public String getStreetName() {
        return streetName;
    }


    public int getHouseNumber() {
        return houseNumber;
    }


    public String getNumberSuffix() {
        return numberSuffix;
    }


    public String getCity() {
        return city;
    }


    public String getPostalCode() {
        return postalCode;
    }


    @Override
    public String toString() {
        return this.firstName + " " + this.lastName;
    }
}
