package Data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class SqlConnection {

    //connection with database

    private Connection connection;
    String connectionUrl = "jdbc:sqlserver://localhost\\MSSQLSERVER;databaseName=Netflix;integratedSecurity=true;";

        public void connectDatabase() {
            try{
                Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                // Maak de verbinding met de database.
                connection = DriverManager.getConnection(connectionUrl);

            }
            catch(Exception e)
            {
                System.out.println(e);
                connection=null;
            }

        }

        public void disconnectDatabase() {
            if (connection != null) {
                try {
                    connection.close();
                }
                catch(Exception e) {
                    System.out.println(e);
                }
                connection=null;
            }
        }

        public ResultSet executeSql(String sqlQuery) {
            ResultSet rs = null;
            try
            {
                Statement statement = this.connection.createStatement();
                rs= statement.executeQuery(sqlQuery);
            }
            catch(Exception e)
            {
                System.out.println(e);
            }
            return rs;
        }

        public void executeSqlNoResult(String sqlQuery) {
            try
            {
                Statement statement = this.connection.createStatement();
                statement.execute(sqlQuery);
            }
            catch(Exception e)
            {
                System.out.println(e);
            }
        }

    public Connection getConnection() {
        if(this.connection != null) {
            return this.connection;
        } else {
            return null;
        }
    }
}


