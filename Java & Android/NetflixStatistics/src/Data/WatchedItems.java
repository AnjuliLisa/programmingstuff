package Data;

import java.util.ArrayList;

public class WatchedItems {

    private ArrayList<Movie> movies;
    private ArrayList<Episode> episodes;
    private int profileId;
    private String serie;
    private int episodeId;
    private int filmId;
    private double percentageWatched;

    public WatchedItems(int profileId, String serie, int episodeId, int filmId, double percentageWatched) {
        this.profileId = profileId;
        this.serie = serie;
        this.episodeId = episodeId;
        this.filmId = filmId;
        this.percentageWatched = percentageWatched;
    }

    public int getProfileId() {
        return profileId;
    }

    public String getSerie() {
        return serie;
    }

    public int getEpisodeId() {
        return episodeId;
    }

    public int getFilmId() {
        return filmId;
    }

    public double getPercentageWatched() {
        return percentageWatched;
    }

    @Override
    public String toString(){
        return this.filmId + "  " + this.episodeId;
        }
    }

