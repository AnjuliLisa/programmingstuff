package Data;

import java.util.ArrayList;

public class Profile {

    private ArrayList<WatchedItems> watchedItems;
    private int accountId;
    private String profileName;
    private int age;

    public Profile(int accountId, String profileName, int age) {
        this.accountId = accountId;
        this.profileName = profileName;
        this.age = age;
    }

    public String getName(){
        return this.profileName;
    }

    public String toString(){
        return this.profileName;
    }


}
