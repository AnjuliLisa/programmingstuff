package Data;

import java.sql.ResultSet;
import java.util.ArrayList;

public class MovieRepository {

    //class that fetched all the data from the database related to movies


    private SqlConnection sqlConnection = new SqlConnection();

    //reads all data from all movies in database
    public ArrayList<Movie> readAll() {
        ArrayList<Movie> lijst = new ArrayList<>();
        try {
            sqlConnection.connectDatabase();
            ResultSet rs = sqlConnection.executeSql("SELECT * FROM FILM");
            while(rs.next()) {
                lijst.add(new Movie(rs.getString("Titel"),rs.getString("Leeftijdscategorie"), rs.getString("Taal"), rs.getTime("Tijdsduur"), rs.getString("Genre")));
            }
            sqlConnection.disconnectDatabase();
        }
        catch(Exception e) {
            System.out.println(e);
        }
        return lijst;
    }

    //method to get the movieID from the database with a given movie name
    public int getMovieID(String movieName){
        int movieID = 0;
        try{
            sqlConnection.connectDatabase();
            String query = "SELECT FilmID FROM Film WHERE Titel = '" + movieName + "';";
            ResultSet rs = sqlConnection.executeSql(query);
            while (rs.next()){
                movieID = rs.getInt("FilmID");

            }
            sqlConnection.disconnectDatabase();
        }catch (Exception e){
            System.out.println(e);
        }

        return movieID;
    }


}
