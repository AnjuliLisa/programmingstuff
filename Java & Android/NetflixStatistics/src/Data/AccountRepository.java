package Data;

import java.lang.reflect.Array;

import java.sql.*;
import java.util.*;

public class AccountRepository {

    //class that fetched all the data from the database related to Accounts


    private SqlConnection sqlConnection;

    //Constructor
    public AccountRepository() {
        this.sqlConnection = new SqlConnection();
    }

    //reads all the accounts stored in the database and collects them in an ArrayList
    public ArrayList<Account> readAll() {
        ArrayList<Account> lijst = new ArrayList<>();
        try {
            //Connectie maken met de database
            sqlConnection.connectDatabase();
            //Met SQl query data opslaan in resultset
            ResultSet rs = sqlConnection.executeSql("SELECT * FROM ACCOUNT");
            //Resultaten uit Resultset toevoegen aan Accounts
            while(rs.next()) {
                lijst.add(new Account(rs.getString("Voornaam"), rs.getString("Achternaam"), rs.getString("Straatnaam"), rs.getInt("Huisnummer"), rs.getString("Toevoeging"), rs.getString("Woonplaats"), rs.getString("Postcode")));
            }
            //Connectie met database afsluiten
            sqlConnection.disconnectDatabase();
        }
        catch(Exception e) {
            System.out.println(e);
        }
        return lijst;
    }

    //Creates a new account in the database
    public void create(Account account) {
        try
        {
            sqlConnection.connectDatabase();

            //let op: het samenvoegen van strings binnen SQL commando's is ONVEILIG. Pas dit niet toe in een productieomgeving.
            //later in het curriculum wordt behandeld op welke wijze je je hiertegen kunt beschermen.
            String sqlQuery = "INSERT INTO ACCOUNT VALUES ('" + account.getFirstName() + "', '" + account.getLastName() + "', '" + account.getStreetName() + "', '" + account.getHouseNumber() + "', '" + account.getNumberSuffix() + "', '" + account.getCity() + "', '" + account.getPostalCode() + "');";
            sqlConnection.executeSqlNoResult(sqlQuery);
            sqlConnection.disconnectDatabase();
        }
        catch(Exception e) {
            System.out.println(e);
        }
    }

    //Deletes account from database with given first name and last name
    public void delete(String firstName, String lastName) {
        try
        {
            sqlConnection.connectDatabase();
            String query = "DELETE ACCOUNT WHERE Voornaam = '" + firstName + "' AND Achternaam = '" + lastName + "';";
            sqlConnection.executeSqlNoResult(query);
            sqlConnection.disconnectDatabase();
        }
        catch(Exception e) {
            System.out.println(e);
        }
    }

    //Updates an account in database with given name,column and new data
    public void update(String firstName, String lastName, String column, String newData){
        try
        {
            sqlConnection.connectDatabase();
            String query = "UPDATE Account SET " + column + " = '" + newData + "' WHERE Voornaam = '" + firstName + "' AND Achternaam = '" + lastName + "';";
            sqlConnection.executeSqlNoResult(query);
            sqlConnection.disconnectDatabase();
        }
        catch(Exception e) {
            System.out.println(e);
        }

    }

    //method to retrieve the accountID
    public int getAccountID(String text){
        int accountID = 0;
        String[] splitted = text.split(" ");
        String firstName = Array.get(splitted, 0).toString();
        String lastName = Array.get(splitted, 1).toString();

        try{
            sqlConnection.connectDatabase();
            String query = "SELECT AccountID FROM Account WHERE Voornaam = '" + firstName + "' AND Achternaam = '" + lastName + "';";
            ResultSet rs = sqlConnection.executeSql(query);

            while(rs.next()){
                accountID = rs.getInt("AccountID");
            }
        }catch(Exception e){
            System.out.println(e);
        }
        return accountID;
    }

}
