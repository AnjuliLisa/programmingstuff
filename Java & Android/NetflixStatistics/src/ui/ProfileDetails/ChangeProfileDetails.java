package ui.ProfileDetails;

import Data.Account;
import Data.AccountRepository;
import Data.Profile;
import Data.ProfileRepository;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import static javax.swing.JOptionPane.ERROR_MESSAGE;

public class ChangeProfileDetails extends JPanel {

    //Panel to change the details of a profile

    private JComboBox accounts;
    private JComboBox profiles;
    private JRadioButton name, age;
    private JTextField newData;
    private JButton save;
    private AccountRepository accountRepository = new AccountRepository();
    private ProfileRepository profileRepository = new ProfileRepository();

    public ChangeProfileDetails() {
        createComponents();
        this.setLayout(new FlowLayout(FlowLayout.LEFT));
    }

    public void createComponents(){

        ArrayList<Account> acc = accountRepository.readAll();
        accounts = new JComboBox(acc.toArray());
        ItemListen itemListen = new ItemListen();
        accounts.addItemListener(itemListen);
        this.add(accounts);

        int accountID = accountRepository.getAccountID(accounts.getSelectedItem().toString());

        ArrayList<Profile> list = profileRepository.readFromAccount(accountID);
        profiles = new JComboBox(list.toArray());
        this.add(profiles);

        JLabel title = new JLabel("Wat wil je wijzigen?");
        this.add(title);

        name = new JRadioButton("Naam");
        name.setSelected(true);
        age = new JRadioButton("Leeftijd");

        ButtonGroup options = new ButtonGroup();
        options.add(name);
        options.add(age);

        JPanel buttons = new JPanel();
        Border operationBorder = BorderFactory.createTitledBorder("Details");
        buttons.setBorder(operationBorder);
        buttons.add(name);
        buttons.add(age);

        this.add(buttons);

        newData = new JTextField("", 20);
        this.add(newData);

        save = new JButton("Opslaan");
        ChangeProfileListener listener = new ChangeProfileListener();
        save.addActionListener(listener);
        this.add(save);

    }

    private class ChangeProfileListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {

            int accountID = accountRepository.getAccountID(accounts.getSelectedItem().toString());
            int profileID = profileRepository.getProfileID(accountID, profiles.getSelectedItem().toString());

            if (e.getSource() == save){
                if (name.isSelected()){
                    profileRepository.updateProfile(newData.getText(), "ProfielNaam", profileID);

                    String message = "Succesvol opgeslagen!";
                    JOptionPane.showMessageDialog(null, message, "Informatie", JOptionPane.INFORMATION_MESSAGE);

                }else if (age.isSelected()){
                    try{
                        int number = Integer.parseInt(newData.getText());
                        profileRepository.updateProfile(newData.getText(), "Leeftijd", profileID);

                        String message = "Succesvol opgeslagen!";
                        JOptionPane.showMessageDialog(null, message,"Informatie", JOptionPane.INFORMATION_MESSAGE);
                    }catch (NumberFormatException ex) {
                        String message = "Leeftijd kan alleen een getal zijn!";
                        JOptionPane.showMessageDialog(null, message,"Error Message", ERROR_MESSAGE);
                    }



                }
            }
            newData.setText("");
        }
    }

    private class ItemListen implements ItemListener {

        public void itemStateChanged(ItemEvent event) {
            if (event.getStateChange() == ItemEvent.SELECTED) {
                Object item = event.getItem();
                String selectedAccount = item.toString();
                int accountID = accountRepository.getAccountID(selectedAccount);
                profiles.removeAllItems();
                ArrayList<Profile> list = profileRepository.readFromAccount(accountID);
                for (Profile prof : list)
                    profiles.addItem(prof);

            }
        }
    }


}
