package ui.ProfileDetails;

import Data.AccountRepository;
import Data.Profile;
import Data.ProfileRepository;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

public class DeleteProfile extends JPanel {

    //panel to delete a profile

    private JComboBox boxAccount;
    private JComboBox boxProfile;
    private JButton delete;
    private int accountID;
    private AccountRepository accountRepo = new AccountRepository();
    private ProfileRepository profileRepo = new ProfileRepository();

    public DeleteProfile() {
        createComponents();
    }

    public void createComponents(){

        ArrayList accounts = accountRepo.readAll();
        boxAccount = new JComboBox(accounts.toArray());
        ItemListen itemListen = new ItemListen();
        boxAccount.addItemListener(itemListen);

        ArrayList profiles = profileRepo.readFromAccount(accountRepo.getAccountID(boxAccount.getSelectedItem().toString()));
        boxProfile = new JComboBox(profiles.toArray());

        delete = new JButton("Verwijder Profiel");
        DeleteProfileListener listener = new DeleteProfileListener();
        delete.addActionListener(listener);

        add(boxAccount);
        add(boxProfile);
        add(delete);

    }

    private class DeleteProfileListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {

            accountID = accountRepo.getAccountID(boxAccount.getSelectedItem().toString());

            if (e.getSource() == delete) {
                String name = (boxProfile.getSelectedItem().toString());

                int profileID = profileRepo.getProfileID(accountID, name);
                profileRepo.deleteProfile(profileID);

                boxProfile.removeAllItems();
                ArrayList<Profile> newItems = profileRepo.readFromAccount(accountID);
                for (Profile item : newItems) {
                    boxProfile.addItem(item);
                }
                String message = "Succesvol verwijderd!";
                JOptionPane.showMessageDialog(null, message, "Informatie", JOptionPane.INFORMATION_MESSAGE);

            }

        }
    }

        private class ItemListen implements ItemListener {

            public void itemStateChanged(ItemEvent event) {
                if (event.getStateChange() == ItemEvent.SELECTED) {
                    Object item = event.getItem();
                    String selectedAccount = item.toString();
                    int accountID = accountRepo.getAccountID(selectedAccount);
                    boxProfile.removeAllItems();
                    ArrayList<Profile> list = profileRepo.readFromAccount(accountID);
                    for (Profile prof : list)
                        boxProfile.addItem(prof);

                }
            }
        }
}

