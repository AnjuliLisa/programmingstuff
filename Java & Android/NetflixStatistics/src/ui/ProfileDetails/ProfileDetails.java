package ui.ProfileDetails;


import Data.Account;
import Data.AccountRepository;
import Data.Profile;
import Data.ProfileRepository;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class ProfileDetails extends JPanel {

    //panel to read details of a profile

    private JLabel name, resultName, age, resultAge;
    private JButton show;
    private JComboBox accounts, profiles;
    private AccountRepository accountRepository = new AccountRepository();
    private ProfileRepository profileRepository = new ProfileRepository();

    public ProfileDetails(){
        createComponents();
        this.setLayout(new GridLayout(3, 3, 10, 10));
    }

    private void createComponents() {

        ArrayList<Account> acc = accountRepository.readAll();
        accounts = new JComboBox(acc.toArray());
        ItemListen itemListen = new ItemListen();
        accounts.addItemListener(itemListen);
        this.add(accounts);

        int accountID = accountRepository.getAccountID(accounts.getSelectedItem().toString());

        ArrayList<Profile> list = profileRepository.readFromAccount(accountID);
        profiles = new JComboBox(list.toArray());
        this.add(profiles);

        show = new JButton("Haal details op");
        ButtonListener listen = new ButtonListener();
        show.addActionListener(listen);
        this.add(show);

        name = new JLabel("Naam");
        resultName = new JLabel("");
        age = new JLabel("Leeftijd");
        resultAge = new JLabel("");

        this.add(name);
        this.add(resultName);
        this.add(new JLabel());
        this.add(age);
        this.add(resultAge);
        this.add(new JLabel());

    }

    private class ButtonListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

                int accountID = accountRepository.getAccountID(accounts.getSelectedItem().toString());
                int profileID = profileRepository.getProfileID(accountID, profiles.getSelectedItem().toString());

                if (e.getSource() == show){
                    ArrayList<Profile> list = profileRepository.readProfile(profileID);
                    String text = profiles.getSelectedItem().toString();
                    for (Profile item : list){
                        if (item.getName().equals(text)){
                            resultName.setText(profileRepository.getProfileName(profileID));
                            resultAge.setText(Integer.toString(profileRepository.getProfileAge(profileID)));

                        }
                    }
                }
        }
    }
    private class ItemListen implements ItemListener {

        public void itemStateChanged(ItemEvent event) {
            if (event.getStateChange() == ItemEvent.SELECTED) {
                Object item = event.getItem();
                String selectedAccount = item.toString();
                int accountID = accountRepository.getAccountID(selectedAccount);
                profiles.removeAllItems();
                ArrayList<Profile> list = profileRepository.readFromAccount(accountID);
                for (Profile prof : list)
                    profiles.addItem(prof);

            }
        }
    }
}


