package ui.ProfileDetails;

import Data.Account;
import Data.AccountRepository;
import Data.ProfileRepository;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class CreateProfile extends JPanel{

    //panel to create a profile

    private JComboBox accounts;
    private JTextField name, age;
    private JButton save;

    private AccountRepository accountRepository = new AccountRepository();
    private ProfileRepository profileRepository = new ProfileRepository();


    public CreateProfile(){
        createComponents();
        this.setLayout(new GridLayout(0,2,20,20));

    }

    private void createComponents() {

        ArrayList<Account> text = accountRepository.readAll();
        accounts = new JComboBox(text.toArray());

        JLabel labelName = new JLabel("Naam");
        name = new JTextField("", 20);

        JLabel labelAge = new JLabel("Leeftijd");
        age = new JTextField("", 20);

        save = new JButton("Opslaan");
        Listener listen = new Listener();
        save.addActionListener(listen);

        this.add(accounts);
        this.add(new JLabel());
        this.add(labelName);
        this.add(name);
        this.add(labelAge);
        this.add(age);
        this.add(new JLabel());
        this.add(save);
    }

    private class Listener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {

            int accountID = accountRepository.getAccountID(accounts.getSelectedItem().toString());
            boolean nameExists = false;

            ArrayList<String> names = profileRepository.getAllProfileNames();
            for (String item : names) {
                if (item.equals(name.getText())) {
                    nameExists = true;
                }
            }


            if (e.getSource() == save) {
                if (profileRepository.getAmountOfProfiles(accountID) <= 4) {
                    if (!nameExists) {
                        try {
                            int number = Integer.parseInt(age.getText());
                            profileRepository.createProfile(accountID, name.getText(), number);

                            String message = "Succesvol opgeslagen!";
                            JOptionPane.showMessageDialog(null, message, "Informatie", JOptionPane.INFORMATION_MESSAGE);

                        } catch (NumberFormatException ex) {
                            String message = "Leeftijd kan alleen een getal zijn!";
                            JOptionPane.showMessageDialog(null, message, "Error Message", JOptionPane.ERROR_MESSAGE);
                        }
                    }else {
                        String message = "Naam bestaat al";
                        JOptionPane.showMessageDialog(null, message, "Error Message", JOptionPane.ERROR_MESSAGE);
                    }
                }else {
                    String message = "Maximum aantal profielen bereikt! (maximaal 5)";
                    JOptionPane.showMessageDialog(null, message, "Error Message", JOptionPane.ERROR_MESSAGE);
                }
            }


            name.setText("");
            age.setText("");


        }
    }
}
