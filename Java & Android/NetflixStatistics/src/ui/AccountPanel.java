package ui;

import ui.AccountDetails.*;

import javax.swing.*;

public class AccountPanel extends JTabbedPane {


    public AccountPanel(){
        createComponents();
    }

    public void createComponents(){

        ReadDetails readDetails = new ReadDetails();
        ChangeDetails change = new ChangeDetails();
        CreateAccount newAccount = new CreateAccount();
        DeleteAccount deleteAccount = new DeleteAccount();
        addTab("Details", readDetails);
        addTab("Wijzigen", change);
        addTab("Nieuw Account", newAccount);
        addTab("Verwijder Account", deleteAccount);


    }


}
