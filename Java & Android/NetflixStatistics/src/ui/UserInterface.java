package ui;

import java.awt.*;
import javax.swing.*;

public class UserInterface implements Runnable {

    private JFrame frame;

    //Constructor
    public UserInterface() {
        }

        //starts the frame
        @Override
        public void run() {
            frame = new JFrame("Netflix Statistix");
            frame.setPreferredSize(new Dimension(800, 600));

            //Zorgt ervoor dat het programma afsluit als je op X drukt rechtsbovenin
            frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

            createComponents(frame.getContentPane());

            frame.pack();
            frame.setVisible(true);
        }

        //creates components in the frame
        private void createComponents(Container container) {

            //Panel that contains all panels
            HomePanel home = new HomePanel(container);
            home.createComponents(frame.getContentPane());

           //Footer for every panel
            container.add(new Footer(), BorderLayout.SOUTH);

        }

        public JFrame getFrame() {
            return frame;
        }

}





