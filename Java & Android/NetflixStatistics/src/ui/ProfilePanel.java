package ui;

import ui.ProfileDetails.ChangeProfileDetails;
import ui.ProfileDetails.CreateProfile;
import ui.ProfileDetails.DeleteProfile;
import ui.ProfileDetails.ProfileDetails;

import javax.swing.*;

public class ProfilePanel extends JTabbedPane {


    public ProfilePanel() {
        createComponents();
    }

    public void createComponents(){

        ProfileDetails details = new ProfileDetails();
        ChangeProfileDetails change = new ChangeProfileDetails();
        CreateProfile create = new CreateProfile();
        DeleteProfile delete = new DeleteProfile();

        addTab("Details", details);
        addTab("Wijzigen", change);
        addTab("Nieuw Profiel", create);
        addTab("Verwijder Profiel", delete);

    }
}
