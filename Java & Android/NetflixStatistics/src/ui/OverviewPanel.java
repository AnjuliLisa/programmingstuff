package ui;

import ui.OverviewPanels.*;

import javax.swing.*;
import java.awt.*;

public class OverviewPanel extends JTabbedPane {

    //Panel that contains all the overview classes in a tabbed pane

    public OverviewPanel() {
        createComponents();
        setPreferredSize(new Dimension(300, 50));
    }

    //adds components to the panel
    private void createComponents() {

        addTab("Overzicht 1", new PercWatchedEpisode());
        addTab("Overzicht 2", new PercWatchedAccount());
        addTab("Overzicht 3", new MoviesWatchedAccount());
        addTab("Overzicht 4", new LongestMovie());
        addTab("Overzicht 5", new AccountsOneProfile());
        addTab("Overzicht 6", new ProfilesWatchedMovie());

    }
}
