package ui;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;


public class Footer extends JPanel{
    //creating a bar at the bottom of the JFrame which stays on every panel

    //Constructor
    public Footer(){
        super(new FlowLayout(FlowLayout.LEFT));
        createComponents();
        setPreferredSize(new Dimension(800, 30));
        setBorder(BorderFactory.createLineBorder(Color.black));
    }

    //method to add components to this panel
    private void createComponents() {

        add(new JLabel("Informatica -"));
        add(new JLabel("Jaar 1 -"));
        add(new JLabel("23IVT1E -"));
        add(new JLabel("Anjuli Jhakry, 2134978 -"));
        add(new JLabel("Jarne Smits, 2138640 -"));
        add(new JLabel("Stefan Ilmer, 2075582"));
    }

}
