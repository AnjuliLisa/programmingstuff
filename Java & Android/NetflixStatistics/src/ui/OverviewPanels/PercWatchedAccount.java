package ui.OverviewPanels;

import Data.Account;
import Data.AccountRepository;
import Data.Serie;
import Data.SerieRepository;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class PercWatchedAccount extends JPanel {

    //This JPanel-class gives the overview: give the average percentage watched of an Episode, filtered by chosen account and serie


    private JButton button;
    private JTextArea result;
    private JComboBox boxAccount;
    private JComboBox boxSerie;
    private AccountRepository accountRepository = new AccountRepository();
    private SerieRepository serieRepository = new SerieRepository();
    private OverviewRepository repo = new OverviewRepository();

    public PercWatchedAccount(){
        createComponents();
    }

    private void createComponents() {

        //overzicht 2: gemiddel bekeken % per aflevering van gekozen account en serie

        JLabel message = new JLabel("Overzicht van het bekeken gemiddelde percentage per aflevering en account");
        this.add(message);

        this.setLayout(new FlowLayout(FlowLayout.LEFT, 30, 20));

        ArrayList<Account> accounts = accountRepository.readAll();
        boxAccount = new JComboBox(accounts.toArray());

        ArrayList<Serie> series = serieRepository.readAll();
        boxSerie = new JComboBox(series.toArray());
        button = new JButton("Resultaat");
        Listener listen = new Listener();
        button.addActionListener(listen);

        result = new JTextArea();

        result.setEditable(false);
        Border border = BorderFactory.createLineBorder(Color.BLACK);
        result.setBorder(BorderFactory.createCompoundBorder(border,
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));

        this.add(boxAccount);
        this.add(boxSerie);
        this.add(button);
        this.add(result);
    }

    private class Listener implements ActionListener {


        @Override
        public void actionPerformed(ActionEvent e) {


            if (e.getSource() == button) {
                String serie = boxSerie.getSelectedItem().toString();
                String account = (boxAccount.getSelectedItem().toString());
                String[] splitted = account.split(" ");
                String firstName = Array.get(splitted, 0).toString();
                String lastName = Array.get(splitted, 1).toString();


                ArrayList<String> list = repo.percPerAccount(firstName, lastName, serie);
                String text = "";
                for (String item : list){
                    text += item + "\n";
                }

                result.setText(text);
            }

        }
    }
}
