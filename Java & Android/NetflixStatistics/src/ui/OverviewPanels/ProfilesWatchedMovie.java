package ui.OverviewPanels;

import Data.Movie;
import Data.MovieRepository;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

public class ProfilesWatchedMovie extends JPanel {

    //Overzicht 6: how much profiles watched the movie for 100%

    private JLabel result;
    private JComboBox boxFilms;
    private MovieRepository movieRepository = new MovieRepository();
    private OverviewRepository repository = new OverviewRepository();

    public ProfilesWatchedMovie(){
        createComponents();
    }

    //method to add components to panel
    private void createComponents() {

        this.setLayout(new FlowLayout(FlowLayout.LEFT, 30, 20));

        JLabel message = new JLabel("Hoeveel profielen de film volledig hebben afgekeken");
        this.add(message);

        ArrayList<Movie> movies = movieRepository.readAll();
        boxFilms = new JComboBox(movies.toArray());
        ItemListen listen = new ItemListen();
        boxFilms.addItemListener(listen);

        result = new JLabel(repository.haveWatchedMovie(boxFilms.getSelectedItem().toString()));

        this.add(boxFilms);
        this.add(result);
    }

    //Class to change result when something in JComboBox is selected
    private class ItemListen implements ItemListener {

        public void itemStateChanged(ItemEvent event) {
            if (event.getStateChange() == ItemEvent.SELECTED) {
                Object item = event.getItem();

                String selectedFilm = item.toString();

                result = new JLabel(repository.haveWatchedMovie(selectedFilm));


            }
        }
    }
}
