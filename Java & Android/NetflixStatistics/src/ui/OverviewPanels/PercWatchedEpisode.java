package ui.OverviewPanels;

import Data.Serie;
import Data.SerieRepository;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class PercWatchedEpisode extends JPanel {

    //Overzicht 1: amount % watched per episode

    private JButton button1;
    private JComboBox boxAccount;
    private JTextArea result;
    private SerieRepository repository = new SerieRepository();


    public PercWatchedEpisode(){

        createComponents();

    }

    private void createComponents() {

        this.setLayout(new FlowLayout(FlowLayout.LEADING, 30, 20));

        ArrayList<Serie> series = repository.readAll();
        boxAccount = new JComboBox(series.toArray());

        this.add(new JLabel("Overzicht van elke aflevering en het gemiddelde percentage van hoelang de aflevering bekeken is"));

        this.button1 = new JButton("Resultaat");
        Listener listener = new Listener();
        button1.addActionListener(listener);

        result = new JTextArea();
        result.setEditable(false);
        Border border = BorderFactory.createLineBorder(Color.BLACK);
        result.setBorder(BorderFactory.createCompoundBorder(border,
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));


        this.add(boxAccount);
        this.add(button1);
        this.add(result);


    }

    private class Listener implements ActionListener {


        @Override
        public void actionPerformed(ActionEvent e) {

            if(e.getSource() == button1){
                OverviewRepository repo = new OverviewRepository();

                ArrayList<String> list = repo.percWatchedE(boxAccount.getSelectedItem().toString());
                String text = "";
                for (String item : list) {
                    text += item + "\n";
                }
                result.setText(text);
            }

        }
    }

}
