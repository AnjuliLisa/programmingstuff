package ui.OverviewPanels;

import Data.Account;
import Data.AccountRepository;
import Data.Movie;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class MoviesWatchedAccount extends JPanel {

    //Overzicht 3: Shows the movies watched per account
    //If the textarea stays empty there is no result for this account

    private JButton button;
    private JTextArea result;
    private JComboBox accounts;
    private AccountRepository accountRepository = new AccountRepository();
    private OverviewRepository repo = new OverviewRepository();

    public MoviesWatchedAccount() {
        createComponents();
    }

    //adds components to this panel
    private void createComponents() {

        JLabel message = new JLabel("Overzicht van bekeken films per account");

        this.setLayout(new FlowLayout(FlowLayout.LEFT, 30, 20));

        ArrayList<Account> acc = accountRepository.readAll();
        accounts = new JComboBox(acc.toArray());
        button = new JButton("Resultaat");
        Listener listen = new Listener();
        button.addActionListener(listen);
        result = new JTextArea();

        result.setEditable(false);
        Border border = BorderFactory.createLineBorder(Color.BLACK);
        result.setBorder(BorderFactory.createCompoundBorder(border,
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));

        this.add(message);
        this.add(accounts);
        this.add(button);
        this.add(result);
    }

    private class Listener implements ActionListener {

        //this method executes when button is pressed
        @Override
        public void actionPerformed(ActionEvent e) {

            if (e.getSource() == button) {
                String account = (accounts.getSelectedItem().toString());
                String[] splitted = account.split(" ");
                String firstName = Array.get(splitted, 0).toString();
                String lastName = Array.get(splitted, 1).toString();

                ArrayList<String> list = repo.moviesPerAccount(firstName, lastName);
                String text = "";
                for (String item : list){
                    text += item + "\n";
                }

                result.setText(text);
            }

        }
    }
}
