package ui.OverviewPanels;

import Data.Account;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.util.ArrayList;

public class AccountsOneProfile extends JPanel {

    //Overzicht 5: Panel that shows the accounts with only one Profile

    private OverviewRepository repository = new OverviewRepository();
    private JTextArea result;

    public AccountsOneProfile() {
        createComponents();

    }

    //methdo adds component to panel
    private void createComponents() {

        this.setLayout(new FlowLayout(FlowLayout.LEFT, 30, 20));
        JLabel oneProfileAccounts = new JLabel("Accounts met 1 profiel:");

        ArrayList<Account> list = repository.getAccountOneProfile();
        String text = "";
        for (Account item : list){
            text += item.toString() + "\n";
        }
        result = new JTextArea(text);
        result.setEditable(false);
        Border border = BorderFactory.createLineBorder(Color.BLACK);
        result.setBorder(BorderFactory.createCompoundBorder(border,
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));

        this.add(oneProfileAccounts);
        this.add(result);
    }

}
