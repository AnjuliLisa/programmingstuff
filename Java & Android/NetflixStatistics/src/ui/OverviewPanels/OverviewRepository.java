package ui.OverviewPanels;

import Data.Account;
import Data.SqlConnection;

import java.sql.ResultSet;
import java.util.ArrayList;

public class OverviewRepository {

    //class that retrieved data from the database for the overviews

    private SqlConnection sqlConnection;

    public OverviewRepository(){
        this.sqlConnection = new SqlConnection();
    }

    //Method to get average percentage of time watched per episode of a given serie
    public ArrayList<String> percWatchedE(String serie){

        ArrayList<String> list = new ArrayList<>();
        try {
            sqlConnection.connectDatabase();
            ResultSet rs = sqlConnection.executeSql("SELECT a.Titel, AVG(k.Percentage_gekeken)  AS Perc_gekeken " +
                    "FROM Aflevering AS a " +
                    "JOIN Kijkgedrag AS k " +
                    "ON k.AfleveringID = a.AfleveringID " +
                    "WHERE a.Serie = '" + serie + "'" +
                    "GROUP BY a.Titel;");

            while(rs.next()){
                list.add(rs.getString("Titel") + " - " + rs.getDouble("Perc_gekeken") + " %");
            }

            sqlConnection.disconnectDatabase();

        }catch(Exception e) {
            System.out.println(e);
        }
        return list;
    }

    //Method to get average percentage of time watched per episode of a given serie and account(first name and last name)
    public ArrayList<String> percPerAccount(String firstName, String lastName, String serie){
        //returns an ArrayList with average watched per episode, per given account
        ArrayList<String> list = new ArrayList<>();
        try {
            sqlConnection.connectDatabase();

            String query = "SELECT a.Titel, AVG(k.Percentage_gekeken) AS Perc_gek " +
                    "FROM [Profile] AS p " +
                    "JOIN Kijkgedrag AS k " +
                    "ON k.ProfileID = p.ProfileID " +
                    "JOIN Aflevering AS a " +
                    "ON a.AfleveringID = k.AfleveringID " +
                    "JOIN Account AS ac " +
                    "ON ac.AccountID = p.AccountID " +
                    "WHERE a.Serie = '" + serie +
                    "' AND ac.Voornaam = '" + firstName +
                    "' AND ac.Achternaam = '" + lastName +
                    "' GROUP BY a.Titel";
            ResultSet rs = sqlConnection.executeSql(query);

            while (rs.next()) {
                //nog aanpassen
                list.add(rs.getString("Titel") + " - " +  rs.getDouble("Perc_gek") + " %");

            }

            sqlConnection.disconnectDatabase();

        }catch (Exception e){
            System.out.println(e);
        }
        return list;
    }

    //Method to get all the movies that are watched by everyone in a given account
    public ArrayList<String> moviesPerAccount(String firstName, String lastName){
        ArrayList<String> movies = new ArrayList<>();
        try {
            sqlConnection.connectDatabase();
            String query = "SELECT f.Titel " +
                    "FROM Account AS a " +
                    "JOIN [Profile] AS p " +
                    "ON p.AccountID = a.AccountID " +
                    "JOIN Kijkgedrag AS k " +
                    "ON k.ProfileID = p.ProfileID " +
                    "JOIN Film AS f " +
                    "ON f.FilmID = k.FilmID " +
                    "WHERE a.Voornaam = '" + firstName +
                    "' AND a.Achternaam = '" + lastName + "';";
            ResultSet rs = sqlConnection.executeSql(query);

            while (rs.next()) {
                movies.add(rs.getString("Titel"));

            }
            sqlConnection.disconnectDatabase();
        }catch (Exception e) {
            System.out.println(e);
        }
        return movies;
    }

    //Method to get the movie with the longest duration and has a age category of < 16
    public String longestMovie(){
        String result = null;
        try{
            sqlConnection.connectDatabase();
            String query = "SELECT TOP 1 Titel " +
                    "FROM Film " +
                    "WHERE Leeftijdscategorie NOT IN('16 jaar en ouder', '18 jaar en ouder') " +
                    "GROUP BY Tijdsduur, Titel " +
                    "ORDER BY Tijdsduur DESC ";
            ResultSet rs = sqlConnection.executeSql(query);

            while(rs.next()) {
                result = rs.getString("Titel");
            }
            sqlConnection.disconnectDatabase();
        }catch (Exception e){
            System.out.println(e);
        }

        return result;
    }

    //Method to get the accounts with only one profile
    public ArrayList<Account> getAccountOneProfile(){
        ArrayList<Account> accounts = new ArrayList<>();
        try {
            sqlConnection.connectDatabase();
            String query = "SELECT * " +
                    "FROM Account AS a " +
                    "WHERE AccountID IN (SELECT AccountID " +
                    "FROM [Profile] " +
                    "GROUP BY AccountID " +
                    "HAVING COUNT(AccountID) = 1)";
            ResultSet rs = sqlConnection.executeSql(query);

            while (rs.next()) {
                accounts.add(new Account(rs.getString("Voornaam"), rs.getString("Achternaam"), rs.getString("Straatnaam"), rs.getInt("Huisnummer"), rs.getString("Toevoeging"), rs.getString("Woonplaats"), rs.getString("Postcode")));
            }

            sqlConnection.disconnectDatabase();
        }catch (Exception e){
            System.out.println(e);

        }
        return accounts;
    }

    //Method to get the amount of profiles which have watched a selected movie
    public String haveWatchedMovie(String movie){
        String result = null;
        try {
            sqlConnection.connectDatabase();
            String query = "SELECT COUNT(*) AS Kijkers " +
                    "FROM Kijkgedrag AS k " +
                    "JOIN Film AS f " +
                    "ON f.FilmID = k.FilmID " +
                    "WHERE f.Titel = '" + movie +
                    "' AND k.Percentage_gekeken = '100.00';";
            ResultSet rs = sqlConnection.executeSql(query);
            while (rs.next()){

                result = Integer.toString(rs.getInt("Kijkers"));
            }
            sqlConnection.disconnectDatabase();
        }catch (Exception e){
            System.out.println(e);
        }
        return result;
    }


}
