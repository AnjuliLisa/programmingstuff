package ui.OverviewPanels;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

public class LongestMovie extends JPanel {

    //Overzicht 4: Panel that shows the movie with the longest playtime with maximum age of 16

    private OverviewRepository repository = new OverviewRepository();
    private JTextArea result;

    public LongestMovie(){
        createComponents();

    }

    //Method adds components to this panel
    private void createComponents() {

        this.setLayout(new FlowLayout(FlowLayout.LEFT, 30, 20));

        JLabel announcement = new JLabel("De film met de langste speelduur voor leeftijdsklasse <16 is:");
        result = new JTextArea(repository.longestMovie());

        result.setEditable(false);
        Border border = BorderFactory.createLineBorder(Color.BLACK);
        result.setBorder(BorderFactory.createCompoundBorder(border,
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));

        this.add(announcement);
        this.add(result);
    }
}
