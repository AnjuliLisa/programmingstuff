package ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class HomePanel extends JPanel {

    private JPanel cards;
    private JButton account;
    private JButton views;
    private JButton profile;
    private JButton watchedShows;

    public HomePanel(Container pane){
        super(new GridBagLayout());
        createComponents(pane);
    }

    //method for components in this panel
    protected void createComponents(Container pane){

        //Panel for buttons: Accounts, Overzichten en Nieuw account aanmaken
        JPanel menu = new JPanel(new FlowLayout(FlowLayout.CENTER));

        //Listener for buttons
        ButtonListener listener = new ButtonListener();

        //Buttons Menu panel
        account = new JButton("Accounts");
        account.addActionListener(listener);

        views = new JButton("Overzichten");
        views.addActionListener(listener);

        profile = new JButton("Profielen");
        profile.addActionListener(listener);

        watchedShows = new JButton("Kijkgedrag");
        watchedShows.addActionListener(listener);

        menu.add(account);
        menu.add(profile);
        menu.add(watchedShows);
        menu.add(views);


        //Panels linked to buttons (CardLayout)
        OverviewPanel view = new OverviewPanel();
        AccountPanel accounts = new AccountPanel();
        ProfilePanel profiles = new ProfilePanel();
        WatchedPanel watched = new WatchedPanel();

        //add panels to cards panel with CardLayout
        cards = new JPanel(new CardLayout());

        cards.add(accounts, "Card 1");
        cards.add(profiles, "Card 2");
        cards.add(watched, "Card 3");
        cards.add(view, "Card 4");

        pane.add(menu, BorderLayout.PAGE_START);
        pane.add(cards, BorderLayout.CENTER);

    }

    //Inner class that listens to buttons
    private class ButtonListener implements ActionListener{

        //method starts when button is pressed
        @Override
        public void actionPerformed(ActionEvent e) {

            if(e.getSource() == account){

                CardLayout cl = (CardLayout) (cards.getLayout());
                cl.show(cards, "Card 1");

            } else if (e.getSource() == profile) {

                CardLayout cl = (CardLayout) (cards.getLayout());
                cl.show(cards, "Card 2");
            }else if (e.getSource() == watchedShows) {

                CardLayout cl = (CardLayout) (cards.getLayout());
                cl.show(cards, "Card 3");
            }else if (e.getSource() == views) {

                CardLayout cl = (CardLayout) (cards.getLayout());
                cl.show(cards, "Card 4");
            }

        }
    }



}
