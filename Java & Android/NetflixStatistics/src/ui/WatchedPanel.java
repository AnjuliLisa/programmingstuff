package ui;

import ui.WatchedDetails.AddWatchedShows;
import ui.WatchedDetails.DeleteWatchedShows;
import ui.WatchedDetails.ReadWatchedShows;
import ui.WatchedDetails.UpdateWatchedShows;

import javax.swing.*;

public class WatchedPanel extends JTabbedPane {


    public WatchedPanel(){
        createComponents();

    }

    public void createComponents(){

        ReadWatchedShows read = new ReadWatchedShows();
        UpdateWatchedShows update = new UpdateWatchedShows();
        AddWatchedShows add = new AddWatchedShows();
        DeleteWatchedShows delete = new DeleteWatchedShows();

        addTab("Details", read);
        addTab("Wijzigen", update);
        addTab("Nieuw Kijkgedrag", add);
        addTab("Verwijder film/aflevering", delete);
    }


}
