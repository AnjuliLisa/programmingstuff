package ui.WatchedDetails;

import Data.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import static javax.swing.JOptionPane.ERROR_MESSAGE;

public class DeleteWatchedShows extends JPanel {

    //This class deletes watched shows to the database, connected to an account and profile

    private AccountRepository accountRepository = new AccountRepository();
    private ProfileRepository profileRepository = new ProfileRepository();
    private WatchedItemsRepository itemsRepository = new WatchedItemsRepository();
    private SerieRepository serieRepository = new SerieRepository();
    private MovieRepository movieRepository = new MovieRepository();


    private JComboBox accounts, profiles, boxSerie, boxEpisodes, films;
    private JButton deleteSerie, deleteFilm, delSerie, delFilm;
    private JPanel cards, dFilm, dSerie;
    private int accountID, profileID, filmID, episodeID, itemID;


    public DeleteWatchedShows() {
        createComponents();
        episodeID = 0;
        itemID = 0;
        filmID = 0;
    }

    public void createComponents() {

        Listener listen = new Listener();

        JPanel menu = new JPanel(new FlowLayout(FlowLayout.CENTER));
        cards = new JPanel(new CardLayout());

        deleteSerie = new JButton("Verwijder serie");
        deleteSerie.addActionListener(listen);
        menu.add(deleteSerie);

        deleteFilm = new JButton("Verwijder film");
        deleteFilm.addActionListener(listen);
        menu.add(deleteFilm);

        ArrayList<Account> listAccounts = accountRepository.readAll();
        accounts = new JComboBox(listAccounts.toArray());
        accountID = accountRepository.getAccountID(accounts.getSelectedItem().toString());
        AccountItemListener itemListen = new AccountItemListener();
        accounts.addItemListener(itemListen);
        this.add(accounts);

        ArrayList<Profile> listProfiles = profileRepository.readFromAccount(accountID);
        profiles = new JComboBox(listProfiles.toArray());
        ProfileItemListener profileListener = new ProfileItemListener();
        profiles.addItemListener(profileListener);
        profileID = profileRepository.getProfileID(accountID, profiles.getSelectedItem().toString());
        this.add(profiles);

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////

        dSerie = new JPanel();
        JLabel message = new JLabel("Kies een Serie en Aflevering: ");

        ArrayList<String> series = itemsRepository.readWatchedSeries(profileID);
        boxSerie = new JComboBox(series.toArray());
        SerieItemListener boxListener = new SerieItemListener();
        boxSerie.addItemListener(boxListener);

        ArrayList<String> episodes = itemsRepository.readWatchedEpisodes(profileID);
        boxEpisodes = new JComboBox(episodes.toArray());
        delSerie = new JButton("Verwijder");
        delSerie.addActionListener(listen);

        //Add components to delete Serie panel
        dSerie.add(message);
        dSerie.add(boxSerie);
        dSerie.add(boxEpisodes);
        dSerie.add(delSerie);

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////

        dFilm = new JPanel();
        JLabel textMessage = new JLabel("Kies een film:  ");
        ArrayList<String> movies = itemsRepository.readWatchedMovies(profileID);
        films = new JComboBox(movies.toArray());
        delFilm = new JButton("Verwijder");
        delFilm.addActionListener(listen);

        //Add components to delete film panel
        dFilm.add(textMessage);
        dFilm.add(films);
        dFilm.add(delFilm);

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////
        this.add(profiles);

        //Add Panels to Card panel with CardLayout
        cards.add(new JPanel(), "Card 3"); //This makes sure the page is clear at start
        cards.add(dFilm, "Card 1");
        cards.add(dSerie, "Card 2");

        this.add(menu, BorderLayout.PAGE_START);
        this.add(cards, BorderLayout.SOUTH);
    }

    private class Listener implements ActionListener {


        @Override
        public void actionPerformed(ActionEvent e) {

            if(e.getSource() == deleteFilm){

                CardLayout cl = (CardLayout) (cards.getLayout());
                cl.show(cards, "Card 1");
                accountID = accountRepository.getAccountID(accounts.getSelectedItem().toString());
                profileID = profileRepository.getProfileID(accountID, profiles.getSelectedItem().toString());

            } else if (e.getSource() == deleteSerie) {

                CardLayout cl = (CardLayout) (cards.getLayout());
                cl.show(cards, "Card 2");
                accountID = accountRepository.getAccountID(accounts.getSelectedItem().toString());
                profileID = profileRepository.getProfileID(accountID, profiles.getSelectedItem().toString());

            } else if (e.getSource() == delSerie){
                try{
                    episodeID = serieRepository.EpisodeID(boxEpisodes.getSelectedItem().toString());
                    String serie = boxSerie.getSelectedItem().toString();
                    itemID = itemsRepository.getIDSerie(accountID, profileID, episodeID, serie);
                    itemsRepository.deleteWatchedItem(itemID);
                    String message = "Serie succesvol Verwijderd!";
                    JOptionPane.showMessageDialog(null, message,"Informatie", JOptionPane.INFORMATION_MESSAGE);
                }catch (Exception ex) {
                    String message = "Er is iets fout gegaan...";
                    JOptionPane.showMessageDialog(null, message,"Error Message", ERROR_MESSAGE);
                }


            } else if (e.getSource() == delFilm){
                try{
                    filmID = movieRepository.getMovieID(films.getSelectedItem().toString());
                    itemID = itemsRepository.getIDFilm(accountID, profileID, filmID);
                    itemsRepository.deleteWatchedItem(itemID);
                    String message = "Film Succesvol Verwijderd!";
                    JOptionPane.showMessageDialog(null, message,"Informatie", JOptionPane.INFORMATION_MESSAGE);
                }catch (Exception ex) {
                    String message = "Er is iets fout gegaan...";
                    JOptionPane.showMessageDialog(null, message,"Error Message", ERROR_MESSAGE);
                }
            }

        }
    }

    private class AccountItemListener implements ItemListener {

        //Changes comboBox profiles to the account which is selected

        public void itemStateChanged(ItemEvent event) {
            if (event.getStateChange() == ItemEvent.SELECTED) {
                Object item = event.getItem();
                String selectedAccount = item.toString();
                accountID = accountRepository.getAccountID(selectedAccount);

                profiles.removeAllItems();
                ArrayList<Profile> list = profileRepository.readFromAccount(accountID);
                for (Profile prof : list)
                    profiles.addItem(prof);

            }
        }
    }

    private class ProfileItemListener implements ItemListener {

        //Changes the comboboxen of the series, episodes and films connected to the selected profile

        public void itemStateChanged(ItemEvent event) {
            if (event.getStateChange() == ItemEvent.SELECTED) {
                Object item = event.getItem();
                String selectedProfile = item.toString();
                profileID = profileRepository.getProfileID(accountID, selectedProfile);

                boxSerie.removeAllItems();
                ArrayList<String> series = itemsRepository.readWatchedSeries(profileID);
                for (String s : series) {
                    boxSerie.addItem(s);
                }

                boxEpisodes.removeAllItems();
                ArrayList<String> episode = itemsRepository.readWatchedEpisodes(profileID);
                for (String e : episode){
                    boxEpisodes.addItem(e);
                }

                films.removeAllItems();
                ArrayList<String> movies = itemsRepository.readWatchedMovies(profileID);
                for (String m : movies){
                    films.addItem(m);
                }

            }
        }
    }

    private class SerieItemListener implements ItemListener {

        //Changes the comboBox with episodes connected to the selected Serie

        @Override
        public void itemStateChanged(ItemEvent event) {

            if (event.getStateChange() == ItemEvent.SELECTED) {
                Object item = event.getItem();
                String selectedSerie = item.toString();

                boxEpisodes.removeAllItems();
                ArrayList<String> list = itemsRepository.readWatchedEpisodes(profileID);
                for (String episode : list)
                    boxEpisodes.addItem(episode);

            }

        }
    }
}
