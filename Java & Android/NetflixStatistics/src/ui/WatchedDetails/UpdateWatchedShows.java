package ui.WatchedDetails;

import Data.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import static javax.swing.JOptionPane.ERROR_MESSAGE;

public class UpdateWatchedShows extends JPanel {

    //This updates watched shows to the database, connected to an account and profile

    private AccountRepository accountRepository = new AccountRepository();
    private ProfileRepository profileRepository = new ProfileRepository();
    private WatchedItemsRepository itemsRepository = new WatchedItemsRepository();
    private SerieRepository serieRepository = new SerieRepository();
    private MovieRepository movieRepository = new MovieRepository();

    private JComboBox accounts, profiles, boxSerie, boxEpisodes, films;
    private JButton updateSerie, updateFilm, saveSerie, saveFilm;
    private JPanel cards, upSerie, upFilm;
    private int accountID, profileID, episodeID, itemID, filmID;
    private JTextField amountSerieWatched,amountFilmWatched;

    public UpdateWatchedShows() {
        createComponents();
        episodeID = 0;
        itemID = 0;
        filmID = 0;
    }

    public void createComponents() {

        //Panel for ComboBox accounts and profiles, buttons updateFilm and updateSerie
        JPanel menu = new JPanel(new FlowLayout(FlowLayout.CENTER));
        //Panel waar de Films/Serie toevoegen panels inkomen
        cards = new JPanel(new CardLayout());

        //Account kiezen Combobox
        ArrayList<Account> listAccounts = accountRepository.readAll();
        accounts = new JComboBox(listAccounts.toArray());
        accountID = accountRepository.getAccountID(accounts.getSelectedItem().toString());
        AccountItemListener itemListen = new AccountItemListener();
        accounts.addItemListener(itemListen);
        this.add(accounts);

        ArrayList<Profile> listProfiles = profileRepository.readFromAccount(accountID);
        profiles = new JComboBox(listProfiles.toArray());
        ProfileItemListener profileListener = new ProfileItemListener();
        profiles.addItemListener(profileListener);
        profileID = profileRepository.getProfileID(accountID, profiles.getSelectedItem().toString());
        this.add(profiles);

        Listener listen = new Listener();

        updateSerie = new JButton("Wijzig serie");
        updateSerie.addActionListener(listen);
        menu.add(updateSerie);

        updateFilm = new JButton("Wijzig film");
        updateFilm.addActionListener(listen);
        menu.add(updateFilm);

        ////////////////////////////////////////////////////////////////////////////////////////////////////////

        //Panel for updating serie
        upSerie = new JPanel();
        JLabel message = new JLabel("Update percentage gekeken: ");

        ArrayList<String> series = itemsRepository.readWatchedSeries(profileID);
        boxSerie = new JComboBox(series.toArray());
        SerieItemListener boxListener = new SerieItemListener();
        boxSerie.addItemListener(boxListener);

        ArrayList<String> episodes = itemsRepository.readWatchedEpisodes(profileID);
        boxEpisodes = new JComboBox(episodes.toArray());
        amountSerieWatched = new JTextField(10);
        JLabel perc = new JLabel("%");
        saveSerie = new JButton("Opslaan");
        saveSerie.addActionListener(listen);

        //Components toevoegen aan Toevoegen Serie Panel
        upSerie.add(message);
        upSerie.add(boxSerie);
        upSerie.add(boxEpisodes);
        upSerie.add(amountSerieWatched);
        upSerie.add(perc);
        upSerie.add(saveSerie);

        ////////////////////////////////////////////////////////////////////////////////////////////////////////

        //Panel update Film
        upFilm = new JPanel();
        JLabel textMessage = new JLabel("Update percentage gekeken: ");
        ArrayList<String> movies = itemsRepository.readWatchedMovies(profileID);
        films = new JComboBox(movies.toArray());
        amountFilmWatched = new JTextField(10);
        JLabel proc = new JLabel("%");
        saveFilm = new JButton("Opslaan");
        saveFilm.addActionListener(listen);

        //toevoegen components aan Film toevoegen Panel
        upFilm.add(textMessage);
        upFilm.add(films);
        upFilm.add(amountFilmWatched);
        upFilm.add(proc);
        upFilm.add(saveFilm);

        ////////////////////////////////////////////////////////////////////////////////////////////////////////


        //Add Panels to Card panel with CardLayout
        cards.add(new JPanel(), "Card 3"); //This makes sure the page is clear at start
        cards.add(upFilm, "Card 1");
        cards.add(upSerie, "Card 2");

        this.add(menu, BorderLayout.PAGE_START);
        this.add(cards, BorderLayout.SOUTH);

    }

    private class Listener implements ActionListener{



        @Override
        public void actionPerformed(ActionEvent e) {

            if (e.getSource() == updateFilm) {

                CardLayout cl = (CardLayout) (cards.getLayout());
                cl.show(cards, "Card 1");
                accountID = accountRepository.getAccountID(accounts.getSelectedItem().toString());
                profileID = profileRepository.getProfileID(accountID, profiles.getSelectedItem().toString());
            } else if (e.getSource() == updateSerie) {

                CardLayout cl = (CardLayout) (cards.getLayout());
                cl.show(cards, "Card 2");
                accountID = accountRepository.getAccountID(accounts.getSelectedItem().toString());
                profileID = profileRepository.getProfileID(accountID, profiles.getSelectedItem().toString());
            } else if (e.getSource() == saveSerie) {
                try {
                    episodeID = serieRepository.EpisodeID(boxEpisodes.getSelectedItem().toString());
                    String serie = boxSerie.getSelectedItem().toString();
                    itemID = itemsRepository.getIDSerie(accountID, profileID, episodeID, serie);
                    double perc = Double.parseDouble(amountSerieWatched.getText());
                    itemsRepository.updateWatchedItem(itemID, perc);

                    String message = "Succesvol Opgeslagen!";
                    JOptionPane.showMessageDialog(null, message,"Informatie", JOptionPane.INFORMATION_MESSAGE);

                } catch (NumberFormatException ex) {
                    String message = "Vul een getal in! \n Bijvoorbeeld: 75.0";
                    JOptionPane.showMessageDialog(null, message, "Error Message", ERROR_MESSAGE);

                }
            } else if (e.getSource() == saveFilm){
                try{
                    filmID = movieRepository.getMovieID(films.getSelectedItem().toString());
                    itemID = itemsRepository.getIDFilm(accountID, profileID, filmID);
                    double perc = Double.parseDouble(amountFilmWatched.getText());
                    itemsRepository.updateWatchedItem(itemID, perc);

                    String message = "Succesvol Opgeslagen!";
                    JOptionPane.showMessageDialog(null, message,"Informatie", JOptionPane.INFORMATION_MESSAGE);

                }catch (NumberFormatException ex) {
                    String message = "Vul een getal in! \n Bijvoorbeeld: 75.0";
                    JOptionPane.showMessageDialog(null, message,"Error Message", ERROR_MESSAGE);
                }

            }

        }
    }

    private class AccountItemListener implements ItemListener{

        //Changes comboBox profiles to the account which is selected

        public void itemStateChanged(ItemEvent event) {
            if (event.getStateChange() == ItemEvent.SELECTED) {
                Object item = event.getItem();
                String selectedAccount = item.toString();
                accountID = accountRepository.getAccountID(selectedAccount);

                profiles.removeAllItems();
                ArrayList<Profile> list = profileRepository.readFromAccount(accountID);
                for (Profile prof : list)
                profiles.addItem(prof);

            }
        }
    }

    private class ProfileItemListener implements ItemListener {

        //Changes the comboboxes of the series, episodes and films connected to the selected profile

        public void itemStateChanged(ItemEvent event) {
            if (event.getStateChange() == ItemEvent.SELECTED) {
                Object item = event.getItem();
                String selectedProfile = item.toString();
                profileID = profileRepository.getProfileID(accountID, selectedProfile);

                boxSerie.removeAllItems();
                ArrayList<String> series = itemsRepository.readWatchedSeries(profileID);
                for (String s : series) {
                    boxSerie.addItem(s);
                }

                boxEpisodes.removeAllItems();
                ArrayList<String> episode = itemsRepository.readWatchedEpisodes(profileID);
                for (String e : episode){
                    boxEpisodes.addItem(e);
                }

                films.removeAllItems();
                ArrayList<String> movies = itemsRepository.readWatchedMovies(profileID);
                for (String m : movies){
                    films.addItem(m);
                }

            }
        }
    }

    private class SerieItemListener implements ItemListener {

        //Changes the comboBox with episodes connected to the selected Serie

        @Override
        public void itemStateChanged(ItemEvent event) {

            if (event.getStateChange() == ItemEvent.SELECTED) {
                Object item = event.getItem();
                String selectedSerie = item.toString();

                boxEpisodes.removeAllItems();
                ArrayList<String> list = itemsRepository.readWatchedEpisodes(profileID);
                for (String episode : list)
                    boxEpisodes.addItem(episode);

            }

        }
    }

}

