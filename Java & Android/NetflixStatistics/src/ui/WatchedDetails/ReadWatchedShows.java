package ui.WatchedDetails;

import Data.*;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

public class ReadWatchedShows extends JPanel {

    //This class reads watched shows to the database, connected to an account and profile

    private AccountRepository accountRepository = new AccountRepository();
    private ProfileRepository profileRepository = new ProfileRepository();
    WatchedItemsRepository watchedRepository = new WatchedItemsRepository();
    private JComboBox accounts, profiles;
    private JTextArea result;
    private JButton showResult;
    private int profileID;

    public ReadWatchedShows() {
        createComponents();
    }

    public void createComponents(){

        ArrayList<Account> listAccounts = accountRepository.readAll();
        accounts = new JComboBox(listAccounts.toArray());
        int accountID = accountRepository.getAccountID(accounts.getSelectedItem().toString());
        ItemListen itemListen = new ItemListen();
        accounts.addItemListener(itemListen);

        ArrayList<Profile> listProfiles = profileRepository.readFromAccount(accountID);
        profiles = new JComboBox(listProfiles.toArray());


        showResult = new JButton("Haal resultaat op");
        Listener listen = new Listener();
        showResult.addActionListener(listen);

        result = new JTextArea();
        result.setEditable(false);
        Border border = BorderFactory.createLineBorder(Color.BLACK);
        result.setBorder(BorderFactory.createCompoundBorder(border,
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));

        this.add(accounts);
        this.add(profiles);
        this.add(showResult);
        this.add(result);
    }

    private class Listener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == showResult){
                int accountID = accountRepository.getAccountID(accounts.getSelectedItem().toString());
                profileID = profileRepository.getProfileID(accountID, profiles.getSelectedItem().toString());
                String rs = "";
                ArrayList<String> movies = watchedRepository.readWatchedMovies(profileID);
                ArrayList<String> episodes = watchedRepository.readWatchedEpisodes(profileID);
                for (String text : movies){
                    rs += text;
                    rs += "\n";
                }
                for (String text : episodes){
                    rs += text;
                    rs += "\n";
                }
                result.setText(rs);
            }
        }
    }

    private class ItemListen implements ItemListener {

        //Changes the profile combobox with the profiles connected to the chosen account

        public void itemStateChanged(ItemEvent event) {
            if (event.getStateChange() == ItemEvent.SELECTED) {
                Object item = event.getItem();
                String selectedAccount = item.toString();
                int accountID = accountRepository.getAccountID(selectedAccount);
                profiles.removeAllItems();
                ArrayList<Profile> list = profileRepository.readFromAccount(accountID);
                for (Profile prof : list)
                    profiles.addItem(prof);

            }
        }
    }

}
