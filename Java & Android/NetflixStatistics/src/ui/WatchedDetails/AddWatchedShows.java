package ui.WatchedDetails;

import Data.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import static javax.swing.JOptionPane.ERROR_MESSAGE;

public class AddWatchedShows extends JPanel {

    //This class add watched shows to the database, connected to an account and profile

    private AccountRepository accountRepository = new AccountRepository();
    private ProfileRepository profileRepository = new ProfileRepository();
    private WatchedItemsRepository itemsRepository = new WatchedItemsRepository();
    private SerieRepository serieRepository = new SerieRepository();
    private MovieRepository movieRepository = new MovieRepository();

    private JComboBox accounts, profiles, boxSerie, boxEpisodes, films;
    private JButton addSerie, addFilm, saveSerie, saveFilm;
    private JPanel cards, aSerie, aFilm;
    private int accountID, profileID;
    private JTextField amountSerieWatched,amountFilmWatched;


    public AddWatchedShows(){
        createComponents();
    }

    public void createComponents(){

        //Panel for cardlayout-buttons and combobox for account & profiles
        JPanel menu = new JPanel(new FlowLayout(FlowLayout.CENTER));
        cards = new JPanel(new CardLayout());

        //Create Button Listener
        ButtonActionListener listener = new ButtonActionListener();

        //Choose Account ComboBox
        ArrayList<Account> listAccounts = accountRepository.readAll();
        accounts = new JComboBox(listAccounts.toArray());
        accountID = accountRepository.getAccountID(accounts.getSelectedItem().toString());
        AccountItemListener itemListen = new AccountItemListener();
        accounts.addItemListener(itemListen);
        this.add(accounts);

        //Choose profile ComboBox
        ArrayList<Profile> listProfiles = profileRepository.readFromAccount(accountID);
        profiles = new JComboBox(listProfiles.toArray());
        this.add(profiles);

        //Button opens Add Serie-Episode Panel
        addSerie = new JButton("Voeg serie toe");
        addSerie.addActionListener(listener);
        menu.add(addSerie);

        //Button opens Add Film Panel
        addFilm = new JButton("Voeg film toe");
        addFilm.addActionListener(listener);
        menu.add(addFilm);

        /////////////////////////////////////////////////////////////////////////////////////////////////////////

        aFilm = new JPanel();
        JLabel textMessage = new JLabel("Kies de gekeken film: ");
        ArrayList<Movie> movies = movieRepository.readAll();
        films = new JComboBox(movies.toArray());
        amountFilmWatched = new JTextField(10);
        JLabel proc = new JLabel("%");
        saveFilm = new JButton("Opslaan");
        saveFilm.addActionListener(listener);

        //Add components to Add Film Panel
        aFilm.add(textMessage);
        aFilm.add(films);
        aFilm.add(amountFilmWatched);
        aFilm.add(proc);
        aFilm.add(saveFilm);

        ////////////////////////////////////////////////////////////////////////////////////////////////////////

        aSerie = new JPanel();
        JLabel message = new JLabel("Kies een serie en aflevering:");
        ArrayList<Serie> series = serieRepository.readAll();
        boxSerie = new JComboBox(series.toArray());
        SerieItemListener boxListener = new SerieItemListener();
        boxSerie.addItemListener(boxListener);


        ArrayList<Episode> episodes = serieRepository.readEpisodes(boxSerie.getSelectedItem().toString());
        boxEpisodes = new JComboBox(episodes.toArray());
        amountSerieWatched = new JTextField(10);
        JLabel perc = new JLabel("%");
        saveSerie = new JButton("Opslaan");
        saveSerie.addActionListener(listener);

        aSerie.add(message);
        aSerie.add(boxSerie);
        aSerie.add(boxEpisodes);
        aSerie.add(amountSerieWatched);
        aSerie.add(perc);
        aSerie.add(saveSerie);

        /////////////////////////////////////////////////////////////////////////////////////////////////////////

        //Panels toevoegen aan Card panel met Card layout
        cards.add(new JPanel(), "Card 3");
        cards.add(aFilm, "Card 1");
        cards.add(aSerie, "Card 2");

        this.add(menu, BorderLayout.PAGE_START);
        this.add(cards, BorderLayout.SOUTH);

    }

    private class ButtonActionListener implements ActionListener{


        @Override
        public void actionPerformed(ActionEvent e) {

            if(e.getSource() == addFilm){

                CardLayout cl = (CardLayout) (cards.getLayout());
                cl.show(cards, "Card 1");
                accountID = accountRepository.getAccountID(accounts.getSelectedItem().toString());
                profileID = profileRepository.getProfileID(accountID, profiles.getSelectedItem().toString());

            } else if (e.getSource() == addSerie) {

                CardLayout cl = (CardLayout) (cards.getLayout());
                cl.show(cards, "Card 2");
                accountID = accountRepository.getAccountID(accounts.getSelectedItem().toString());
                profileID = profileRepository.getProfileID(accountID, profiles.getSelectedItem().toString());

            }else if (e.getSource() == saveFilm){
                try{
                    double perc = Double.parseDouble(amountFilmWatched.getText());
                    itemsRepository.addFilm(profileID, films.getSelectedItem().toString(), perc);

                    String message = "Succesvol Opgeslagen!";
                    JOptionPane.showMessageDialog(null, message,"Informatie", JOptionPane.INFORMATION_MESSAGE);
                }catch (NumberFormatException ex) {
                    String message = "Vul een getal in! \n Bijvoorbeeld: 75.0";
                    JOptionPane.showMessageDialog(null, message,"Error Message", ERROR_MESSAGE);
                }

            }else if (e.getSource() == saveSerie){
                try{
                    double perc = Double.parseDouble(amountSerieWatched.getText());
                    itemsRepository.addSerie(profileID, boxSerie.getSelectedItem().toString(), boxEpisodes.getSelectedItem().toString(), perc);

                    String message = "Succesvol Opgeslagen!";
                    JOptionPane.showMessageDialog(null, message,"Informatie", JOptionPane.INFORMATION_MESSAGE);
                }catch (NumberFormatException ex) {
                String message = "Vul een getal in! \n Bijvoorbeeld: 75.0";
                JOptionPane.showMessageDialog(null, message,"Error Message", ERROR_MESSAGE);
            }


        }
        }
    }

    private class AccountItemListener implements ItemListener {

        //Changes comboBox profiles to the account which is selected


        public void itemStateChanged(ItemEvent event) {
            if (event.getStateChange() == ItemEvent.SELECTED) {
                Object item = event.getItem();
                String selectedAccount = item.toString();
                accountID = accountRepository.getAccountID(selectedAccount);

                profiles.removeAllItems();
                ArrayList<Profile> list = profileRepository.readFromAccount(accountID);
                for (Profile prof : list)
                    profiles.addItem(prof);

            }
        }
    }
    private class SerieItemListener implements ItemListener {

        //Changes the comboBox with episodes connected to the selected Serie


        @Override
        public void itemStateChanged(ItemEvent event) {

            if (event.getStateChange() == ItemEvent.SELECTED) {
                Object item = event.getItem();
                String selectedSerie = item.toString();

                boxEpisodes.removeAllItems();
                ArrayList<Episode> episodes = serieRepository.readEpisodes(selectedSerie);
                for (Episode e : episodes)
                    boxEpisodes.addItem(e);

            }

        }
    }
}
