package ui.AccountDetails;

import Data.Account;
import Data.AccountRepository;
import org.omg.IOP.TAG_JAVA_CODEBASE;

import javax.swing.*;
import javax.swing.plaf.basic.BasicOptionPaneUI;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.channels.FileLock;

public class CreateAccount extends JPanel {

    //Adds a new account to the database

    private JTextField firstName;
    private JTextField lastName;
    private JTextField streetName;
    private JTextField number;
    private JTextField numberSuffix;
    private JTextField city;
    private JTextField postalCode;
    private JButton save;
    private AccountRepository repository;

    public CreateAccount(){
        this.setLayout(new GridLayout(0,2,20,20));
        createComponents();

    }

    private void createComponents(){

        //Voornaam label en tekstbalk
        JLabel labelFirstName = new JLabel("Voornaam");
        labelFirstName.setHorizontalAlignment(JLabel.CENTER);
        firstName = new JTextField(20);

        this.add(labelFirstName);
        this.add(firstName);

        //Achternaam label en tekstbalk
        JLabel labelLastName = new JLabel("Achternaam");
        labelLastName.setHorizontalAlignment(JLabel.CENTER);
        lastName = new JTextField(20);

        this.add(labelLastName);
        this.add(lastName);

        //Straatnaam label en textbalk
        JLabel labelStreet = new JLabel("Straat");
        labelStreet.setHorizontalAlignment(JLabel.CENTER);
        streetName = new JTextField(20);

        this.add(labelStreet);
        this.add(streetName);

        //Huisnummer label en tekstbalk
        JLabel labelNumber = new JLabel("Huisnummer");
        labelNumber.setHorizontalAlignment(JLabel.CENTER);
        number = new JTextField(20);

        this.add(labelNumber);
        this.add(number);

        //Toevoeging huisnummer label en tekstbalk
        JLabel labelSuffix = new JLabel("Toevoeging");
        labelSuffix.setHorizontalAlignment(JLabel.CENTER);
        numberSuffix = new JTextField(20);

        this.add(labelSuffix);
        this.add(numberSuffix);

        //Woonplaats Label en tekstbalk
        JLabel labelCity = new JLabel("Woonplaats");
        labelCity.setHorizontalAlignment(JLabel.CENTER);
        city = new JTextField(20);

        this.add(labelCity);
        this.add(city);

        //Postcode label en tekstbalk
        JLabel labelPostalCode = new JLabel("Postcode");
        labelPostalCode.setHorizontalAlignment(JLabel.CENTER);
        postalCode = new JTextField(20);

        this.add(labelPostalCode);
        this.add(postalCode);

        //Om de grid linksonderin te vullen
        this.add(new JLabel());

        //Button om op te slaan
        save = new JButton("Opslaan");
        ButtonActionListener listener = new ButtonActionListener();
        save.addActionListener(listener);
        this.add(save);
    }

    private class ButtonActionListener implements ActionListener{


        @Override
        public void actionPerformed(ActionEvent e) {


            if (e.getSource() == save){
                //number moet omgezet worden want in database is het een integer
                try {
                    int num = Integer.parseInt(number.getText());
                    //Account object aanmaken met gegeven data (JTextfields)
                    Account newAccount = new Account(firstName.getText(), lastName.getText(), streetName.getText(), num, numberSuffix.getText(), city.getText(), postalCode.getText());

                    //Account toevoegen aan database
                    repository = new AccountRepository();
                    repository.create(newAccount);

                    String message = "Succesvol Opgeslagen!";
                    JOptionPane.showMessageDialog(null, message,"Informatie", JOptionPane.INFORMATION_MESSAGE);
                }catch (NumberFormatException ex) {
                    String message = "Huisnummer kan alleen een getal zijn!";
                    JOptionPane.showMessageDialog(null, message,"Error Message", JOptionPane.ERROR_MESSAGE);
                }

            }                //Maakt elke JTextfield weer leeg nadat
                firstName.setText("");
                lastName.setText("");
                streetName.setText("");
                number.setText("");
                numberSuffix.setText("");
                city.setText("");
                postalCode.setText("");

            }

    }
}

