package ui.AccountDetails;

import Data.Account;
import Data.AccountRepository;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class ReadDetails extends JPanel {

    //shows the details from a selected account

    private JComboBox account;
    private JLabel firstName;
    private JLabel lastName;
    private JLabel streetName;
    private JLabel number;
    private JLabel numberSuffix;
    private JLabel city;
    private JLabel postalCode;
    private JButton showDetails;
    private AccountRepository accountRepository = new AccountRepository();


    public ReadDetails() {
        createComponents();
        this.setLayout(new GridLayout(0, 2, 20, 20));
    }

    private void createComponents() {

        //Account selecteren, method getAccountRepository
        ArrayList<Account> list = accountRepository.readAll();
        account = new JComboBox(list.toArray());

        this.add(account);

        showDetails = new JButton("Haal details op");
        ButtonActionListener listen = new ButtonActionListener();
        showDetails.addActionListener(listen);
        this.add(showDetails);


        //Voornaam label en tekstbalk
        JLabel labelFirstName = new JLabel("Voornaam");
        labelFirstName.setHorizontalAlignment(JLabel.CENTER);
        firstName = new JLabel("");

        this.add(labelFirstName);
        this.add(firstName);

        //Achternaam label en tekstbalk
        JLabel labelLastName = new JLabel("Achternaam");
        labelLastName.setHorizontalAlignment(JLabel.CENTER);
        lastName = new JLabel("");

        this.add(labelLastName);
        this.add(lastName);

        //Straatnaam label en textbalk
        JLabel labelStreet = new JLabel("Straat");
        labelStreet.setHorizontalAlignment(JLabel.CENTER);
        streetName = new JLabel("");

        this.add(labelStreet);
        this.add(streetName);

        //Huisnummer label en tekstbalk
        JLabel labelNumber = new JLabel("Huisnummer");
        labelNumber.setHorizontalAlignment(JLabel.CENTER);
        number = new JLabel("");

        this.add(labelNumber);
        this.add(number);

        //Toevoeging huisnummer label en tekstbalk
        JLabel labelSuffix = new JLabel("Toevoeging");
        labelSuffix.setHorizontalAlignment(JLabel.CENTER);
        numberSuffix = new JLabel("");

        this.add(labelSuffix);
        this.add(numberSuffix);

        //Woonplaats Label en tekstbalk
        JLabel labelCity = new JLabel("Woonplaats");
        labelCity.setHorizontalAlignment(JLabel.CENTER);
        city = new JLabel("");

        this.add(labelCity);
        this.add(city);

        //Postcode label en tekstbalk
        JLabel labelPostalCode = new JLabel("Postcode");
        labelPostalCode.setHorizontalAlignment(JLabel.CENTER);
        postalCode = new JLabel("");

        this.add(labelPostalCode);
        this.add(postalCode);


    }

    private class ButtonActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            String text = (account.getSelectedItem().toString());
            String[] splitted = text.split(" ");
            String name = Array.get(splitted, 0).toString();

            ArrayList<Account> list =  accountRepository.readAll();
            for(Account item : list){
                if(item.getFirstName().equals(name)){
                    firstName.setText(item.getFirstName());
                    lastName.setText(item.getLastName());
                    streetName.setText(item.getStreetName());
                    number.setText(Integer.toString(item.getHouseNumber()));
                    numberSuffix.setText(item.getNumberSuffix());
                    city.setText(item.getCity());
                    postalCode.setText(item.getPostalCode());
                }
            }

            account.removeAllItems();
            ArrayList<Account> acc = accountRepository.readAll();
            for (Account item : acc){
                account.addItem(item);

            }
        }

    }
}