package ui.AccountDetails;

import Data.Account;
import Data.AccountRepository;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class ChangeDetails extends JPanel {

    //Changes a chosen detail in the database

    private JComboBox account;
    private JRadioButton firstName, lastName, streetName, number, numberSuffix, city, postalCode;
    private JButton save;
    private JTextField newData;
    private AccountRepository repository = new AccountRepository();

    public ChangeDetails() {
        createComponents();
    }

    private void createComponents() {


        ArrayList<Account> text = repository.readAll();
        account = new JComboBox(text.toArray());
        this.add(account);

        JLabel title = new JLabel("Wat wil je wijzigen?");
        this.add(title);

        firstName = new JRadioButton("Voornaam");
        lastName = new JRadioButton("Achternaam");
        streetName = new JRadioButton("Straat");
        number = new JRadioButton("Huisnummer");
        numberSuffix = new JRadioButton("Toevoeging");
        city = new JRadioButton("Woonplaats");
        postalCode = new JRadioButton("Postcode");

        ButtonGroup options = new ButtonGroup();
        options.add(firstName);
        options.add(lastName);
        options.add(streetName);
        options.add(number);
        options.add(numberSuffix);
        options.add(city);
        options.add(postalCode);

        JPanel operation = new JPanel();
        Border operationBorder = BorderFactory.createTitledBorder("Details");
        operation.setBorder(operationBorder);

        operation.add(firstName);
        operation.add(lastName);
        operation.add(streetName);
        operation.add(number);
        operation.add(numberSuffix);
        operation.add(city);
        operation.add(postalCode);

        firstName.setSelected(true);

        this.add(operation);

        newData = new JTextField("", 20);
        this.add(newData);

        save = new JButton("Opslaan");
        ListenForButtons listener = new ListenForButtons();
        save.addActionListener(listener);
        this.add(save);


    }

    private class ListenForButtons implements ActionListener {

        private AccountRepository repository = new AccountRepository();

        @Override
        public void actionPerformed(ActionEvent e) {

            String acc = (account.getSelectedItem().toString());
            String[] splitted = acc.split(" ");
            String fname = Array.get(splitted, 0).toString();
            String lname = Array.get(splitted, 1).toString();

            if (e.getSource() == save) {
                if (firstName.isSelected()) {
                    String newName = newData.getText();
                    String column = "Voornaam";
                    repository.update(fname, lname, column, newName);
                    String message = "Succesvol opgeslagen!";
                    JOptionPane.showMessageDialog(null, message, "Informatie", JOptionPane.INFORMATION_MESSAGE);


                }else if (lastName.isSelected()) {
                    String newName = newData.getText();
                    String column = "Achternaam";
                    repository.update(fname, lname, column, newName);
                    String message = "Succesvol opgeslagen!";
                    JOptionPane.showMessageDialog(null, message, "Informatie", JOptionPane.INFORMATION_MESSAGE);


                }else if (streetName.isSelected()) {
                    String newStreet = newData.getText();
                    String column = "Straatnaam";
                    repository.update(fname, lname, column, newStreet);
                    String message = "Succesvol opgeslagen!";
                    JOptionPane.showMessageDialog(null, message, "Informatie", JOptionPane.INFORMATION_MESSAGE);


                }else if (number.isSelected()) {
                    String newHouseNumber = newData.getText();
                    try{
                        int num = Integer.parseInt(newData.getText());
                        String column = "Huisnummer";
                        repository.update(fname, lname, column, newHouseNumber);
                        String message = "Succesvol opgeslagen!";
                        JOptionPane.showMessageDialog(null, message, "Informatie", JOptionPane.INFORMATION_MESSAGE);

                    }catch (NumberFormatException ex){
                        String message = "Huisnummer kan alleen een getal zijn!";
                        JOptionPane.showMessageDialog(null, message,"Error Message", JOptionPane.ERROR_MESSAGE);
                    }


                }else if (numberSuffix.isSelected()) {
                    String newNumberSuffix = newData.getText();
                    String column = "Toevoeging";
                    repository.update(fname, lname, column, newNumberSuffix);
                    String message = "Succesvol opgeslagen!";
                    JOptionPane.showMessageDialog(null, message, "Informatie", JOptionPane.INFORMATION_MESSAGE);


                }else if (city.isSelected()) {
                    String newCity = newData.getText();
                    String column = "Woonplaats";
                    repository.update(fname, lname, column, newCity);
                    String message = "Succesvol opgeslagen!";
                    JOptionPane.showMessageDialog(null, message, "Informatie", JOptionPane.INFORMATION_MESSAGE);


                }else if (postalCode.isSelected()) {
                    String newPostal = newData.getText();
                    String column = "Postcode";
                    repository.update(fname, lname, column, newPostal);
                    String message = "Succesvol opgeslagen!";
                    JOptionPane.showMessageDialog(null, message, "Informatie", JOptionPane.INFORMATION_MESSAGE);

                }
            }

            newData.setText("");
            account.removeAllItems();
            ArrayList<Account> list = repository.readAll();
            for (Account item : list){
                account.addItem(item);

            }
        }

    }
}





