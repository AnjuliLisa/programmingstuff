package ui.AccountDetails;

import Data.Account;
import Data.AccountRepository;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class DeleteAccount extends JPanel {

    //deletes an account


    private AccountRepository accountRepository;

    public DeleteAccount() {
        accountRepository = new AccountRepository();
        createComponents();
        this.setLayout(new FlowLayout(FlowLayout.CENTER));
    }

    public void createComponents(){

        ArrayList<Account> list = accountRepository.readAll();
        JComboBox accounts = new JComboBox(list.toArray());

        JButton delete = new JButton("Verwijder Account");
        DeleteButtonActionListener listener = new DeleteButtonActionListener(accounts, delete);
        delete.addActionListener(listener);

        this.add(accounts);
        this.add(delete);


    }




}
