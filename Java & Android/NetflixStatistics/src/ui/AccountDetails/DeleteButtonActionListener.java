package ui.AccountDetails;

import Data.Account;
import Data.AccountRepository;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class DeleteButtonActionListener implements ActionListener {

    //Listener for delete account panel

    private JComboBox account;
    private JButton delete;
    private AccountRepository repository;

    public DeleteButtonActionListener(JComboBox account, JButton delete) {
        this.account = account;
        this.delete = delete;
        repository = new AccountRepository();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == delete){

            String acc = account.getSelectedItem().toString();
            String[] splitted = acc.split(" ");
            String firstName = Array.get(splitted, 0).toString();
            String lastName = Array.get(splitted, 1).toString();

            repository.delete(firstName, lastName);
            account.removeAllItems();
            ArrayList<Account> list = repository.readAll();
            for (int i = 0; i < list.size(); i++){
                account.addItem(list.get(i));
            }

            String message = "Succesvol verwijderd!";
            JOptionPane.showMessageDialog(null, message,"Informatie", JOptionPane.INFORMATION_MESSAGE);

        }
    }


}
